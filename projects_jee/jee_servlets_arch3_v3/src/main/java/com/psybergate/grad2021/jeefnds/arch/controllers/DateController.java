package com.psybergate.grad2021.jeefnds.arch.controllers;

import com.psybergate.grad2021.jeefnds.arch.framework.Controller;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;

public class DateController implements Controller {

  public String today(HttpServletRequest req) {
    return "<h1> TODAY'S DATE IS: " + LocalDate.now() + "</h1>";
  }

  public String tomorrow(HttpServletRequest req) {
    return "<h1> TOMORROW'S DATE IS: " + (LocalDate.now().plusDays(1)) + "</h1>";
  }

}
