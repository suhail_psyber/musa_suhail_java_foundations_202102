package com.psybergate.grad2021.jeefnds.arch.controllers;

import com.psybergate.grad2021.jeefnds.arch.framework.Controller;

import javax.servlet.http.HttpServletRequest;

public class GreetingController implements Controller {


  public String hello(HttpServletRequest req) {
    String string = "<h1> HELLO WORLD!!! :) </h1> <br>";
    string += "<h1>" + req.getParameter("name").toUpperCase() + "</h1>";
    return string;
  }

  public String goodbye(HttpServletRequest req) {
    String string = "<h1> GOODBYE WORLD!!! :( </h1> <br>";
    string += "<h1>" + req.getParameter("name").toUpperCase() + "</h1>";
    return string;
  }

}
