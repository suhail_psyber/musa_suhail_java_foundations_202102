package com.psybergate.grad2021.jeefnds.servlets.commands;

import com.psybergate.grad2021.jeefnds.servlets.controllers.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;

public class GetCurrentDate {

    public void execute(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        PrintWriter printWriter = resp.getWriter();
        printWriter.println("<html>");
        printWriter.println("<body style=\"text-align:center; color:red;\">");

        printWriter.println("<h1> TODAY'S DATE IS: " + LocalDate.now() + "</h1>");

        printWriter.println("</body>");
        printWriter.println("</html>");

    }

}
