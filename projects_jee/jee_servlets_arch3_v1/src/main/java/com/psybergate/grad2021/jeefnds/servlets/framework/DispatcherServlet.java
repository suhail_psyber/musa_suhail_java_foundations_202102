package com.psybergate.grad2021.jeefnds.servlets.framework;

import com.psybergate.grad2021.jeefnds.servlets.controllers.Controller;
import com.psybergate.grad2021.jeefnds.servlets.controllers.DateController;
import com.psybergate.grad2021.jeefnds.servlets.controllers.GreetingController;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "Dispatcher", urlPatterns = {"/hello", "/goodbye", "/today", "/tomorrow"})
public class DispatcherServlet extends HttpServlet {

  private static final Map<String, Controller> CONTROLLERS = new HashMap<>(); //Servlets should not have state so this is probably not allowed.

  static {
    CONTROLLERS.put("greetingController", new GreetingController());
    CONTROLLERS.put("dateController", new DateController());
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    String servletPath = req.getServletPath();

    Controller controller = CONTROLLERS.get("greetingController");
    if (servletPath.equals("/today") || servletPath.equals("/tomorrow")) {
      controller = CONTROLLERS.get("dateController");
    }

    controller.execute(req, resp);
  }

}
