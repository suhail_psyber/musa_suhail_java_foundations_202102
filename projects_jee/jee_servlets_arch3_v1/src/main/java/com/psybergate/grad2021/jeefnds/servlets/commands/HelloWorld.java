package com.psybergate.grad2021.jeefnds.servlets.commands;

import com.psybergate.grad2021.jeefnds.servlets.controllers.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class HelloWorld {

    public void execute(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        PrintWriter printWriter = resp.getWriter();
        printWriter.println("<html>");
        printWriter.println("<body style=\"text-align:center;color:red\">");

        printWriter.println("<h1> HELLO WORLD!!! :) </h1>");
        printWriter.println("<h1>" + req.getParameter("name").toUpperCase() + "</h1>");


        printWriter.println("</body>");
        printWriter.println("</html>");

    }

}
