package com.psybergate.grad2021.jeefnds.servlets.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class GreetingController implements Controller {

  @Override
  public void execute(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    String servletPath = req.getServletPath();
    PrintWriter printWriter = resp.getWriter();

    printWriter.println("<html>");
    printWriter.println("<body style=\"text-align:center;color:red\">");

    if (servletPath.equals("/hello")) {
      printWriter.println(getHelloMessage());
    }
    else if (servletPath.equals("/goodbye")) {
      printWriter.println(getGoodbyeMessage());
    }

    printWriter.println("<h1>" + req.getParameter("name").toUpperCase() + "</h1>");
    printWriter.println("</body>");
    printWriter.println("</html>");
  }

  public String getHelloMessage() {
    return "<h1> HELLO WORLD!!! :) </h1>";
  }

  private String getGoodbyeMessage() {
    return "<h1> GOODBYE WORLD!!! :( </h1>";
  }

}
