package com.psybergate.grad2021.jeefnds.web;

import javax.servlet.ServletContext;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@WebServlet(name = "hello", urlPatterns = "/hello")
public class HelloWorldServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

    PrintWriter printWriter = resp.getWriter();
    resp.setContentType("text/html");

    printWriter.println("<body style=\"text-align:center; color:red;\">");
    printWriter.println("<br> Hello there " + getName());
    printWriter.println("<br> Today is: " + LocalDate.now());
    printWriter.println("</body>");
  }

  private String getName() {
    ServletContext servletContext = getServletContext();
    List<String> names = new ArrayList<String>((Set) servletContext.getAttribute("names"));

    int nameIndex = (int) (Math.random() * 5);
    String name = names.get(nameIndex);

    System.out.println("names = " + names);
    System.out.println("nameIndex = " + nameIndex);
    System.out.println("nameIndex = " + name);

    return name;
  }

}
