package com.psybergate.grad2021.jeefnds.web;

import javax.servlet.*;
import javax.servlet.annotation.WebListener;
import java.util.HashSet;
import java.util.Set;

@WebListener
public class MyServletContextListener implements ServletContextListener {


  @Override
  public void contextInitialized(ServletContextEvent sce) {
    final Set validNames = new HashSet();
    validNames.add("John");
    validNames.add("Jane");
    validNames.add("Eren");
    validNames.add("Vidic");
    validNames.add("Thando");

    ServletContext servletContext = sce.getServletContext();
    servletContext.setAttribute("names", validNames);
    System.out.println("*********SCE has been initialized************");
  }

}
