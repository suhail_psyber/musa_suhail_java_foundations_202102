package com.psybergate.grad2021.jeefnds.web;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;

@WebServlet(name = "hello", urlPatterns = "/hello")
public class HelloWorldServlet extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    System.out.println("running doGet");
    //getName(req);

    PrintWriter printWriter = resp.getWriter();
    resp.setContentType("text/html");

    printWriter.println("<body style=\"text-align:center; color:red;\">");
    printWriter.println("<br> Hello there " + req.getParameter("name"));
    printWriter.println("<br> Today is: " + LocalDate.now());
    printWriter.println("</body>");
  }

}
