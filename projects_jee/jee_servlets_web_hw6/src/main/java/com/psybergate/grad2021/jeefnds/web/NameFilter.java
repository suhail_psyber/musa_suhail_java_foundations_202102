package com.psybergate.grad2021.jeefnds.web;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * This class ensures that if "http://localhost:8080/jee_servlets_web_hw6/hello?name=nameValue" is passed as the request, and the nameValue is John,
 * the request will be stopped and not proceed to servletNames
 */
@WebFilter(filterName = "John Filter", servletNames = "hello")
public class NameFilter implements Filter {

  @Override
  public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
    System.out.println("invoking filter");
    PrintWriter printWriter = servletResponse.getWriter();

    if (servletRequest.getParameter("name").equalsIgnoreCase("john")) {
      printWriter.println("<h2> John does have permission to access this page </h2>");
    } else {
      filterChain.doFilter(servletRequest, servletResponse);
    }

  }

}
