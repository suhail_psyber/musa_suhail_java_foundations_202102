package com.psybergate.grad2021.jeefnds.helloworldann.ce4a;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;

@WebServlet(name = "helloworld2", urlPatterns = "/helloworld")
public class HelloWorldServletWithAnnotations extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter printWriter = resp.getWriter();
        printWriter.println("<html>");
        printWriter.println("<body style=\"text-align:center; color:red;\">");

        printWriter.println("<h1>This is a RED heading</h1>");
        printWriter.println("<br> Hello World - today is: " + LocalDate.now());
        printWriter.println("<br>Your age is: " + yourAge());
        printWriter.println("<br>Holy Shit, it works!");

        printWriter.println("</body>");
        printWriter.println("</html>");

    }

    private String yourAge() {
        return "26";
    }

}
