package com.psybergate.grad2021.jeefnds.helloworldann.ce4a;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class HttpUrlClient {

    public static void main(String[] args) throws Exception {
        //setup the url and open the connection
        String urlString = "http://localhost:8080/helloann/hello";
        URL url = new URL(urlString);
        URLConnection urlConnection = url.openConnection();

        //Read the request and get the response
        try (BufferedReader response = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()))) {
          String line;
            while ((line = response.readLine()) != null) {
                System.out.println(line);
            }
        }

    }

}
