package com.psybergate.grad2021.core;

import java.time.Duration;
import java.time.Instant;

public class Timer {

  private Instant start;

  private Instant end;

  public Timer() {

  }

  public void start() {
    start = Instant.now();
  }

  public void end() {
    end = Instant.now();
  }

  public Duration duration() {
    return Duration.between(start, end);
  }



}
