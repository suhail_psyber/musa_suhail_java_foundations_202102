package com.psybergate.grad2021.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomerInformationGenerator {

  public static List<String> generateCN(int numberOfCustomers) {
    int currentCustomerNumber = 1000;
    List<String> listOfCustomerNumbers = new ArrayList<>();

    for (int i = 0; i < numberOfCustomers; i++) {
      listOfCustomerNumbers.add("CN" + currentCustomerNumber);
      currentCustomerNumber += Utils.random(10, 1);
    }
    //System.out.println(listOfCustomerNumbers);
    return listOfCustomerNumbers;
  }

  public static String generateName() {
    List<String> names = Arrays.asList("Satori", "Max", "Naruto", "Eren", "Goku", "Sasuke", "Gohan", "Edward", "Peter", "Ragnar");

    return names.get(Utils.random(10, 0));
  }

  public static String generateSurname() {
    String randomName = null;
    if (Math.random() >= 0.2) { //80% of people get a surname and 20% don't
      randomName = generateName();
    }

    return randomName;
  }

  public static String generateCity() {
    String city = null;
    List<String> cities = Arrays.asList("Johannesburg", "Cape Town", "Durban", "Stellenbosch", "Nelspruit", "Pretoria", "Newcastle", "Bloemfontein");
    city = cities.get(Utils.random(8, 0));

    return city;
  }

}

