package com.psybergate.grad2021.jeefnds.servlets.framework;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * DO NOT USE THIS AS A FUNCTIONAL INTERFACE
 */
public interface Controller {

  /**
   * The method called when the Dispatcher passes on a Request to a Controller
   * @param request
   * @param response
   * @throws Exception
   */
  void execute(HttpServletRequest request, HttpServletResponse response) throws IOException;

}
