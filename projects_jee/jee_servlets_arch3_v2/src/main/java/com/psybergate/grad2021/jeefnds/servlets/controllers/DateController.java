package com.psybergate.grad2021.jeefnds.servlets.controllers;

import com.psybergate.grad2021.jeefnds.servlets.framework.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;

public class DateController implements Controller {

  @Override
  public void execute(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    String servletPath = req.getServletPath();
    PrintWriter printWriter = resp.getWriter();

    printWriter.println("<html>");
    printWriter.println("<body style=\"text-align:center;color:red\">");

    if (servletPath.equals("/today")) {
      printWriter.println(getCurrentDate());
    }
    if (servletPath.equals("/tomorrow")) {
      printWriter.println(getTomorrowsDate());
    }

    printWriter.println("</body>");
    printWriter.println("</html>");
  }

  private String getCurrentDate() {
    return "<h1> TODAY'S DATE IS: " + LocalDate.now() + "</h1>";
  }

  private String getTomorrowsDate() {
    return "<h1> TOMORROW'S DATE IS: " + (LocalDate.now().plusDays(1)) + "</h1>";
  }

  @Override
  public String toString() {
    return this.getClass().getSimpleName();
  }
}
