package com.psybergate.grad2021.jeefnds.servlets.framework;

import com.psybergate.grad2021.jeefnds.servlets.controllers.GreetingController;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@WebServlet(name = "Dispatcher", urlPatterns = {"/hello", "/goodbye", "/today", "/tomorrow"})
public class DispatcherServlet extends HttpServlet {

  private static final int VERSION = 1;

  private static final Map<String, Controller> CONTROLLERS = new HashMap<>(); //Servlets should not have state so this is probably not allowed.

  @Override
  public void init() throws ServletException {
    loadController();
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    Controller controller = getController(req.getServletPath());
    controller.execute(req, resp);
  }

  //dynamically add controllers to the map - next step would be to save each method using reflection
  private void loadController() {
    try {
      InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("controllerV1.properties");
      Properties properties = new Properties();
      properties.load(inputStream);

      for (String propKey : properties.stringPropertyNames()) {
        String propValue = (String) properties.get(propKey);
        Controller cont = (Controller) Class.forName(propValue).newInstance();
        CONTROLLERS.put("/" + propKey, cont);
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private Controller getController(String servletPath) {
    Controller controller = CONTROLLERS.get(servletPath);
    return controller;
  }
}
