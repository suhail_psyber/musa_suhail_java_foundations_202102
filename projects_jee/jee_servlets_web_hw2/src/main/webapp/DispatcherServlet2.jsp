<html>
<%@ page import="java.util.Enumeration" %>


<body>

<h2> The HTTP Header Names and Values </h2>
<%
  Enumeration headerNames = request.getHeaderNames();
    while(headerNames.hasMoreElements()) {
    String headerName = (String)headerNames.nextElement();
%>
<li> <%= "Header Name: " + headerName + " = " + request.getHeader(headerName) + "<br>" %>
<% } %>

<h2> The Protocol </h2>
<div> <li> <%= request.getProtocol() %> </div>

<h2> The HTTP Method </h2>
<div> <li> <%= request.getMethod() %> </div>

<h2> The Request URI </h2>
<div> <li> <%= request.getRequestURI() %> </div>

<h2> The Context Path </h2>
<div> <li> <%= request.getContextPath() %> </div>

<h2> The Servlet Path </h2>
<div> <li> <%= request.getServletPath() %> </div>

<h2> The Path Info </h2>
<div> <li> <%= request.getPathInfo() %> </div>

<h2> There are many other things to get from the Request Object </h2>

</body>
</html>