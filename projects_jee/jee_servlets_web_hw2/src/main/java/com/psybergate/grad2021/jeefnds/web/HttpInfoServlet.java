package com.psybergate.grad2021.jeefnds.web;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.*;

@WebServlet(name = "Dispatcher", urlPatterns = {"/servlet/*"})
public class HttpInfoServlet extends HttpServlet {

  private static final int VERSION = 2;

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    resp.setContentType("text/html");
    PrintWriter pw = resp.getWriter();

    pw.println("<h2> The HTTP Header Names and Values </h2>");
    Enumeration headerNames = req.getHeaderNames();
    while(headerNames.hasMoreElements()) {
      String headerName = (String)headerNames.nextElement();
      pw.println("Header Name: " + headerName + " = " + req.getHeader(headerName) + "<br>");
    }

    pw.println("<h2> The Protocol </h2>");
    pw.println(req.getProtocol());
    pw.println("<br><br>");

    pw.println("<h2> The HTTP Method </h2>");
    pw.println(req.getMethod());
    pw.println("<br><br>");

    pw.println("<h2> The Request URI </h2>");
    pw.println(req.getRequestURI());
    pw.println("<br><br>");

    pw.println("<h2> The Context Path </h2>");
    pw.println(req.getContextPath());
    pw.println("<br><br>");

    pw.println("<h2> The Servlet Path </h2>");
    pw.println(req.getServletPath());
    pw.println("<br><br>");

    pw.println("<h2> The Path Info </h2>");
    pw.println(req.getPathInfo());
    pw.println("<br><br>");

    pw.println("<h2> There are many other things to get </h2>");

  }

}
