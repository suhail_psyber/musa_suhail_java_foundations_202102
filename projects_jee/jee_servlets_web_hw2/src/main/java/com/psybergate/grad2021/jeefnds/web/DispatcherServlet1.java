package com.psybergate.grad2021.jeefnds.web;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "Dispatcher1", urlPatterns = "/dispatcher1")
public class DispatcherServlet1 extends HttpServlet {

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    /**
     * Cannot put additional info prior to forwarding
     */
    //resp.setContentType("text/html");
    //PrintWriter pw = resp.getWriter();
    //pw.println("<h2> forwarding to /servlet ... </h2> <br><br><br>");

    RequestDispatcher requestDispatcher = req.getRequestDispatcher("/servlet");
    requestDispatcher.forward(req, resp);

  }

}
