package com.psybergate.grad2021.jeefnds.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

//class annotation for the path goes here
public class TestServlet extends HttpServlet {

    //external css in resources? and how to get a hold of it

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String cssTag = "<link rel='stylesheet' type='text/css' href='../css/style.css'>";

        PrintWriter printWriter = resp.getWriter();
        printWriter.println("<html>");
        printWriter.println("<head><title>Tab Name</title>"+cssTag+"</head>");
        printWriter.println("<br><h1>TEST</h1>");
        printWriter.println("<br><h1>TEST</h1>");
        printWriter.println("<br><h1>TEST</h1>");
        printWriter.println("</html>");

    }

    private String yourAge() {
        return "26";
    }

}