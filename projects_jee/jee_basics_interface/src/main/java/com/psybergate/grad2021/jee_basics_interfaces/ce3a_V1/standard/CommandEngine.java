package com.psybergate.grad2021.core.ce3a_V1.standard;

public interface CommandEngine {

  /**
   * This should have some methods such as those below:
   * add();
   * subtract();
   */

  void runEngine();

  public CommandResponse processCommand(Command command, CommandRequest data);

  public Command getCommand();

  public CommandRequest getRequestValues();

  public CommandResponse getResponse();

}
