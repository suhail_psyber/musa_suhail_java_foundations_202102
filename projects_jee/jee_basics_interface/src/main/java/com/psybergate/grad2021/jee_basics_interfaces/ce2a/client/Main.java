package com.psybergate.grad2021.core.ce2a.client;

import java.sql.*;

public class Main {

  private static String pw = "12345"; //admin

  private static String user = "mydatabase"; //postgres

  private static String url = "jdbc:mydatabase://localhost:5432/mydatabase"; //"jdbc:postgresql://localhost:5432/postgres"

  public static void main(String[] args) throws ClassNotFoundException, SQLException {

    //Load the vendor implementation of the Driver Class - equivalent to "org.postgresql.Driver"
    Class.forName("com.psybergate.grad2021.core.ce2a.vendor.MyDriver"); //Everything below is based on this???

    //Create a connection to /MyDatabase/ - this should load MyConnection with MyDriver using the registerDriver() method
    Connection connection = DriverManager.getConnection(url, user, pw); //The url should ensure we are connecting to vendor impl of Connection

    //Creating a statement to be executed based on MyConnection because MyDriver
    Statement statement = connection.createStatement();

    ResultSet rs = statement.executeQuery("select * from mytable");
  }
}
