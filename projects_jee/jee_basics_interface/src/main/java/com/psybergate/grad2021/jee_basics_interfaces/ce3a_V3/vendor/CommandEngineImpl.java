package com.psybergate.grad2021.core.ce3a_V3.vendor;

import com.psybergate.grad2021.core.ce3a_V3.client.client_commands.Add;
import com.psybergate.grad2021.core.ce3a_V3.standard.*;

import java.util.HashMap;
import java.util.Map;

public class CommandEngineImpl implements CommandEngine {

  Map commands = new HashMap();

  @Override
  public void runEngine() { //initializer
    commands.put("add", new Add());
    //commands.put("subtract", new Subtract());
  }

  @Override
  public CommandOutput processCommand(Command command, CommandInput data) {
    if (data == null) {
      throw new RuntimeException("Please set data values");
    }

    CommandOutput output = new StdCommandOutput();
    output = command.executeCommand(data);

    return output;
  }

}
