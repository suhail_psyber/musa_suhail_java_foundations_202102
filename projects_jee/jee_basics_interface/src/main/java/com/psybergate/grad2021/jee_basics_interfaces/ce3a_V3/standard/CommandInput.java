package com.psybergate.grad2021.core.ce3a_V3.standard;

import java.util.List;

public interface CommandInput {

  List<Object> getInputData();

  void setInputData(Object InputData);

}
