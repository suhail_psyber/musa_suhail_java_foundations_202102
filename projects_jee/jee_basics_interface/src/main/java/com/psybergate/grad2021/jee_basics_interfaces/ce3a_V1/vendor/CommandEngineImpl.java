package com.psybergate.grad2021.core.ce3a_V1.vendor;

import com.psybergate.grad2021.core.ce3a_V1.standard.Command;
import com.psybergate.grad2021.core.ce3a_V1.standard.CommandEngine;
import com.psybergate.grad2021.core.ce3a_V1.standard.CommandRequest;
import com.psybergate.grad2021.core.ce3a_V1.standard.CommandResponse;

import java.io.InputStream;
import java.util.Properties;

public class CommandEngineImpl implements CommandEngine {

  private Command command;

  private CommandRequest commandRequestData;

  private CommandResponse response;

  public CommandEngineImpl(CommandRequest requestValues) {
    this.commandRequestData = requestValues;
  }

  @Override
  public void runEngine() {
    loadCommand();
    processCommand(command, commandRequestData);
  }

  @Override
  public CommandResponse processCommand(Command command, CommandRequest data) {
    this.response = command.mainCommand(data);
    command.dummyMethod1();
    command.dummyMethod2();

    return response;
  }

  public Command loadCommand() { //loads a command impl using reflection

    try(InputStream inputStream = this.getClass().getResourceAsStream("command.properties")) {
      System.out.println("Stream loaded successfully: " + inputStream);

      Properties prop = new Properties();
      prop.load(inputStream);
      String propertyClass = prop.getProperty("command.impl");
      System.out.println("property loaded: " + propertyClass);
      command = (Command) Class.forName(propertyClass).newInstance();
      System.out.println("Command loaded successfully: " + command + "\n");


    } catch (Exception e) {
      throw new RuntimeException("Dynamic loading of command has failed", e);
    }

    return command;
  }

  public Command getCommand() {
    return command;
  }

  public CommandRequest getRequestValues() {
    return commandRequestData;
  }

  public CommandResponse getResponse() {
    return response;
  }

  public void setRequestValues(CommandRequest requestValues) {
    this.commandRequestData = requestValues;
  }
}
