package com.psybergate.grad2021.core.ce1a.standard;

public interface DateFactory {

  public Date createANewDate(int day, int month, int year) throws InvalidDateException;
}
