package com.psybergate.grad2021.core.ce3a_V1.client;

import com.psybergate.grad2021.core.ce3a_V1.standard.CommandEngine;
import com.psybergate.grad2021.core.ce3a_V1.standard.CommandRequestData;
import com.psybergate.grad2021.core.ce3a_V1.standard.CommandResponse;
import com.psybergate.grad2021.core.ce3a_V1.vendor.CommandEngineImpl;

public class Client {

  public static void main(String[] args) {

    CommandEngine ce = new CommandEngineImpl(new CommandRequestData(5,6));
    //ce.setValues(new CommandRequestValues(4,6)); might be better to just create the engine then set the values here? although the Command object
    // would be created prior t
    ce.runEngine();

    CommandResponse cr = ce.getResponse(); //returns the response of the COMMAND ENGINE
    System.out.println("cr = " + cr.getResponse()); //returns the response of the COMMAND RESPONSE
  }

}
