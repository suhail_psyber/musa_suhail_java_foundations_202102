package com.psybergate.grad2021.core.ce3a_V2.standard;

public class CommandRequestData implements CommandRequest {

  private Object val1;

  private Object val2;

  public CommandRequestData(Object val1, Object val2) {
    this.val1 = val1;
    this.val2 = val2;
  }

  @Override
  public CommandRequest request(Object o1, Object o2) {
    return null;
  }

  @Override
  public Object getObj1() {
    return val1;
  }

  @Override
  public Object getObj2() {
    return val2;
  }
}
