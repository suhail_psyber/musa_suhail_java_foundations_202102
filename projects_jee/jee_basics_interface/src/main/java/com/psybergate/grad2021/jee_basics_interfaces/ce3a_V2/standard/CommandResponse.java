package com.psybergate.grad2021.core.ce3a_V2.standard;

public interface CommandResponse {

  public String getResponse();

  public void setResponse(String response);

}
