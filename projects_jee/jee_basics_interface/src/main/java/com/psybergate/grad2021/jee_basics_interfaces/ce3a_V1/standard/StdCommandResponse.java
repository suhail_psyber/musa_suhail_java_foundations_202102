package com.psybergate.grad2021.core.ce3a_V1.standard;

public class StdCommandResponse implements CommandResponse {

  private String commandResponse;

  public String getResponse() {
    return commandResponse;
  }

  @Override
  public void setResponse(String response) {
    this.commandResponse = response;
  }

}
