package com.psybergate.grad2021.core.ce3a_V2.client;

import com.psybergate.grad2021.core.ce3a_V2.standard.CommandEngine;
import com.psybergate.grad2021.core.ce3a_V2.standard.CommandRequestData;
import com.psybergate.grad2021.core.ce3a_V2.standard.CommandResponse;
import com.psybergate.grad2021.core.ce3a_V2.vendor.CommandEngineImpl;

public class Client {

  public static void main(String[] args) {

    CommandEngine ce = new CommandEngineImpl(); //Already Add or Subtract here
    ce.setCommandRequestData(new CommandRequestData(5,6));
    ce.runEngine();

    CommandResponse cr = ce.getResponse(); //returns the response of the COMMAND ENGINE
    System.out.println("cr = " + cr.getResponse()); //returns the response of the COMMAND RESPONSE

  }

}
