package com.psybergate.grad2021.core.ce3a_V3.standard;

public class StdCommandOutput implements CommandOutput {

  private String response;

  public StdCommandOutput() {
  }

  @Override
  public String getResponse() {
    return response;
  }

  @Override
  public void setResponse(String response) {
    this.response = response;
  }

}
