package com.psybergate.grad2021.core;

public @interface Info {

  /**
   * Model 1: Client->Standard<-Vendor:
   * Client runs the show - The client CALLS the methods he wants to run in the order he wants it. Far more readable
   *
   * Model 2: Vendor->Standard<-Client:
   * Vendor runs the show - The client makes multiple objects (each according to the vendor's specs which in turn follows the standard) that would
   * be called by the vendor's implementation engine that would use reflection to instantiate the type of object the client would want at runtime.
   * The vendor controls the order of method calls
   */
}

