package com.psybergate.grad2021.core.ce1a.vendor1;

import com.psybergate.grad2021.core.ce1a.standard.Date;
import com.psybergate.grad2021.core.ce1a.standard.DateFactory;
import com.psybergate.grad2021.core.ce1a.standard.InvalidDateException;

public class DateFactoryImpl1 implements DateFactory {

  @Override
  public Date createANewDate(int day, int month, int year) throws InvalidDateException {
    return new DateImpl1(day, month, year);
  }

}
