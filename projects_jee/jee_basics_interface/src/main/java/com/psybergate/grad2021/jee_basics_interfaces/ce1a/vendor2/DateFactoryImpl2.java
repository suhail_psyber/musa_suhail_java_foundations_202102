package com.psybergate.grad2021.core.ce1a.vendor2;

import com.psybergate.grad2021.core.ce1a.standard.Date;
import com.psybergate.grad2021.core.ce1a.standard.DateFactory;
import com.psybergate.grad2021.core.ce1a.standard.InvalidDateException;

public class DateFactoryImpl2 implements DateFactory {

  @Override
  public Date createANewDate(int day, int month, int year) throws InvalidDateException {
    return new DateImpl2(day, month, year);
  }

}
