package com.psybergate.grad2021.core.ce1a.standard;

import java.io.InputStream;
import java.util.Properties;

/**
 * Connects DateFactoryInterface to the DateFactory Implementation
 */
public class DateFactoryImplLoader {

  private static DateFactory dateFactoryImpl;

  /**
   * This simply looks for which dateFactoryImpl we want to load (we have 2 vendors with their iwn factories)... this loads the factory we want
   * based on the date.factory property in the date.properties Property Class File.
   * @return DateFactory;
   */
  public static DateFactory getADateFactory() {
    if (dateFactoryImpl != null) {
      return dateFactoryImpl;
    }

    try (InputStream inputStream = DateFactoryImplLoader.class.getResourceAsStream("resources/date.properties")) {
      System.out.println(inputStream);

      Properties properties = new Properties();
      properties.load(inputStream);
      String dateFactoryClass = properties.getProperty("date.factory");
      dateFactoryImpl = (DateFactory) Class.forName(dateFactoryClass).newInstance();

      return dateFactoryImpl;
    } catch (Exception e) {
      throw new RuntimeException("Reflective/Dynamic loading of dateFactoryImpl's Class has failed",e);
    }
  }
}

