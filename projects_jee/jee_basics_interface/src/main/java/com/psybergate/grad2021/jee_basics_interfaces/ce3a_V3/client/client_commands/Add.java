package com.psybergate.grad2021.core.ce3a_V3.client.client_commands;

import com.psybergate.grad2021.core.ce3a_V3.standard.Command;
import com.psybergate.grad2021.core.ce3a_V3.standard.CommandInput;
import com.psybergate.grad2021.core.ce3a_V3.standard.CommandOutput;
import com.psybergate.grad2021.core.ce3a_V3.standard.StdCommandOutput;

import java.util.List;

public class Add implements Command {

  @Override
  public CommandOutput executeCommand(CommandInput data) {
    System.out.println("Starting Command with name: " + this.getClass().getSimpleName());
    List<Object> numbers = data.getInputData();

    int result = calcResult(data);
    CommandOutput output = new StdCommandOutput();
    output.setResponse("" + result);

    return output;
  }

  /**
   * This method is the defining logic for each COMMAND and will thus vary per COMMAND
   *
   * @param values
   * @return int - sum of values
   */
  private int calcResult(CommandInput values) {
    int result = 0;

    for (Object o : values.getInputData()) {
      result += (int) o;
    }

    return result;
  }

}
