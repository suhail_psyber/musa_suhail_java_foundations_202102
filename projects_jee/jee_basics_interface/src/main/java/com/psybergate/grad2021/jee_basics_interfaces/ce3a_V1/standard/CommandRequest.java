package com.psybergate.grad2021.core.ce3a_V1.standard;

public interface CommandRequest {

  public Object getVal1();

  public Object getVal2();

  CommandRequest request(Object o1, Object o2);

}
