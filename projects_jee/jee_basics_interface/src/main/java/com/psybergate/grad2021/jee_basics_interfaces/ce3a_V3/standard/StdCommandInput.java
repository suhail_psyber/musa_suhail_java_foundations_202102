package com.psybergate.grad2021.core.ce3a_V3.standard;

import java.util.ArrayList;
import java.util.List;

public class StdCommandInput implements CommandInput {

  private List<Object> inputData = new ArrayList<>();

  @Override
  public List<Object> getInputData() {
    return inputData;
  }

  @Override
  public void setInputData(Object inputData) {
    this.inputData.add(inputData);
  }

}
