package com.psybergate.grad2021.core.ce3a_V2.standard;

public class StdCommandResponse implements CommandResponse {

  private String commandResponse;

  @Override
  public String getResponse() {
    return commandResponse;
  }

  @Override
  public void setResponse(String response) {
    this.commandResponse = response;
  }

}
