package com.psybergate.grad2021.core.ce1a.vendor1;

import com.psybergate.grad2021.core.ce1a.standard.Date;
import com.psybergate.grad2021.core.ce1a.standard.InvalidDateException;

public class DateImpl1 implements Date {

  private int day;
  private int month;
  private int year;

  public DateImpl1(int day, int month, int year) {
    try {
      validateDate(day, month, year);
      this.day = day;
      this.month = month;
      this.year = year;
    } catch (InvalidDateException e) {
      e.printStackTrace();
    }
  }

  @Override
  public boolean isLeapYear() {
    return isLeapYear(this.year);
  }

  /**
   * not actually implemented
   */
  @Override
  public Date addDays(int numOfDays) {
    return new DateImpl1(day, month, year);
  }

  public static boolean isLeapYear(int year) {
    if (year % 100 == 0 && year % 400 == 0) {
      return true;
    } else if (year % 100 != 0 && year % 4 == 0) {
      return true;
    }

    return false;
  }

  public static void validateDate(int day, int month,int year) throws InvalidDateException {
      validateMonth(month);
      validateDay(day, month, year);
  }

  private static void validateMonth(int month) throws InvalidDateException {
    if (month < 1 || month > 12) {
      throw new InvalidDateException();
    }
  }

  private static void validateDay(int day, int month, int year) throws InvalidDateException {
    int maxDay = 31;

    if (month == 4 || month == 6 || month == 9 || month == 10 || month == 11) {
      maxDay = 30;
    }
    if (month == 2) {
      maxDay = 28;
      if (isLeapYear(year)) {
        maxDay = 29;
      }
    }

    if (day > maxDay || day < 1) {
      throw new InvalidDateException();
    }

  }

  public int getDay() {
    return day;
  }

  public int getMonth() {
    return month;
  }

  public int getYear() {
    return year;
  }

  @Override
  public String toString() {
    return "DateImpl1 {" + "day=" + day + ", month=" + month + ", year=" + year + '}';
  }
}
