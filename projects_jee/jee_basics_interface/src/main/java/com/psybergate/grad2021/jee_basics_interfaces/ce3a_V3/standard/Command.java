package com.psybergate.grad2021.core.ce3a_V3.standard;

/**
 * an instruction to the CommandEngine - Add or Subtract
 * param: CommandRequest
 * returns: CommandResponse
 */
public interface Command {

  CommandOutput executeCommand(CommandInput values);

}
