package com.psybergate.grad2021.core.ce3a_V3.standard;

/**
 * Will Process CommandInput based on the Command called
 */

public interface CommandEngine {

  /**
   * Initializes the Engine
   */
  void runEngine();

  /**
   * Runs the command passed in by the client
   * @param command
   * @param data
   * @return CommandOutput which has a String variable "response"
   */
  public CommandOutput processCommand(Command command, CommandInput data);


}
