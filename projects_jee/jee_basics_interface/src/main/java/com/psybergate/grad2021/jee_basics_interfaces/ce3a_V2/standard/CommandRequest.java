package com.psybergate.grad2021.core.ce3a_V2.standard;

public interface CommandRequest {

  CommandRequest request(Object o1, Object o2);

  public Object getObj1();

  public Object getObj2();

}
