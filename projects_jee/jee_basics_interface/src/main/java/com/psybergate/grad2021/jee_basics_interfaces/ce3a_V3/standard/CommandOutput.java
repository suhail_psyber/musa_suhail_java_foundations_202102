package com.psybergate.grad2021.core.ce3a_V3.standard;

public interface CommandOutput {

  public String getResponse();

  public void setResponse(String response);

}
