package com.psybergate.grad2021.core.ce3a_V1.standard;

/**
 * an instruction to the CommandEngine
 * param: CommandRequest
 * returns: CommandResponse
 */
public interface Command {

  CommandResponse mainCommand(CommandRequest values);

  void dummyMethod1();

  void dummyMethod2();

}
