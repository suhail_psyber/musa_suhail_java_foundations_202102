package com.psybergate.grad2021.core.ce3a_V2.vendor;

import com.psybergate.grad2021.core.ce3a_V2.standard.*;

import java.io.InputStream;
import java.util.Properties;

public class CommandEngineImpl implements CommandEngine {

  private Command command; //should not be

  private CommandRequest commandRequestData;

  private CommandResponse response;

  @Override
  public void runEngine() {
    loadCommand();
    processCommand(command, commandRequestData);
  }

  @Override
  public CommandResponse processCommand(Command command, CommandRequest data) {
    if (data == null) {
      throw new RuntimeException("Please set data values");
    }
    this.response = command.mainCommand(data);
    command.dummyMethod1();
    command.dummyMethod2();

    return response;
  }

  public Command loadCommand() { //loads a command impl using reflection

    try (InputStream inputStream = this.getClass().getResourceAsStream("command.properties")) {
      System.out.println("Stream loaded successfully: " + inputStream);

      Properties prop = new Properties();
      prop.load(inputStream);
      String propertyClass = prop.getProperty("command.impl");
      System.out.println("property loaded: " + propertyClass);
      command = (Command) Class.forName(propertyClass).newInstance();
      System.out.println("Command loaded successfully: " + command + "\n");

    } catch (Exception e) {
      throw new RuntimeException("Dynamic loading of command has failed", e);
    }

    return command;
  }

  public Command getCommand() {
    return command;
  }

  public CommandResponse getResponse() {
    return response;
  }

  public CommandRequest getCommandRequestData() {
    return commandRequestData;
  }

  @Override
  public void setCommandRequestData(CommandRequest requestValues) {
    this.commandRequestData = requestValues;
  }
}
