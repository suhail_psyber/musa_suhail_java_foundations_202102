package com.psybergate.grad2021.core.ce1a.standard;

public interface Date {

  public boolean isLeapYear();

  public Date addDays(int numOfDays) throws InvalidDateException;

}
