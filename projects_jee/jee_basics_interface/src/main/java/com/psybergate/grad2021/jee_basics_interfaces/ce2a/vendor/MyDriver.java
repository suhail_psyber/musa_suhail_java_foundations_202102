package com.psybergate.grad2021.core.ce2a.vendor;

import java.sql.*;
import java.util.Properties;
import java.util.logging.Logger;

public class MyDriver implements Driver {

  //registering this driver with DriverManager on Class loading
  static {
    try {
      DriverManager.registerDriver(new MyDriver());
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  @Override
  public Connection connect(String url, Properties info) throws SQLException {
    if (!url.contains("jdbc:mydatabase://localhost:5432/")) {
      return null;
    }
    return new MyConnection(); //this is clearly wrong, what the heck did postgres do here???????
  }

  //default implementations
  @Override
  public boolean acceptsURL(String url) throws SQLException {
    return false;
  }

  @Override
  public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
    return new DriverPropertyInfo[0];
  }

  @Override
  public int getMajorVersion() {
    return 0;
  }

  @Override
  public int getMinorVersion() {
    return 0;
  }

  @Override
  public boolean jdbcCompliant() {
    return false;
  }

  @Override
  public Logger getParentLogger() throws SQLFeatureNotSupportedException {
    return null;
  }
}
