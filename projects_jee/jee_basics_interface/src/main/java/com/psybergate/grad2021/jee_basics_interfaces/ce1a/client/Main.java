package com.psybergate.grad2021.core.ce1a.client;

import com.psybergate.grad2021.core.ce1a.standard.Date;
import com.psybergate.grad2021.core.ce1a.standard.DateFactory;
import com.psybergate.grad2021.core.ce1a.standard.DateFactoryImplLoader;
import com.psybergate.grad2021.core.ce1a.standard.InvalidDateException;

public class Main {
  public static void main(String[] args) {
    /**
     * I get the Invalid Date Exception yet the object is still being created, why?
     */
    System.out.println(createDate(31, 1, 2000));
    System.out.println(createDate(31, 12, 2000));
    System.out.println(createDate(29, 2, 2000));
    //System.out.println(createDate(29, 2, 2100));
  }

  private static Date createDate(int day, int month, int year) {
    try {
      DateFactory dateFactory = DateFactoryImplLoader.getADateFactory();
      Date date = dateFactory.createANewDate(day, month, year);
      return date;
    } catch (InvalidDateException e) {
      throw new RuntimeException(e);
    }
  }
}
