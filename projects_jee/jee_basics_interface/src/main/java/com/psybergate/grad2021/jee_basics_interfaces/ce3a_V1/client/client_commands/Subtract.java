package com.psybergate.grad2021.core.ce3a_V1.client.client_commands;

import com.psybergate.grad2021.core.ce3a_V1.standard.Command;
import com.psybergate.grad2021.core.ce3a_V1.standard.CommandRequest;
import com.psybergate.grad2021.core.ce3a_V1.standard.CommandResponse;
import com.psybergate.grad2021.core.ce3a_V1.standard.StdCommandResponse;

public class Subtract implements Command {

  CommandResponse response;

  @Override
  public CommandResponse mainCommand(CommandRequest data) {
    System.out.println("Starting Command with name: " + this.getClass().getSimpleName());

    this.response = new StdCommandResponse();
    int result = calcResult(data);
    response.setResponse("" + result);
    return response;
  }

  @Override
  public void dummyMethod1() {
    System.out.println("dummyMethod1 processing...");
  }

  @Override
  public void dummyMethod2() {
    System.out.println("dummyMethod2 processing...");
  }

  /**
   * This method is the defining logic for each COMMAND and will thus vary per COMMAND
   *
   * @param values
   * @return int - sum of 2 values
   */
  public int calcResult(CommandRequest values) {
    int num1 = (int) values.getVal1();
    int num2 = (int) values.getVal2();

    return num1 - num2;
  }

}
