package com.psybergate.grad2021.core.ce3a_V3.client;

import com.psybergate.grad2021.core.ce3a_V3.client.client_commands.Add;
import com.psybergate.grad2021.core.ce3a_V3.standard.CommandEngine;
import com.psybergate.grad2021.core.ce3a_V3.standard.StdCommandInput;
import com.psybergate.grad2021.core.ce3a_V3.vendor.CommandEngineImpl;

public class Client {

  public static void main(String[] args) {

    CommandEngine ce = new CommandEngineImpl(); //Already Add or Subtract here
    ce.runEngine();
    ce.processCommand(new Add(), new StdCommandInput());

  }

}
