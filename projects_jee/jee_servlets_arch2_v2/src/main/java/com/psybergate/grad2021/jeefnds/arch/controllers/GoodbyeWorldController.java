package com.psybergate.grad2021.jeefnds.arch.controllers;

import com.psybergate.grad2021.jeefnds.arch.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

public class GoodbyeWorldController implements Controller {

    public void run(HttpServletRequest req, HttpServletResponse resp) throws Exception {

        PrintWriter printWriter = resp.getWriter();
        printWriter.println("<html>");
        printWriter.println("<body style=\"text-align:center;color:red\">");

        printWriter.println("<h1> GOODBYE WORLD </h1>");

        printWriter.println("</body>");
        printWriter.println("</html>");

    }

}
