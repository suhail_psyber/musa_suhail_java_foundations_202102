package com.psybergate.grad2021.jeefnds.arch;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@WebServlet(name = "Dispatcher", urlPatterns = {"/goodbye", "/hello", "/date"})
public class DispatcherServlet extends HttpServlet {

  private static final int VERSION = 10;

  private static final Map<String, Controller> CONTROLLERS = new HashMap<>();

  @Override
  public void init(ServletConfig config) throws ServletException {
    loadController();
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    try {
      String servletName = req.getServletPath();
      Controller controller = getController(servletName);
      controller.run(req, resp);

    } catch (Exception e) {
      throw new RuntimeException("invalid request: cannot find controller in version: " + VERSION);
    }
  }

  //Thread.currentThread().getContextClassLoader().getResourceAsStream() - if in default classpath [/WEB-INF/classes or /WEB-INF/lib]
  //getServletContext().getResourceAsStream("/WEB-INF/*") - if you make up a new classpath for it to follow starting from WEB-INF
  private void loadController() {
    try {
      InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("controller.properties");
      System.out.println("***************************inputStream = " + inputStream);
      Properties properties = new Properties();
      properties.load(inputStream);

      for (String propKey : properties.stringPropertyNames()) {
        String propValue = (String) properties.get(propKey);
        Controller cont = (Controller) Class.forName(propValue).newInstance();
        CONTROLLERS.put("/" + propKey, cont);
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private Controller getController(String servletName) {
    return CONTROLLERS.get(servletName);
  }

}
