package com.psybergate.grad2021.jeefnds.arch;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * DO NOT USE THIS AS A FUNCTIONAL INTERFACE
 */
public interface Controller {

  /**
   * The method called when the Dispatcher passes on a Request to a Controller
   * @param request
   * @param response
   * @throws Exception
   */
  void run(HttpServletRequest request, HttpServletResponse response) throws Exception;

}
