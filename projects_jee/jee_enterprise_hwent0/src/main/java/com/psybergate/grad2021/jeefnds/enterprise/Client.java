package com.psybergate.grad2021.jeefnds.enterprise;

import com.psybergate.grad2021.jeefnds.enterprise.domain.Audit;
import com.psybergate.grad2021.jeefnds.enterprise.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.domain.Services;
import com.psybergate.grad2021.jeefnds.enterprise.utils.DBM;

import java.time.LocalDate;

/**
 * Client layer utilising customer services
 */
public class Client {

  public static void main(String[] args) {
    DBM.dropExistingTables();
    DBM.createTables();

    Customer customer = new Customer(101,"Suhail", "Musa", LocalDate.of(1994,5,31));
    Audit audit = new Audit("This is my audit description message", LocalDate.now());
    Services services = new Services(customer, audit);

    /*Example of transaction failure on atomicity*/
    services.addCustomer(); //this has it's own commit
    services.addAudit(); //this has it's own commit - failure occurs in this method but addCustomer() has already been committed

  }

}
