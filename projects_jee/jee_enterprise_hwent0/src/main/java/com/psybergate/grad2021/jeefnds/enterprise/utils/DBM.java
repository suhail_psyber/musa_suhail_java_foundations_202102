package com.psybergate.grad2021.jeefnds.enterprise.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Resources Layer - Talks to the DB (Postgres)
 */
public class DBM {

  public static final String JDBC_DRIVER = "org.postgresql.Driver";

  private static final String DB_NAME = "jdbc:postgresql://localhost:5432/grad2021";

  private static final String DB_USER = "postgres";

  private static final String DB_PASSWORD = "admin";

  public static final String CUSTOMER_TBL_NAME = "hwent0_Customer"; //raw data

  public static final String AUDIT_TBL_NAME = "hwent0_Audit"; //raw data

  /**
   * Connect to DB
   */
  public static Connection getConnection() throws ClassNotFoundException, SQLException {
    Connection c;
    Class.forName(JDBC_DRIVER);
    c = DriverManager.getConnection(DB_NAME, DB_USER, DB_PASSWORD);
    c.setAutoCommit(false);
    return c;
  }

  /**
   * Drop Customer and Audit Tables
   */
  public static void dropExistingTables() {
    System.out.println("Dropping Tables...");
    try {
      Connection c = getConnection();
      Statement stmt = c.createStatement();

      String sql1 = "DROP TABLE IF EXISTS " + CUSTOMER_TBL_NAME + " CASCADE;";
      stmt.executeUpdate(sql1);

      sql1 = "DROP TABLE IF EXISTS " + AUDIT_TBL_NAME + " CASCADE;";
      stmt.executeUpdate(sql1);

      stmt.close();
      c.commit();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Tables dropped!");
  }

  /**
   * Create Customer and Audit Tables/Relations
   */
  public static void createTables() {
    System.out.println("Creating Tables...");
    try {
      Connection c = getConnection();
      Statement stmt = c.createStatement();

      String sql1 = "CREATE TABLE " + CUSTOMER_TBL_NAME + " (Customer_Number INT PRIMARY KEY, name TEXT, surname TEXT, dob DATE);";
      stmt.executeUpdate(sql1);

      sql1 = "CREATE TABLE " + AUDIT_TBL_NAME + " (description TEXT, audit_date DATE);";
      stmt.executeUpdate(sql1);

      stmt.close();
      c.commit();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Tables created!");
  }

}
