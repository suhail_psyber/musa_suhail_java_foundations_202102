package com.psybergate.grad2021.jeefnds.enterprise.domain;

import com.psybergate.grad2021.jeefnds.enterprise.utils.DBM;

import java.sql.Connection;
import java.sql.Statement;

/**
 * Business/Service Layer
 * This class provides services to the client and talks to the DBM
 */
public class Services {

  private Customer customer;

  private Audit audit;

  private static Connection connection;

  static {
    try {
      connection = DBM.getConnection();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public Services(Customer customer, Audit audit) {
    this.customer = customer;
    this.audit = audit;
  }

  public void addCustomer() {
    System.out.println("Inserting Customer data...");
    try {
      Statement stmt = connection.createStatement();

      String values = " VALUES('" + customer.getCustomerNumber() + "', '" + customer.getName() + "', '" + customer.getSurname() + "', '" + customer.getdOB() + "');";
      stmt.executeUpdate("INSERT INTO " + DBM.CUSTOMER_TBL_NAME + values);

      stmt.close();
      connection.commit();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Customer Data added successfully");
  }

  public void addAudit() {
    System.out.println("Inserting audit data...");
    try {
      Statement stmt = connection.createStatement();

      String values = " VALUES( '" + audit.getDescription() + "', '" + audit.getLoggingDate() + "');";
      /*This sql below is mistyped on purpose to show transaction failure on atomicity*/
      stmt.executeUpdate("INSETR INTO " + DBM.AUDIT_TBL_NAME + values);

      stmt.close();
      connection.commit();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Audit Data added successfully");
  }

}
