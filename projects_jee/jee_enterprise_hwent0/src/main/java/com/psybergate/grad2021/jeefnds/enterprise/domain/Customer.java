package com.psybergate.grad2021.jeefnds.enterprise.domain;

import java.time.LocalDate;

public class Customer {

  private int customerNumber;

  private String name;

  private String surname;

  private LocalDate dOB;

  public Customer(int customerNumber, String name, String surname, LocalDate dOB) {
    this.customerNumber = customerNumber;
    this.name = name;
    this.surname = surname;
    this.dOB = dOB;
  }

  public int getCustomerNumber() {
    return customerNumber;
  }

  public void setCustomerNumber(int customerNumber) {
    this.customerNumber = customerNumber;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public LocalDate getdOB() {
    return dOB;
  }

  public void setdOB(LocalDate dOB) {
    this.dOB = dOB;
  }
}
