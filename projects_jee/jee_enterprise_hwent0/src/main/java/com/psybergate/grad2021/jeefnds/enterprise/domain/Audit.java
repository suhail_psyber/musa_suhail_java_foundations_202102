package com.psybergate.grad2021.jeefnds.enterprise.domain;

import java.time.LocalDate;

public class Audit {

  private String description;

  private LocalDate loggingDate;

  public Audit(String description, LocalDate loggingDate) {
    this.description = description;
    this.loggingDate = loggingDate;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public LocalDate getLoggingDate() {
    return loggingDate;
  }

  public void setLoggingDate(LocalDate loggingDate) {
    this.loggingDate = loggingDate;
  }
}
