package com.psybergate.grad2021.jeefnds.arch;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "hello", urlPatterns = "/helloname")
public class HelloWorldServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter printWriter = resp.getWriter();
        printWriter.println("<html>");
        printWriter.println("<body style=\"text-align:center;color:red\">");

        //user will type http://localhost:8080/jee_servlets_arch1/hello?name=Suhail
        printWriter.println("<h1> HELLO " + req.getParameter("name").toUpperCase() + "</h1>");

        printWriter.println("</body>");
        printWriter.println("</html>");

    }

}
