package com.psybergate.grad2021.jeefnds.arch;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;

@WebServlet(name = "date", urlPatterns = "/date")
public class GetCurrentDateServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter printWriter = resp.getWriter();
        printWriter.println("<html>");
        printWriter.println("<body style=\"text-align:center; color:red;\">");

        printWriter.println("<h1> THE CURRENT DATE IS: " + LocalDate.now() + "</h1>");

        printWriter.println("</body>");
        printWriter.println("</html>");

    }

    private String yourAge() {
        return "26";
    }

}
