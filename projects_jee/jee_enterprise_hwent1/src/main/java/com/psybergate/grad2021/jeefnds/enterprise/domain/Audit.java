package com.psybergate.grad2021.jeefnds.enterprise.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "jee_ent_hw1_Audit")
public class Audit {

  @Column
  private String description;

  @Column
  private LocalDate loggingDate;

  public Audit(String description, LocalDate loggingDate) {
    this.description = description;
    this.loggingDate = loggingDate;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public LocalDate getLoggingDate() {
    return loggingDate;
  }

  public void setLoggingDate(LocalDate loggingDate) {
    this.loggingDate = loggingDate;
  }

  //Create Table jee_ent_hw1_Audit (description TEXT, LD DATE);
}
