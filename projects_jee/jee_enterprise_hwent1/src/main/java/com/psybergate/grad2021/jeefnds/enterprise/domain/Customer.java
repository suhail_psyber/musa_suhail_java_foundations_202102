package com.psybergate.grad2021.jeefnds.enterprise.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "jee_ent_hw1_Customer")
public class Customer {

  @Id
  @Column
  private int customerNumber;

  @Column
  private String name;

  @Column
  private String surname;

  @Column(name = "date_of_birth")
  private LocalDate dOB;

  public Customer(int customerNumber, String name, String surname, LocalDate dOB) {
    this.customerNumber = customerNumber;
    this.name = name;
    this.surname = surname;
    this.dOB = dOB;
  }

  public int getCustomerNumber() {
    return customerNumber;
  }

  public void setCustomerNumber(int customerNumber) {
    this.customerNumber = customerNumber;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public LocalDate getdOB() {
    return dOB;
  }

  public void setdOB(LocalDate dOB) {
    this.dOB = dOB;
  }

  //Create Table jee_ent_hw1_Customer (Customer_Number TEXT, name TEXT, surname TEXT, date_of_birth DATE);
}
