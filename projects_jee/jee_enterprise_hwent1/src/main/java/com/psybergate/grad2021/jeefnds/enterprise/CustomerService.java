package com.psybergate.grad2021.jeefnds.enterprise;

import com.psybergate.grad2021.jeefnds.enterprise.domain.Audit;
import com.psybergate.grad2021.jeefnds.enterprise.domain.Customer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 * Business/Service Layer
 * This class provides services to the client and talks to the DBM
 */
public class CustomerService {

  static {
    System.out.println("Trying to load CustomerService");
  }

  /*This is linked to a persistence.xml file to provide a config for the EntityManager*/
  /*You only have ONE FACTORY that can produce multiple Managers that in turn can have multiple transactions*/
  private static EntityManagerFactory EMF = Persistence.createEntityManagerFactory("hwent1");

  public void insertCustomerAndAudit(Customer customer, Audit audit) {
    System.out.println("............................... ");
    /*Create an EM - Entities are the abstractions we will persist "Customer" and "Audit"*/
    EntityManager entityManager = null;
    /*This is a transaction object. It will have one commit. Every step here is saved to the persistence context*/
    EntityTransaction eTransaction = null;

    try {
      entityManager = EMF.createEntityManager();
      eTransaction = entityManager.getTransaction();

      eTransaction.begin();

      /*These are sent to the persistence context*/
      entityManager.persist(customer);
      entityManager.persist(audit);

      /*The commit will take objects from the Persistence Context and then write to the DB and then commit*/
      /*This way, we aren't writing constantly and LOCKING the data*/
      eTransaction.commit();
    } catch (Exception e) {
      eTransaction.rollback();
      //e.printStackTrace();
    } finally {
      entityManager.close();
    }
  }

}
