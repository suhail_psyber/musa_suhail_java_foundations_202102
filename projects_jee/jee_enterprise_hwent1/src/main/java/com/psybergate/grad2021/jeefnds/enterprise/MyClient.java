package com.psybergate.grad2021.jeefnds.enterprise;

import com.psybergate.grad2021.jeefnds.enterprise.domain.Audit;
import com.psybergate.grad2021.jeefnds.enterprise.domain.Customer;

import java.time.LocalDate;

/**
 * Client layer utilising customer services
 */
public class MyClient {

  public static void main(String[] args) {
    Customer customer = new Customer(101,"Suhail", "Musa", LocalDate.of(1994,05,31));
    Audit audit = new Audit("This is my audit description message", LocalDate.now());

    CustomerService customerService = new CustomerService();
    customerService.insertCustomerAndAudit(customer, audit);
  }

}
