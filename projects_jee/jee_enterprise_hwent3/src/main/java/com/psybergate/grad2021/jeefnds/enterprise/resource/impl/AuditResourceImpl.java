package com.psybergate.grad2021.jeefnds.enterprise.resource.impl;

import com.psybergate.grad2021.jeefnds.enterprise.domain.Audit;
import com.psybergate.grad2021.jeefnds.enterprise.resource.AuditResource;
import com.psybergate.grad2021.jeefnds.enterprise.resource.EntityManagerResource;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

@Named("auditResource")
@ApplicationScoped
//@Dependent - can use this instead of @ApplicationScoped... the bean will take the context of the injection TARGET
public class AuditResourceImpl extends EntityManagerResource implements AuditResource {

  @Override
  public void save(Audit audit) {
    entityManager.persist(audit);
  }

}


