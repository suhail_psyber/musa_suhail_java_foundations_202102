package com.psybergate.grad2021.jeefnds.enterprise.resource;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class EntityManagerResource {

  //This EM is also a proxy... this is how they handle multithreading
  @PersistenceContext(unitName = "hwent3")
  protected EntityManager entityManager;

}
