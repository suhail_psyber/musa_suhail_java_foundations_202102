package com.psybergate.grad2021.jeefnds.enterprise.web.controller;

import com.psybergate.grad2021.jeefnds.enterprise.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.service.CustomerService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;

/**
 * This class will transform the request data to business data
 */
@Named
@ApplicationScoped //needed for CDI
public class ServiceController {

  @Inject
  private CustomerService customerService;

  public ServiceController() {
  }

  public void addCustomer(HttpServletRequest req, HttpServletResponse res) throws Exception {
    Customer customer = new Customer();
    customer.setCustomerNumber(req.getParameter("customerNumber"));
    customer.setName(req.getParameter("name"));
    customer.setSurname(req.getParameter("surname"));
    customer.setdOB(LocalDate.parse(req.getParameter("dOB")));

    System.out.println("customerService = " + getCustomerService().getClass());
    System.out.println("customer = " + customer);

    getCustomerService().registerCustomer(customer);

    res.getWriter().println("The customer was registered: " + customer); //hwent2 version
  }

  public CustomerService getCustomerService() {
    return customerService;
  }

}
