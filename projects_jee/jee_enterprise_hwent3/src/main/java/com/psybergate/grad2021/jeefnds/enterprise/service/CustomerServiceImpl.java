package com.psybergate.grad2021.jeefnds.enterprise.service;

import com.psybergate.grad2021.jeefnds.enterprise.domain.Audit;
import com.psybergate.grad2021.jeefnds.enterprise.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.resource.AuditResource;
import com.psybergate.grad2021.jeefnds.enterprise.resource.CustomerResource;

import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.LocalDate;

@Stateless //becomes @Transactional and @ApplicationScoped for homework 4
@TransactionManagement(TransactionManagementType.CONTAINER)
public class CustomerServiceImpl implements CustomerService {

  //Can use @Inject and @Named("customerResource") here instead of constructor injection
  private CustomerResource customerResource;

  private AuditResource auditResource;

  public CustomerServiceImpl() {
  }

  @Inject
  public CustomerServiceImpl(CustomerResource customerResource, AuditResource auditResource) {
    this.customerResource = customerResource;
    this.auditResource = auditResource;
  }

  @Override
  @Transactional
  //@Transactional(rollbackOn = Exception.class)
  //@Transactional is needed on PUBLIC bean methods - specify when we want a rollback... default is only runtime exceptions(this includes errors)
  public void registerCustomer(Customer customer) {
    System.out.println("***** customerResource Class = " + customerResource.getClass());
    System.out.println("***** auditResource Class = " + auditResource.getClass());

    Audit audit = new Audit("Customer successfully registered", LocalDate.now());

    customerResource.save(customer);
    auditResource.save(audit);
  }

}
