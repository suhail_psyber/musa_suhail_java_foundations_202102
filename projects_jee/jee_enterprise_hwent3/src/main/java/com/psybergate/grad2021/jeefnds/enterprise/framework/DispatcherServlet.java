package com.psybergate.grad2021.jeefnds.enterprise.framework;

import javax.enterprise.inject.spi.CDI;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

@WebServlet(name = "MyDispatcher", urlPatterns = {"/dispatcher/*"})
public class DispatcherServlet extends HttpServlet {

  private static final Map<String, Object> CONTROLLERS = new HashMap<>();

  private static final Map<String, Method> CONTROLLER_DECLARED_METHODS = new HashMap<>();

  @Override
  public void init() throws ServletException {
    loadProperties();
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    PrintWriter printWriter = resp.getWriter();

    printWriter.println("<html>");
    printWriter.println("<body style=\"text-align:center;color:red\">");

    try {
      System.out.println("*****getController(req).getClass() = " + getCDIController(req).getClass());
      System.out.println("*****getControllerMethod(req) = " + getControllerMethod(req));
      getControllerMethod(req).invoke(getCDIController(req), req, resp);
    } catch (Exception e) {
      e.printStackTrace();
    }

    printWriter.println("</body>");
    printWriter.println("</html>");
  }

  private Object getCDIController(HttpServletRequest req) {
    return CONTROLLERS.get(req.getPathInfo());
  }

  private Method getControllerMethod(HttpServletRequest req) {
    return CONTROLLER_DECLARED_METHODS.get(req.getPathInfo());
  }

  private void loadProperties() {
    try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("customer.properties")) {
      Properties properties = new Properties();
      properties.load(inputStream);

      Set<String> propKeys = properties.stringPropertyNames();
      String propValue;
      Object controller;
      Object cdiController;
      Method controllerMethod;

      for (String propKey : propKeys) {
        propValue = (String) properties.get(propKey); //ClassName#MethodName

        String[] strArray = propValue.split("#");
        String className = strArray[0];
        String methodName = strArray[1];

        cdiController = CDI.current().select(Class.forName(className)).get(); //grabbing the bean
        CONTROLLERS.put("/" + propKey, cdiController); //we store the CDIController (the controller that is available to the CDI container)

        controller = Class.forName(className).newInstance();
        controllerMethod = controller.getClass().getDeclaredMethod(methodName, HttpServletRequest.class, HttpServletResponse.class);
        CONTROLLER_DECLARED_METHODS.put("/" + propKey, controllerMethod);
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
