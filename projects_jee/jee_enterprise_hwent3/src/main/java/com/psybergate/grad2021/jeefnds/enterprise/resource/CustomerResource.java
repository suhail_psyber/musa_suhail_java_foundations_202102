package com.psybergate.grad2021.jeefnds.enterprise.resource;

import com.psybergate.grad2021.jeefnds.enterprise.domain.Customer;

public interface CustomerResource {

  public void save(Customer customer);

}
