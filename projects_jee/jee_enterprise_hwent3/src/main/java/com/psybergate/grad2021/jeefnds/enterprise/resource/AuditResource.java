package com.psybergate.grad2021.jeefnds.enterprise.resource;

import com.psybergate.grad2021.jeefnds.enterprise.domain.Audit;

public interface AuditResource {

  public void save(Audit audit);

}
