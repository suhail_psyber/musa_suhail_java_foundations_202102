package com.psybergate.grad2021.jeefnds.enterprise.service;

import com.psybergate.grad2021.jeefnds.enterprise.domain.Audit;
import com.psybergate.grad2021.jeefnds.enterprise.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.resource.AuditResource;
import com.psybergate.grad2021.jeefnds.enterprise.resource.CustomerResource;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

/**
 * Manages persistence to a DB - uses the persistence.xml to config the connection to the DB
 */
public class CustomerServices {

  private static final EntityManagerFactory EMF = Persistence.createEntityManagerFactory("hwent2");

  public void insertCustomerToDB(Customer customer, Audit audit) {
    System.out.println("*****************EMF.getClass() = " + EMF.getClass());

    EntityManager entityManager = null;
    EntityTransaction eTransaction = null;

    try {
      entityManager = EMF.createEntityManager();
      eTransaction = entityManager.getTransaction();

      eTransaction.begin();

      CustomerResource.customerPersistence(entityManager, customer);
      AuditResource.auditPersistence(entityManager, audit);

      eTransaction.commit();
    } catch (Exception e) {
      eTransaction.rollback();
    } finally {
      entityManager.close();
    }
  }

  //comment
}
