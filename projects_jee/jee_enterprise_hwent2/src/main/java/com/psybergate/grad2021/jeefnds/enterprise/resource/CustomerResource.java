package com.psybergate.grad2021.jeefnds.enterprise.resource;

import com.psybergate.grad2021.jeefnds.enterprise.domain.Customer;

import javax.persistence.EntityManager;

public class CustomerResource {

  public static void customerPersistence(EntityManager entityManager, Customer customer) {
    entityManager.persist(customer);
  }

  //comment
}
