package com.psybergate.grad2021.jeefnds.enterprise.resource;

import com.psybergate.grad2021.jeefnds.enterprise.domain.Audit;

import javax.persistence.EntityManager;

public class AuditResource {

  public static void auditPersistence(EntityManager entityManager, Audit audit) {
    entityManager.persist(audit);
  }

  //comment
}
