package com.psybergate.grad2021.jeefnds.enterprise.framework;

import com.psybergate.grad2021.jeefnds.enterprise.controller.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

@WebServlet(name = "MyDispatcher", urlPatterns = {"/dispatcher/*"})
public class DispatcherServlet extends HttpServlet {

  private static final Map<String, Controller> CONTROLLERS = new HashMap<>();

  private static final Map<String, Method> CONTROLLER_DECLARED_METHODS = new HashMap<>();

  @Override
  public void init() throws ServletException {
    loadProperties();
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    PrintWriter printWriter = resp.getWriter();

    printWriter.println("<html>");
    printWriter.println("<body style=\"text-align:center;color:red\">");

    try {
      getControllerMethod(req).invoke(getController(req), req, resp);
    } catch (Exception e) {
      e.printStackTrace();
    }

    printWriter.println("</body>");
    printWriter.println("</html>");
  }

  private Controller getController(HttpServletRequest req) {
    return CONTROLLERS.get(req.getPathInfo());
  }

  private Method getControllerMethod(HttpServletRequest req) {
    return CONTROLLER_DECLARED_METHODS.get(req.getPathInfo());
  }

  private void loadProperties() {
    try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("customer.properties")) {
      Properties properties = new Properties();
      properties.load(inputStream);

      Set<String> propKeys = properties.stringPropertyNames();
      String propValue; //Some Controller's appropriate Method
      Controller controller;
      Method controllerMethod;

      for (String propKey : propKeys) {
        propValue = (String) properties.get(propKey); //ClassName#MethodName

        String[] strArray = propValue.split("#");
        String className = strArray[0];
        String methodName = strArray[1];

        controller = (Controller) Class.forName(className).newInstance();
        controllerMethod = controller.getClass().getDeclaredMethod(methodName, HttpServletRequest.class, HttpServletResponse.class);

        CONTROLLERS.put("/" + propKey, controller);
        CONTROLLER_DECLARED_METHODS.put("/" + propKey, controllerMethod);
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  //comment
}
