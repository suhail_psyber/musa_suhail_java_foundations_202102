package com.psybergate.grad2021.jeefnds.enterprise.controller;

import com.psybergate.grad2021.jeefnds.enterprise.domain.Audit;
import com.psybergate.grad2021.jeefnds.enterprise.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.service.CustomerServices;

import javax.enterprise.context.ApplicationScoped;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.time.LocalDate;

/**
 * This class will transform the request data to business data
 */
@ApplicationScoped
public class ServiceController implements Controller {

  public void addCustomer(HttpServletRequest req, HttpServletResponse res) {
    Customer customer = new Customer();
    customer.setCustomerNumber(Integer.parseInt(req.getParameter("customerNumber")));
    customer.setName(req.getParameter("name"));
    customer.setSurname(req.getParameter("surname"));
    customer.setdOB(LocalDate.parse(req.getParameter("dOB")));

    Audit audit = new Audit("This is my audit description message", LocalDate.now());

    try {
      CustomerServices customerServices = new CustomerServices();
      customerServices.insertCustomerToDB(customer, audit);
      PrintWriter out = res.getWriter();
      out.println("Customer Added Successfully: " + customer);
    } catch (Exception e) {
      e.printStackTrace();
    }

  }

  //comment
}
