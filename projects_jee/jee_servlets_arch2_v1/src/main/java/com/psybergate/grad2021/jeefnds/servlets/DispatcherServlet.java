package com.psybergate.grad2021.jeefnds.servlets;

import com.psybergate.grad2021.jeefnds.servlets.controllers.Controller;
import com.psybergate.grad2021.jeefnds.servlets.controllers.GetCurrentDateController;
import com.psybergate.grad2021.jeefnds.servlets.controllers.GoodbyeWorldController;
import com.psybergate.grad2021.jeefnds.servlets.controllers.HelloWorldController;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "Dispatcher", urlPatterns = {"/goodbyeworld", "/helloworld", "/date"})
public class DispatcherServlet extends HttpServlet {

  private static final Map<String, Controller> CONTROLLERS = new HashMap<>();
  //We shouldn't have state, so:
  // move to start of doGet?
  // or just create a new object by using an if (servletName.equals("/date")) { new GetCurrentDateController().run()}

  static {
    CONTROLLERS.put("/goodbyeworld", new GoodbyeWorldController());
    CONTROLLERS.put("/helloworld", new HelloWorldController());
    CONTROLLERS.put("/date", new GetCurrentDateController());
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    try {
      String servletName = req.getServletPath();
      Controller controller = getController(servletName);
      controller.run(req, resp);

    } catch (Exception e) {
      throw new RuntimeException("invalid request: cannot find controller");
    }
  }

  private Controller getController(String servletName) {
    return CONTROLLERS.get(servletName);
  }

  /* http://localhost/webappname/servletsname/extrainfo1/extrainfo2?paramkey=paramvalue
   *
   * req.getServletPath() - returns: "/servletname" ---> if servletname urlpatterns is "/*", it returns null ... does not return extra info or param
   * req.getPathInfo() - returns: "/extrainfo1/extrainfo2" ---> even if Dispatcher listens for "servletname/*"
   * req.getParameter("paramkey") - returns: "/paramvalue"
   *
   * */

}
