package com.psybergate.grad2021.jeefnds.web.framework;

import com.psybergate.grad2021.jeefnds.web.controller.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;


@WebServlet(name = "MyDispatcher", urlPatterns = {"/dispatcher/*"})
public class DispatcherServlet extends HttpServlet {

  private static final int VERSION = 10;

  private static final Map<String, Controller> CONTROLLERS = new HashMap<>();

  private static final Map<String, Method> CONTROLLER_DECLARED_METHODS = new HashMap<>();

  @Override
  public void init() throws ServletException {
    loadControllerAndMethods();
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    PrintWriter printWriter = resp.getWriter();

    printWriter.println("<html>");
    printWriter.println("<body style=\"text-align:center;color:red\">");

    try {
      System.out.println("*************************Trying to invoke method********************************");
      printWriter.println(getControllerMethod(req).invoke(getController(req), req));
      System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!Method invoked!!!!!!!!!!!!!!!!!!!!!!!!");
    } catch (Exception e) {
      e.printStackTrace();
    }

    printWriter.println("</body>");
    printWriter.println("</html>");
  }

  private Controller getController(HttpServletRequest req) {
    return CONTROLLERS.get(req.getPathInfo());
  }

  private Method getControllerMethod(HttpServletRequest req) {
    System.out.println(CONTROLLERS);
    System.out.println(CONTROLLER_DECLARED_METHODS);
    System.out.println(req.getServletPath()); //returns /dispatcher
    System.out.println(req.getPathInfo()); //returns /addCustomer
    return CONTROLLER_DECLARED_METHODS.get(req.getPathInfo());
  }

  private void loadControllerAndMethods() {
    try (InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("customer.properties")) {
      Properties properties = new Properties();
      properties.load(inputStream);

      Set<String> propKeys = properties.stringPropertyNames();
      String propValue; //Some Controller's appropriate Method
      Controller controller;
      Method controllerMethod;

      for (String propKey : propKeys) {
        propValue = (String) properties.get(propKey); //ClassName#MethodName

        String[] strArray = propValue.split("#");
        String className = strArray[0];
        String methodName = strArray[1];

        controller = (Controller) Class.forName(className).newInstance();
        controllerMethod = controller.getClass().getDeclaredMethod(methodName, HttpServletRequest.class);

        CONTROLLERS.put("/" + propKey, controller);
        CONTROLLER_DECLARED_METHODS.put("/" + propKey, controllerMethod);
      }

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
