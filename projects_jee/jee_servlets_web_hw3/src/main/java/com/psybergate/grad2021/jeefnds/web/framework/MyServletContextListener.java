package com.psybergate.grad2021.jeefnds.web.framework;

import com.psybergate.grad2021.jeefnds.web.domain.DBM;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class MyServletContextListener implements ServletContextListener {


  @Override
  public void contextInitialized(ServletContextEvent sce) {
    DBM.setupTables();
    System.out.println("*********SCE has been initialized************");
  }

}
