package com.psybergate.grad2021.jeefnds.web.domain;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Handles all DB related connections / queries
 */
public class DBM {

  public static final String JDBC_DRIVER = "org.postgresql.Driver";

  private static final String DB_NAME = "jdbc:postgresql://localhost:5432/grad2021";

  private static final String DB_USER = "postgres";

  private static final String DB_PASSWORD = "admin";

  private static final String CUSTOMER_TBL_NAME = "jee_servlets_web_hw3_Customers"; //raw data

  public static void setupTables() {
    dropCustomerTable();
    createCustomerTable();
  }

  private static Connection connectToDb() throws ClassNotFoundException, SQLException {
    Connection c;
    Class.forName(JDBC_DRIVER);
    c = DriverManager.getConnection(DB_NAME, DB_USER, DB_PASSWORD);
    c.setAutoCommit(false);
    return c;
  }

  private static void dropCustomerTable() {
    System.out.println("Dropping Tables...");
    try {
      Connection c = connectToDb();
      Statement stmt = c.createStatement();

      String sql1 = "DROP TABLE IF EXISTS " + CUSTOMER_TBL_NAME + " CASCADE;";
      stmt.executeUpdate(sql1);

      stmt.close();
      c.commit();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Tables dropped!");
  }

  private static void createCustomerTable() {
    System.out.println("Creating Customer Tables...");
    try {
      Connection c = connectToDb();
      Statement stmt = c.createStatement();

      String sql1 = "CREATE TABLE " + CUSTOMER_TBL_NAME + " (Customer_Number BIGINT PRIMARY KEY, name TEXT, surname TEXT, dob DATE);";
      stmt.executeUpdate(sql1);

      stmt.close();
      c.commit();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Customer Table created!");
  }

  public static void insertCustomersToTableInDB(Customer c1) {
    System.out.println("Inserting data...");
    try {
      Connection c = connectToDb();
      Statement stmt = c.createStatement();

      String values = " VALUES('" + c1.getCustomerNum() + "', '" + c1.getName() + "', '" + c1.getSurname() + "', '" + c1.getdOB() + "');";
      stmt.executeUpdate("INSERT INTO " + CUSTOMER_TBL_NAME + values);

      stmt.close();
      c.commit();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Data added successfully");
  }

}
