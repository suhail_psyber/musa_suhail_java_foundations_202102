package com.psybergate.grad2021.jeefnds.web.controller;

import com.psybergate.grad2021.jeefnds.web.domain.Customer;
import com.psybergate.grad2021.jeefnds.web.domain.DBM;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;

/**
 * This class will perform all methods on a customer and persist the data to a DB (postgres)
 */
public class CustomerServices implements Controller {

  /**
   * Adds a Customer to the DB through the information passed in the request
   */
  public static void addCustomer(HttpServletRequest req) {
    Long customerNumber = Long.valueOf(req.getParameter("customerNumber"));
    String name = req.getParameter("name");
    String surname = req.getParameter("surname");
    LocalDate dOB = LocalDate.parse(req.getParameter("dOB")); //LocalDate.parse can convert a text String "yyyy-mm-dd" into type LocalDate

    Customer customer = new Customer(customerNumber, name, surname, dOB);
    DBM.insertCustomersToTableInDB(customer);

    System.out.println("Added customer to DB with details: " + customer);
  }

}
