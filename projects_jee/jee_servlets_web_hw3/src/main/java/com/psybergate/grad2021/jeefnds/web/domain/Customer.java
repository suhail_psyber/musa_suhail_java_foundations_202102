package com.psybergate.grad2021.jeefnds.web.domain;

import java.time.LocalDate;

public class Customer {

  private Long customerNum;

  private String name;

  private String surname;

  private LocalDate dOB;

  public Customer(Long customerNum, String name, String surname, LocalDate dOB) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dOB = dOB;
  }

  public Long getCustomerNum() {
    return customerNum;
  }

  public void setCustomerNum(Long customerNum) {
    this.customerNum = customerNum;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public LocalDate getdOB() {
    return dOB;
  }

  public void setdOB(LocalDate dOB) {
    this.dOB = dOB;
  }

  @Override
  public String toString() {
    return "{" + "customerNum=" + customerNum + ", name='" + name + '\'' + ", surname='" + surname + '\'' + ", dOB=" + dOB + '}';
  }
}
