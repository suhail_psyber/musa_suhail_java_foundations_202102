package com.psybergate.grad2021.jeefnds.enterprise.resource.impl;

import com.psybergate.grad2021.jeefnds.enterprise.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.resource.CustomerResource;
import com.psybergate.grad2021.jeefnds.enterprise.resource.EntityManagerResource;

import javax.enterprise.context.Dependent;

@Dependent
public class CustomerResourceImpl extends EntityManagerResource implements CustomerResource {

  @Override
  public void save(Customer customer) {
    entityManager.persist(customer);
  }
}

