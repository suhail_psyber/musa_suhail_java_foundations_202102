package com.psybergate.grad2021.jeefnds.enterprise.domain;

import javax.persistence.*;
import java.time.LocalDate;

@Entity(name = "jee_ent_hw4_Audit")
public class Audit {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column
  private Long id;

  @Column
  private String description;

  @Column
  private LocalDate loggingDate;

  public Audit() {
  }

  public Audit(String description, LocalDate loggingDate) {
    this.description = description;
    this.loggingDate = loggingDate;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public LocalDate getLoggingDate() {
    return loggingDate;
  }

  public void setLoggingDate(LocalDate loggingDate) {
    this.loggingDate = loggingDate;
  }

}
