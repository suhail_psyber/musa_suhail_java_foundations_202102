package com.psybergate.grad2021.jeefnds.enterprise.domain;

import javax.persistence.*;
import java.time.LocalDate;

@Entity(name = "jee_ent_hw4_Customer")
public class Customer {

  @Id
  @Column(name = "Customer_Number")
  private String customerNumber;

  @Column
  private String name;

  @Column
  private String surname;

  @Column(name = "date_of_birth")
  private LocalDate dOB;

  public Customer() {
  }

  public Customer(String customerNumber, String name, String surname, LocalDate dOB) {
    this.customerNumber = customerNumber;
    this.name = name;
    this.surname = surname;
    this.dOB = dOB;
  }

  public String getCustomerNumber() {
    return customerNumber;
  }

  public void setCustomerNumber(String customerNumber) {
    this.customerNumber = customerNumber;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public LocalDate getdOB() {
    return dOB;
  }

  public void setdOB(LocalDate dOB) {
    this.dOB = dOB;
  }

  @Override
  public String toString() {
    return "{" + "customerNumber = " + customerNumber + ", Full Name = " + name + ' ' + surname + ", dOB = " + dOB + "}";
  }

}
