package com.psybergate.grad2021.jeefnds.enterprise.service;

import com.psybergate.grad2021.jeefnds.enterprise.domain.Audit;
import com.psybergate.grad2021.jeefnds.enterprise.domain.Customer;
import com.psybergate.grad2021.jeefnds.enterprise.resource.AuditResource;
import com.psybergate.grad2021.jeefnds.enterprise.resource.CustomerResource;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import java.time.LocalDate;

@Transactional
@ApplicationScoped
public class CustomerServiceImpl implements CustomerService {

  private CustomerResource customerResource;

  private AuditResource auditResource;

  public CustomerServiceImpl() {
  }

  @Inject
  public CustomerServiceImpl(CustomerResource customerResource, AuditResource auditResource) {
    this.customerResource = customerResource;
    this.auditResource = auditResource;
  }

  @Override
  public void registerCustomer(Customer customer) {
    System.out.println("***** customerResource Class = " + customerResource.getClass());
    System.out.println("***** auditResource Class = " + auditResource.getClass());

    Audit audit = new Audit("Customer successfully registered", LocalDate.now());

    customerResource.save(customer);
    auditResource.save(audit);
  }

}
