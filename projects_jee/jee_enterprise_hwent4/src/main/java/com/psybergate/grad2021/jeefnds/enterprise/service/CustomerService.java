package com.psybergate.grad2021.jeefnds.enterprise.service;

import com.psybergate.grad2021.jeefnds.enterprise.domain.Customer;

public interface CustomerService {

  public void registerCustomer(Customer customer);

}
