package dir2.hw1b;

import java.util.*;
import dir1.hw1a.*;

public class StringReverse {

	public static void main(String[] args) {
		reverse();
	}

	public static void reverse() {
		System.out.println("Your Name is reverse is: " + MyStringUtils.reverse("Suhail"));
		
		//method below receives an input from the user
		// System.out.println("Enter your Name: ");
		// Scanner inputNameObject = new Scanner(System.in);
		// String inputNameStr = inputNameObject.nextLine();
		// System.out.println("Your Name is reverse is: " + MyStringUtils.reverse(inputNameStr)); //PS: if it's a method, we need the class name the method is in. This is treated very differently than a class!!
	}										 //we import classes, not methods. It won't just search for the method sequentially like it does when we do	
}											 //Customer customer = new Customer() // where this would search the current and imported files for this class