package com.psybergate.grad21.core.hw2a_JAR.src.dir1.hw1a;

public class MyStringUtils {

	public static String reverse(String name) {
		String revStr = "";

		for(int i = name.length()-1; i >= 0; i--) {
			revStr += Character.toString(name.charAt(i)); //java.lang.* is always imported. Also, we don't need to specify classpath because it is in rt.jar OR ext...find it
		}

		return(revStr);
	}
	
}