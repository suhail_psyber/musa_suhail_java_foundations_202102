package com.psybergate.grad2021.core.generics.hw1b.concrete;

import com.psybergate.grad2021.core.generics.hw1b.AbstractClasses.OffensivePlayer;

public class Midfielder extends OffensivePlayer {

  public static boolean hasGloves = false;

  public Midfielder(String name) {
    super(name);
  }

}
