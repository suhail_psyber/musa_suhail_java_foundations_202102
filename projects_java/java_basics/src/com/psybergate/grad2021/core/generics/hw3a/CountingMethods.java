package com.psybergate.grad2021.core.generics.hw3a;

import java.util.*;

public class CountingMethods {

  public static int countEvenNumbers(Collection<Integer> collection) {
    int count = 0;

    for (Integer integer : collection) {
      if (integer % 2 == 0) {
        count++;
      }
    }

    return count;
  }

  public static int countOddNumbers(Collection<Integer> collection) {
    int count = 0;

    for (Integer integer : collection) {
      if (integer % 2 == 1) {
        count++;
      }
    }

    return count;
  }

  public static int countPrimeNumbers(Collection<Integer> collection) {
    int count = 0;

    for (Integer integer : collection) {

    }

    return count;
  }

  public static int countCStrings(Collection<String> collection) {
    int count = 0;

    for (String string : collection) {
      if (string.charAt(0) == 'c') {
        count++;
      }
    }

    return count;
  }

  //"dangers" of raw types (why we use generics) - could have added a non-String type in my collection
  public static int countCStrings2(Collection collection) {
    int count = 0;

    //cannot use enhanced for loop if we don't have generics
    for (Iterator iter = collection.iterator(); iter.hasNext(); ) {
      Object obj = iter.next();
      String str = (String) obj; //this is the line that goes wrong when we use a RAW Type collection
      if (str.charAt(0) == 'c') {
        count++;
      }
    }

    return count;
  }

  public static int countNegativeBalances(Collection<Account> collection) {
    int count = 0;

    for (Account account : collection) {
      if (account.getBalance() < 0) {
        count++;
      }
    }

    return count;
  }

}
