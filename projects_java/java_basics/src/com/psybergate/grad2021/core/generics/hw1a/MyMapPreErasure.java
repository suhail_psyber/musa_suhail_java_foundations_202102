package com.psybergate.grad2021.core.generics.hw1a;

public class MyMapPreErasure<K, V> {

  private K key;
  private V value;

  public MyMapPreErasure(K key, V value) {
    this.key = key;
    this.value = value;
  }

  public K getKey() {
    return key;
  }

  public V getValue() {
    return value;
  }

  public void setKey(K key) {
    this.key = key;
  }

  public void setValue(V value) {
    this.value = value;
  }

  /**
   * Try javap on this file and you should get the post erasure file
   */
}
