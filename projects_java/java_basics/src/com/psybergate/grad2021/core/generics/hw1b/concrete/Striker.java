package com.psybergate.grad2021.core.generics.hw1b.concrete;

import com.psybergate.grad2021.core.generics.hw1b.AbstractClasses.OffensivePlayer;

public class Striker extends OffensivePlayer {

  public static boolean hasGloves = false;

  public Striker(String name) {
    super(name);
  }

}
