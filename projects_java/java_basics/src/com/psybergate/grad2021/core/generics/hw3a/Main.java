package com.psybergate.grad2021.core.generics.hw3a;

import java.util.Arrays;
import java.util.Collection;
import java.util.TreeSet;

import static com.psybergate.grad2021.core.generics.hw3a.CountingMethods.*;

public class Main {

  public static void main(String[] args) {
    Collection intCollection1 = new TreeSet(Arrays.asList(1, 2, 3, 4, 5, 13, 19, 100));
    System.out.println("countEvenNumbers(intCollection1) = " + countEvenNumbers(intCollection1));
    System.out.println("countOddNumbers(intCollection1) = " + countOddNumbers(intCollection1));
    System.out.println("countPrimeNumbers(intCollection1) = " + countPrimeNumbers(intCollection1));
    //System.out.println("countCStrings(intCollection1) = " + countCStrings(intCollection1)); //RUNTIME ERROR - CLASS CAST EXCEPTION
    System.out.println("");

    Collection<Integer> intCollection2 = Arrays.asList(1, 2, 3, 4, 5, 13, 19, 100); //read up
    System.out.println("countEvenNumbers(intCollection2) = " + countEvenNumbers(intCollection2));
    System.out.println("countOddNumbers(intCollection2) = " + countOddNumbers(intCollection2));
    System.out.println("countPrimeNumbers(intCollection2) = " + countPrimeNumbers(intCollection2));
    //System.out.println("countCStrings(intCollection2) = " + countCStrings(intCollection2)); //COMPILETIME ERROR - Incompatible Types
    System.out.println("");

    Collection<String> strCollection1 = Arrays.asList("cbc", "cba", "ccc", "ab", "abc", "xyz");
    //System.out.println("countEvenNumbers(strCollection1) = " + countEvenNumbers(strCollection1)); //COMPILETIME ERROR - Incompatible Types
    //System.out.println("countOddNumbers(strCollection1) = " + countOddNumbers(strCollection1)); //COMPILETIME ERROR - Incompatible Types
    //System.out.println("countPrimeNumbers(strCollection1) = " + countPrimeNumbers(strCollection1)); //COMPILETIME ERROR - Incompatible Types
    System.out.println("countCStrings(strCollection1) = " + countCStrings(strCollection1));
    System.out.println("");

    Collection<Account> accCollection1 = Arrays.asList(new Account("1a",-5000),new Account("2a",5000),new Account("1a",-1),new Account("1a",0));
    System.out.println("countNegativeBalances(accCollection1) = " + countNegativeBalances(accCollection1));
  }
}
