package com.psybergate.grad2021.core.generics.hw1b.AbstractClasses;

public class Player extends Person {

  public boolean hasGloves;

  private String name;

  public Player(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

}
