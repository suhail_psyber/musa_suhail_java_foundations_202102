package com.psybergate.grad2021.core.generics.hw1b.concrete;

import com.psybergate.grad2021.core.generics.hw1b.AbstractClasses.DefensivePlayer;

public class Goalkeeper extends DefensivePlayer {

  public static boolean hasGloves = true;

  public Goalkeeper(String name) {
    super(name);
  }

}
