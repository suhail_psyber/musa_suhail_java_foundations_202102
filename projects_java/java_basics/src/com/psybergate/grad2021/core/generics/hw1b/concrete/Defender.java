package com.psybergate.grad2021.core.generics.hw1b.concrete;

import com.psybergate.grad2021.core.generics.hw1b.AbstractClasses.DefensivePlayer;

public class Defender extends DefensivePlayer {

  public static boolean hasGloves = false;

  public Defender(String name) {
    super(name);
  }

}
