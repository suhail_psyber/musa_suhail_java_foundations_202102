package com.psybergate.grad2021.core.generics.hw3b.counters;

public class EvenNumbers implements Condition<Integer> {

  public boolean isSatisfiedBy(Integer num) {
    return (num % 2 == 0);
  }

}
