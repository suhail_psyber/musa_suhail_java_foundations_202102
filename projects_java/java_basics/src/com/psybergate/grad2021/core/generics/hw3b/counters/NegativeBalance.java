package com.psybergate.grad2021.core.generics.hw3b.counters;

import com.psybergate.grad2021.core.generics.hw3a.Account;

public class NegativeBalance implements Condition<Account> {

  public boolean isSatisfiedBy(Account account) {
    return (account.getBalance() < 0);
  }

}
