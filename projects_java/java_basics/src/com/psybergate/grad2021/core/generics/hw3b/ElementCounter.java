package com.psybergate.grad2021.core.generics.hw3b;

import com.psybergate.grad2021.core.generics.hw3a.Account;
import com.psybergate.grad2021.core.generics.hw3b.counters.*;

import java.util.Arrays;
import java.util.Collection;

public final class ElementCounter {

  public static void main(String[] args) {
    System.out.println("countAnything(Arrays.asList(1, 2, 3, 4), new Even()) = " + countAnything(Arrays.asList(1, 2, 3, 4), new EvenNumbers()));

    System.out.println("countAnything(Arrays.asList(1, 2, 3, 4), new OddNumbers()) = " + countAnything(Arrays.asList(1, 2, 3, 4), new OddNumbers()));

    System.out.println("countAnything(Arrays.asList(accountList, new Account()) = " + countAnything(Arrays.asList(new Account("101",-1),
            new Account("102",-500), new Account("103",0), new Account("104",1000)), new NegativeBalance()));

    System.out.println("countAnything(Arrays.asList(1, 2, 3, 4), new PrimeNumbers()) = " + countAnything(Arrays.asList(1, 2, 3, 4), new PrimeNumbers()));

    System.out.println("countAnything(Arrays.asList(\"cc\", \"ab\", \"Cab\", \"rf\"), new StringStartsWithC()) = " + countAnything(Arrays.asList("cc", "ab", "Cab", "rf"),
            new StringStartsWithC()));
  }

  public static <T,M extends Condition> int countAnything(Collection<T> collection, M condition) {
    int count = 0;

    for (T element : collection) {
      if (condition.isSatisfiedBy(element)) {
        count++;
      }
    }

    return count;
  }
}
