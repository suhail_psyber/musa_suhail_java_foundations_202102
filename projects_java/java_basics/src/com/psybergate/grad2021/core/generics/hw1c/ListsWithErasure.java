package com.psybergate.grad2021.core.generics.hw1c;

import java.util.ArrayList;
import java.util.List;

public class ListsWithErasure {

  public static void main(String[] args) {
    List myList = new ArrayList();
    myList.add("abc");
    myList.add("mno");
    myList.add("xyz");
    System.out.println("myList = " + myList);
    System.out.println("method1(myList) = " + method1(myList));

  }

  /*Original method*/
  private static List<String> method1(List<String> list) {
    List reveredStringList = new ArrayList();

    for (String s : list) {
      reveredStringList.add(reverse(s));
    }
    return reveredStringList;
  }

  /*This method looks like below to the JVM*/
  private static List method1b(List list) {
    List reveredStringList = new ArrayList();

    for (Object s : list) {
      reveredStringList.add(reverse((String)s));
    }
    return reveredStringList;
  }

  /*utility method*/
  public static String reverse(String str) {
    String revStr = "";

    for (int i = str.length() - 1; i >= 0; i--) {
      revStr += Character.toString(str.charAt(i));
    }

    return (revStr);
  }
}
