package com.psybergate.grad2021.core.generics.hw3b.counters;

public class PrimeNumbers implements Condition<Integer> {

  @Override
  public boolean isSatisfiedBy(Integer element) {

    boolean isPrime = true;

    for (int i = 2; i <= Math.sqrt(element); i++) {
      if (element % i == 0) {
        isPrime = false;
        return isPrime;
      }
    }

    return isPrime;

  }
}