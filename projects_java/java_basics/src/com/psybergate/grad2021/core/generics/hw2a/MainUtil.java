package com.psybergate.grad2021.core.generics.hw2a;

import java.util.ArrayList;
import java.util.List;

import static com.psybergate.grad2021.core.generics.hw2a.GreaterThan.*;

public class MainUtil {

  public static void main(String[] args) {
    /**
     * Using the method from a JAR - the compiler keeps track of some metadata i.e it knows that the list that should be passed in should adhere to
     * myList.E = list<Integer> ... A list declared as a collection of Strings will fail and so would a list containing anything not considered an
     * Integer.
     */
    List integerList = new ArrayList<>();
    integerList.add(999);
    integerList.add(1000);
    integerList.add(1001);
    integerList.add(9999);

    System.out.println("GreaterThan.greaterThan1000(integerList) = " + greaterThan1000(integerList));

    //List stringList = new ArrayList();
    //stringList.add("999");
    //stringList.add("1000");
    //stringList.add("1001");
    //stringList.add("9999");
    //
    //System.out.println("GreaterThan.greaterThan1000(integerList) = " + greaterThan1000(stringList));
    /*Gives RUNTIME ERROR - Class Cast Exception*/

    //List<String> stringList2 = new ArrayList();
    //stringList2.add("999");
    //stringList2.add("1000");
    //stringList2.add("1001");
    //stringList2.add("9999");
    //
    //System.out.println("GreaterThan.greaterThan1000(integerList) = " + greaterThan1000(stringList2));
    ///*Gives COMPILATION ERROR - Expects: List<Integer> ... Provided: List<String>*/

  }

}
