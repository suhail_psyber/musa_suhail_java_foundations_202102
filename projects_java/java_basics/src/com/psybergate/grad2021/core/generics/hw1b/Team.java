package com.psybergate.grad2021.core.generics.hw1b;

import com.psybergate.grad2021.core.generics.hw1b.AbstractClasses.DefensivePlayer;
import com.psybergate.grad2021.core.generics.hw1b.AbstractClasses.OffensivePlayer;
import com.psybergate.grad2021.core.generics.hw1b.AbstractClasses.Player;
import com.psybergate.grad2021.core.generics.hw1b.concrete.Defender;
import com.psybergate.grad2021.core.generics.hw1b.concrete.Goalkeeper;
import com.psybergate.grad2021.core.generics.hw1b.concrete.Midfielder;
import com.psybergate.grad2021.core.generics.hw1b.concrete.Striker;

import java.util.ArrayList;
import java.util.List;

public class Team {

  public static void main(String[] args) {

    /**
     * A lot of these don't work when the REFERENCE TYPE is abstracted to high. This is because the variable could be REASSIGNED to an object that
     * does not meet the specific criteria, eg gaolkeeper1 = new Striker<>(); after the declaration
     */

    /*List of Players*/
    Player goalkeeper1 = new Goalkeeper("Edwin");
    Goalkeeper goalkeeper2 = new Goalkeeper("De Gea");

    Player defender1 = new Defender("Sergio");
    DefensivePlayer defender2 = new Defender("Pique");
    Defender defender3 = new Defender("Vidic");
    Defender defender4 = new Defender("Shaw");

    Player midfielder1 = new Midfielder("Bruno");
    OffensivePlayer midfielder2 = new Midfielder("Ronaldo");
    Midfielder midfielder3 = new Midfielder("Xavi");
    Midfielder midfielder4 = new Midfielder("Iniesta");

    Player striker1 = new Striker("Messi");
    OffensivePlayer striker2 = new Striker("Rashford");
    Striker striker3 = new Striker("Aguero");
    Striker striker4 = new Striker("Eto");

    /*This list can take any type of player*/
    List<Player> myTeam = new ArrayList<>(11);
    myTeam.add(goalkeeper1);
    myTeam.add(defender1);
    myTeam.add(defender2);
    myTeam.add(defender3);
    myTeam.add(defender4);
    myTeam.add(midfielder1);
    myTeam.add(midfielder2);
    myTeam.add(midfielder3);
    myTeam.add(striker1);
    myTeam.add(striker2);
    myTeam.add(striker3);

    /*This list can only take offensive players - strikers and midfielders (OFFENSIVE PLAYERS)*/
    List<OffensivePlayer> myOffensivePlayers = new ArrayList<>(6);
    myOffensivePlayers.add(midfielder2);
    myOffensivePlayers.add(midfielder3);
    myOffensivePlayers.add(midfielder4);
    myOffensivePlayers.add(striker2);
    myOffensivePlayers.add(striker3);
    myOffensivePlayers.add(striker4);
    //myOffensivePlayers.add(goalkeeper1);
    //myOffensivePlayers.add(goalkeeper2);
    //myOffensivePlayers.add(defender1);
    //myOffensivePlayers.add(defender2);
    //myOffensivePlayers.add(defender3);
    //myOffensivePlayers.add(defender4);
    //myOffensivePlayers.add(midfielder1);
    //myOffensivePlayers.add(striker1);

    /*This is a list of only GOALKEEPERS - goalkeeper1 is of type PLAYER and thus will not compile*/
    List<Goalkeeper> myGoalKeepers = new ArrayList<>(2);
    myGoalKeepers.add(goalkeeper2);
    //myGoalKeepers.add(goalkeeper1);
    //myGoalKeepers.add(defender1);
    //myGoalKeepers.add(defender2);
    //myGoalKeepers.add(defender3);
    //myGoalKeepers.add(defender4);
    //myGoalKeepers.add(midfielder1);
    //myGoalKeepers.add(midfielder2);
    //myGoalKeepers.add(midfielder3);
    //myGoalKeepers.add(striker1);
    //myGoalKeepers.add(striker2);
    //myGoalKeepers.add(striker3);
  }

}
