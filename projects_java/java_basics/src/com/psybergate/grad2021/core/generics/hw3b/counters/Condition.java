package com.psybergate.grad2021.core.generics.hw3b.counters;

public interface Condition<T> {

  boolean isSatisfiedBy(T element);

}
