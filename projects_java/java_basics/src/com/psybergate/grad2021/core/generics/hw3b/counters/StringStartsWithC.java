package com.psybergate.grad2021.core.generics.hw3b.counters;

public class StringStartsWithC implements Condition<String> {

  public boolean isSatisfiedBy(String str) {
    return (str.charAt(0) == 'c');
  }

}
