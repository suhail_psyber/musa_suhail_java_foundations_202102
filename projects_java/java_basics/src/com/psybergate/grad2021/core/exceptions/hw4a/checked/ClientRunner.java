package com.psybergate.grad2021.core.exceptions.hw4a.checked;

import com.psybergate.grad2021.core.exceptions.hw4a.checked.exceptions.*;

import static com.psybergate.grad2021.core.exceptions.hw4a.checked.AccountDB.*;
import static com.psybergate.grad2021.core.exceptions.hw4a.checked.AccountServiceController.*;

public class ClientRunner {
  public static void main(String[] args) throws ApplicationException {

    /*Good Customer*/
    try {
      addAccount("A1", 10000);
      deposit(getAccount("A1"), 2);
      withdraw(getAccount("A1"), 1);
      System.out.println(getAccount("A1"));
    } catch (MyDepositException | MyCustomerNumberException | MyWithdrawalException e) {
      throw new ApplicationException(e);
    }

    /*Bad Account Creation - CustomerNumber in use (Validation not complete)*/
    try {
      addAccount("A1", 20000);
      System.out.println(getAccount("A1"));
    } catch (ApplicationException e) {
      throw new ApplicationException(e);
    }

    /*Bad Account Creation - Deposit too high*/
    try {
      addAccount("A2", 10000000);
      System.out.println(getAccount("A2"));
    } catch (MyDepositException | MyCustomerNumberException e) {
      throw new ApplicationException(e);
    }

    /*Good Account Creation but bad methods (deposit/withdrawal too high)*/
    try {
      addAccount("A3", 10000);
      deposit(getAccount("A3"), 20000);
      withdraw(getAccount("A3"), 10000000);
      System.out.println(getAccount("A3"));
    } catch (MyDepositException | MyWithdrawalException | MyCustomerNumberException e) {
      throw new ApplicationException(e);
    }

  }

  private static ChequeAccount getAccount(String a1) {
    return getAccounts().get(a1);
  }
}
