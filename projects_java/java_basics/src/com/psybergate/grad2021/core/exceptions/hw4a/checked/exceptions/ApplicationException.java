package com.psybergate.grad2021.core.exceptions.hw4a.checked.exceptions;

public class ApplicationException extends Exception {
  public ApplicationException() {
  }

  public ApplicationException(String message) {
    super(message);
  }

  public ApplicationException(String message, Throwable cause) {
    super(message, cause);
  }

  public ApplicationException(Throwable cause) {
    super("An ApplicationException has taken place. Account Service could not be completed", cause);
  }
}
