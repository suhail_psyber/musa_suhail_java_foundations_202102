package com.psybergate.grad2021.core.exceptions.ce2a;

public class ClassB {

  public static void methodException() throws MyCheckedException {
    throw new MyCheckedException();
    /*needs to be caught or propagated*/
  }

  public static void methodThrowable() throws MyThrowableException {
    throw new MyThrowableException();
    /*needs to be caught or propagated*/
  }

  public static void methodError() {
    throw new MyErrorException();
    /*propagated by default*/
  }

  public static void methodRuntime() {
    throw new MyRuntimeException();
    /*propagated by default*/
  }
}
