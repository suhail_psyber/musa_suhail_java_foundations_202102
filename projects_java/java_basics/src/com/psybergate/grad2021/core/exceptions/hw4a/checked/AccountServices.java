package com.psybergate.grad2021.core.exceptions.hw4a.checked;

import com.psybergate.grad2021.core.exceptions.hw4a.checked.exceptions.*;

public class AccountServices {

  public static void deposit(ChequeAccount account, double amount) throws MyDepositException {
    account.setCurrentBalance(account.getCurrentBalance() + amount);
  }

  public static void withdraw(ChequeAccount account, double amount) throws MyWithdrawalException {
    account.setCurrentBalance(account.getCurrentBalance() - amount);
  }

}
