package com.psybergate.grad2021.core.exceptions.hw1a;

import java.io.IOException;

public class Checked_NestedMethodStructure {

  public static void main(String[] args) throws IOException {
    method1();
  }

  private static void method1() throws IOException {
    method2();
  }

  private static void method2() throws IOException {
    method3();
  }

  private static void method3() throws IOException {
    method4();
  }

  private static void method4() throws IOException {
    method5();
  }

  /*IF ONE METHOD PROPAGATES A CHECKED EXCEPTION, ALL CALLERS MUST PROPAGATE OR CATCH THE CHECKED EXCEPTION*/
  private static void method5() throws IOException {
    throw new IOException();
  }
}
