package com.psybergate.grad2021.core.exceptions.ce3a;

public class MyRuntimeException extends RuntimeException {

  public MyRuntimeException() {
    System.out.println("default MyRuntimeException Constructor");
  }

  /**
   * IF YOU WANT TO USE CHAINED EXCEPTIONS WITH THIS CLASS BEING THE HIGHER LEVEL, WE NEED THESE ADDITIONAL CONSTRUCTORS
   */

/*  public MyRuntimeException(String message) {
    super(message);
    System.out.println("String MyRuntimeException Constructor");
  }

  public MyRuntimeException(Throwable cause) {
    super(cause);
    System.out.println("Cause MyRuntimeException Constructor");
  }

  public MyRuntimeException(String message, Throwable cause) {
    super(message, cause);
    System.out.println("String AND Cause MyRuntimeException Constructor");
  }*/
}
