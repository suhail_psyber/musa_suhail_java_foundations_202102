package com.psybergate.grad2021.core.exceptions.ce2a;

import static com.psybergate.grad2021.core.exceptions.ce2a.ClassB.*;

public class ClassA {

  public static void methodExceptionCaller() throws MyCheckedException {
    methodException();
    /*needs to be caught or >propagated<*/
  }

  public void methodThrowableCaller() {
    try {
      methodThrowable();
    } catch (MyThrowableException e) {
      e.printStackTrace();
    }
    /*needs to be >caught< or propagated*/
  }

  public void methodErrorCaller() {
    methodError();
    /*propagated by default*/
  }

  public void methodRuntimeCaller() {
    methodRuntime();
    /*propagated by default*/
  }
}
