package com.psybergate.grad2021.core.exceptions.hw4a.checked;

import com.psybergate.grad2021.core.exceptions.hw4a.checked.exceptions.*;

import java.util.HashMap;
import java.util.Map;

public class AccountDB {

  private static Map<String, ChequeAccount> accounts = new HashMap<String, ChequeAccount>();

  public AccountDB() throws MyDepositException, MyCustomerNumberException {
  }

  public static Map<String, ChequeAccount> addAccount(String customerNumber, double initDeposit) throws MyDepositException,
          MyCustomerNumberException {

    accounts.put(customerNumber, new ChequeAccount(customerNumber, initDeposit));

    return accounts;
  }

  public static Map<String, ChequeAccount> getAccounts() {
    return accounts;
  }
}
