package com.psybergate.grad2021.core.exceptions.hw4a.checked;

import com.psybergate.grad2021.core.exceptions.hw4a.checked.exceptions.*;
public class AccountValidator {

  /**
   * @param customerNumber - validated on creation (should be unique)
   * @param deposit - validated on creation (should not exceed R1m)
   */
  public static void validateAccount(String customerNumber, double deposit) throws MyDepositException, MyCustomerNumberException {
    validateCustomerNumber(customerNumber);
    validateDeposit(deposit);
  }

  /**
   * customerNumber has to be Unique
   */
  public static void validateCustomerNumber(String customerNumber) throws MyCustomerNumberException {
    if (AccountDB.getAccounts().containsKey(customerNumber)) {
      throw new MyCustomerNumberException();
    }
  }

  /**
   * Deposit cannot exceed R100_000
   */
  public static void validateDeposit(double deposit) throws MyDepositException {
    if (deposit > 1000_000) {
      throw new MyDepositException();
    }
  }

  /**
   * Withdrawals cannot exceed currentBalance
   */
  public static void validateWithdrawal(ChequeAccount account, double withdrawal) throws MyWithdrawalException {
    if (withdrawal > account.getCurrentBalance()) {
      throw new MyWithdrawalException();
    }
  }


}
