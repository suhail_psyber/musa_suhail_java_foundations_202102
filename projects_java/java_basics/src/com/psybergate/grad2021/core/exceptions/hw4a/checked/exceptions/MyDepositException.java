package com.psybergate.grad2021.core.exceptions.hw4a.checked.exceptions;

public class MyDepositException extends ApplicationException {
  public MyDepositException() {
    System.out.println("Deposited amount exceeds max amount (R1000 000)");
  }
}
