package com.psybergate.grad2021.core.exceptions.hw4a.checked.exceptions;

public class MyCustomerNumberException extends ApplicationException {
  public MyCustomerNumberException() {
    System.out.println("Account Number already in use");
  }
}
