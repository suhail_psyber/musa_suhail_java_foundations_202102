package com.psybergate.grad2021.core.exceptions.hw4a.checked;

import com.psybergate.grad2021.core.exceptions.hw4a.checked.exceptions.*;

public class AccountServiceController {

  public static void deposit(ChequeAccount account, double amount) throws MyDepositException {
    AccountValidator.validateDeposit(amount);
    AccountServices.deposit(account, amount);
  }

  public static void withdraw(ChequeAccount account, double amount) throws MyWithdrawalException {
    AccountValidator.validateWithdrawal(account, amount);
    AccountServices.withdraw(account, amount);

    /**Can use this to get a smaller StackTrace as the method in Service is not needed*/
    //account.setCurrentBalance(account.getCurrentBalance() - amount);
  }

}
