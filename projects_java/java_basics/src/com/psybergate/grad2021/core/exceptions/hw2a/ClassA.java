package com.psybergate.grad2021.core.exceptions.hw2a;

import java.sql.SQLException;

public class ClassA {

  public static void methodA() throws ApplicationException {
    methodB();
  }

  private static void methodB() throws ApplicationException {
    try {
      //assume a call to a DB is made here
      throw new SQLException(); //this could occur based on above
    } catch (SQLException e) {
      System.out.println("SQLException caught when trying to pull from DB");
      throw new ApplicationException("ApplicationException is wrapping a SQLException", e);
    }

  }

}
