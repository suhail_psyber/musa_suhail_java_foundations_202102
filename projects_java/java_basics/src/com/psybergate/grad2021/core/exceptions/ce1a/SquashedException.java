package com.psybergate.grad2021.core.exceptions.ce1a;

public class SquashedException {
  public static void main(String[] args) {
    someMethod1();
  }

  //exception is badly handled - squashed exception (DOES NOT NEED PROPAGATION IF HANDLED)
  private static void someMethod1() {
    try {
      someMethod3();
      someMethod2();
      //second method call doesn't run as we enter catch block after line 10 (or rather line 19) runs
    } catch (Exception e) {
      e.printStackTrace(); //Exceptions are squashed i.e catched and no logic performed to resolve the issue
    }
  }

  //exception is propagated to the caller (by default)
  private static void someMethod2() {
    System.out.println("someMethod2");
    throw new RuntimeException();
  }

  //exception is propagated to the caller
  private static void someMethod3() throws Exception {
    System.out.println("someMethod3");
    throw new Exception();
  }
}
