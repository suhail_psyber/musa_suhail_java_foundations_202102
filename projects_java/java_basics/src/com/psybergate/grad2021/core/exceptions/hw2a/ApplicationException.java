package com.psybergate.grad2021.core.exceptions.hw2a;

import java.io.IOException;

public class ApplicationException extends IOException {

  public ApplicationException(String message, Throwable cause) {
    super(message, cause);
  }
}
