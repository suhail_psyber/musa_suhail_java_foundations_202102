package com.psybergate.grad2021.core.exceptions.hw2a;

import static com.psybergate.grad2021.core.exceptions.hw2a.ClassA.*;

public class ClientClass {
  public static void main(String[] args) {

    /*Want full stacktrace*/
    try {
      methodA();
    } catch (ApplicationException e) {
      e.printStackTrace();
    }

    /*Only want SQLException (cause)*/
    try {
      methodA();
    } catch (ApplicationException e) {
      e.getCause().printStackTrace();
    }

  }
}
