package com.psybergate.grad2021.core.exceptions.hw1a;

public class Runtime_NestedMethodStructure {

  public static void main(String[] args) {
    method1();
  }

  private static void method1() {
    method2();
  }

  private static void method2() {
    method3();
  }

  private static void method3() {
    method4();
  }

  private static void method4() {
    method5();
  }

  /*IF ONE METHOD PROPAGATES A RUNTIME EXCEPTION (BY DEFAULT), ALL CALLERS PROPAGATE (BY DEFAULT) OR CATCH THE CHECKED EXCEPTION*/
  private static void method5() {
    throw new Error();
  }
}
