package com.psybergate.grad2021.core.exceptions.hw4a.checked;

import com.psybergate.grad2021.core.exceptions.hw4a.checked.exceptions.*;

public class ChequeAccount {

  private final String customerNumber;

  private double currentBalance;

  public ChequeAccount(String customerNumber, double deposit) throws MyDepositException, MyCustomerNumberException {
    AccountValidator.validateAccount(customerNumber, deposit);
    this.customerNumber = customerNumber;
    this.currentBalance = deposit;
  }

  public String getCustomerNumber() {
    return customerNumber;
  }

  public double getCurrentBalance() {
    return currentBalance;
  }

  public void setCurrentBalance(double currentBalance) {
    this.currentBalance = currentBalance;
  }

  @Override
  public String toString() {
    return  "customerNumber: " + customerNumber + "\n" + "currentBalance: " + currentBalance + "\n";
  }
}
