package com.psybergate.grad2021.core.exceptions.ce3a;

public class ExceptionChaining {
  /**
   * Exception Chaining is throwing an exception with a type compatible broader exception as a parameter
   * Often required where an interface or component propagates its own exception but wants to convey to its Client the underlying (HIGHER LEVEL) cause
   * of the issue (i.e. the HIGHER LEVEL exception)
   */
  public static void main(String[] args) throws Throwable {

    /*Using Throwable(Throwable cause)*/
    try {
      runtimeMethod();
    } catch (MyRuntimeException e) { //catching specific exception
      throw new Throwable(e); //throwing higher level exception with the cause (lower level, specific issue)
    }

    /*Using Throwable(String messageForParent, Throwable cause)*/
    try {
      runtimeMethod();
    } catch (MyRuntimeException e) {
      throw new Exception("!!!THIS MESSAGE APPEARS FOR THE HIGHER LEVEL EXCEPTION!!!", e);
    }

    /*Using Throwable initCause()*/
    try {
      runtimeMethod();
    } catch (MyRuntimeException e) {
      throw new Exception().initCause(e);
    }


    /*Using Throwable getCause() with initCause in method we propagated from*/
    try {
      runtimeMethodWithInitCause1();
    } catch (MyRuntimeException e) { //still catch the specific
      throw new Exception().getCause();
    }

    try {
      runtimeMethodWithInitCause2();
    } catch (MyRuntimeException e) { //still catch the specific
      throw new Exception().getCause();
    }

  }

  private static void runtimeMethod() {
    throw new MyRuntimeException();
  }

    //Q - WHY DOES THIS FAIL IF I ONLY PROPAGATE EXCEPTION INSTEAD OF THROWABLE - the return type of initCause() is a Throwable so the propagation
    // has to match that
  private static void runtimeMethodWithInitCause1() throws Throwable {
    throw new Exception().initCause(new MyRuntimeException());
  }

  /*same as above*/
  private static void runtimeMethodWithInitCause2() throws Throwable {
    throw new Exception(new MyRuntimeException()); //We are using a different constructor and thus do no explicitly use the initCause()
  }

}
