package com.psybergate.grad2021.core.exceptions.hw4a.checked.exceptions;

public class MyWithdrawalException extends ApplicationException {
  public MyWithdrawalException() {
    System.out.println("Withdrawal amount exceeds current balance");
  }
}
