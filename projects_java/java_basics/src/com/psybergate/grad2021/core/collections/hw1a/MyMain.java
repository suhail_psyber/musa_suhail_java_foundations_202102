package com.psybergate.grad2021.core.collections.hw1a;

import java.util.SortedSet;
import java.util.TreeSet;

import static com.psybergate.grad2021.core.collections.hw1a.CustomerType.LOCAL;

public class MyMain {
  public static void main(String[] args) {
    printCustomersAscending();
    printCustomersDescending();
  }

  private static void printCustomersAscending() {
    Customer c1 = new Customer("101", LOCAL, "South Africa", "John Johnson", "2018-01-01");
    Customer c2 = new Customer("105", LOCAL, "South Africa", "John Johnson", "2018-01-01");
    Customer c3 = new Customer("107", LOCAL, "South Africa", "John Johnson", "2018-01-01");
    Customer c4 = new Customer("112", LOCAL, "South Africa", "John Johnson", "2018-01-01");
    Customer c5 = new Customer("109", LOCAL, "South Africa", "John Johnson", "2018-01-01");

    SortedSet mySetAsc = new TreeSet();
    mySetAsc.add(c1);
    mySetAsc.add(c2);
    mySetAsc.add(c3);
    mySetAsc.add(c4);
    mySetAsc.add(c5);

    System.out.println("mySetAsc = " + mySetAsc);
  }

  private static void printCustomersDescending() {
    Customer c1 = new Customer("101", LOCAL, "South Africa", "John Johnson", "2018-01-01");
    Customer c2 = new Customer("105", LOCAL, "South Africa", "John Johnson", "2018-01-01");
    Customer c3 = new Customer("107", LOCAL, "South Africa", "John Johnson", "2018-01-01");
    Customer c4 = new Customer("112", LOCAL, "South Africa", "John Johnson", "2018-01-01");
    Customer c5 = new Customer("109", LOCAL, "South Africa", "John Johnson", "2018-01-01");

    SortedSet mySetDesc = new TreeSet(new DescendingComparator());
    mySetDesc.add(c1);
    mySetDesc.add(c2);
    mySetDesc.add(c3);
    mySetDesc.add(c4);
    mySetDesc.add(c5);

    System.out.println("mySetDesc = " + mySetDesc);
  }
}
