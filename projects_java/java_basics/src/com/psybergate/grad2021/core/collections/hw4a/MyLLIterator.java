package com.psybergate.grad2021.core.collections.hw4a;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyLLIterator implements Iterator {

  private MyLinkedList myLinkedList;

  private Node currentNode;

  public MyLLIterator(MyLinkedList myLinkedList) {
    this.myLinkedList = myLinkedList;
  }

  @Override
  public boolean hasNext() {
    if (myLinkedList.head == null)
      return false;
    if (currentNode != null && currentNode.isTheTail()) {
      return false;
    }
    return true;
  }

  @Override
  public Object next() {
    if (hasNext() == false) { //if (!hasNext())
      throw new NoSuchElementException("you have reached the end of the LinkedList");
    }
    if (currentNode == null) {
      currentNode = myLinkedList.head;
    } else {
      currentNode = currentNode.next;
    }
    return currentNode.getNodeElement();
  }

  public Node getCurrentNode() {
    return currentNode;
  }
}
