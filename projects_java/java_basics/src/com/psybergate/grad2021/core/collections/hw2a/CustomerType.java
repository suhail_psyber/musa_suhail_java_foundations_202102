package com.psybergate.grad2021.core.collections.hw2a;

public enum CustomerType {
  LOCAL, INTERNATIONAL
}
