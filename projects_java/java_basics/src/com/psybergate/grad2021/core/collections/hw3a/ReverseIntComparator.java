package com.psybergate.grad2021.core.collections.hw3a;

import java.util.Comparator;

public class ReverseIntComparator implements Comparator {


  @Override
  public int compare(Object o1, Object o2) {
    return ((String) o2).charAt(1) - ((String) o1).charAt(1);
  }

}
