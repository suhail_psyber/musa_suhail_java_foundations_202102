package com.psybergate.grad2021.core.collections.hw4a;

import java.util.Iterator;

public class Main {
  public static void main(String[] args) {
    MyLinkedList myLL = new MyLinkedList();

    myLL.add("abc");
    myLL.add("xyz");
    myLL.add("I will be removed");
    myLL.add("def");
    myLL.add(true);
    myLL.add(5);
    myLL.add("I will be removed as the Tail");

    System.out.println("myLL.contains(\"abc\") = " + myLL.contains("abc"));

    myLL.remove("I will be removed");
    myLL.removeTail();

    for (Iterator iterator = new MyLLIterator(myLL); iterator.hasNext();) {
      System.out.println(iterator.next());
    }
  }
}
