package com.psybergate.grad2021.core.collections.hw3a;

import java.util.SortedSet;
import java.util.TreeSet;

public class MyStrings {

  public static void main(String[] args) {
    printBasedOffInput();
    //printStringDesc();
    //printReverseInt(); //rename method and class
    //printReverseAlpha(); //rename method and class
  }

  private static void printBasedOffInput() {
    String s1 = "d2";
    String s2 = "a5";
    String s3 = "e1";
    String s4 = "c3";
    String s5 = "b4";

    SortedSet mySet = new TreeSet();
    mySet.add(s1);
    mySet.add(s2);
    mySet.add(s3);
    mySet.add(s4);
    mySet.add(s5);

    System.out.println(mySet);
  }

  private static void printStringDesc() {
    String s1 = "d2";
    String s2 = "a5";
    String s3 = "e1";
    String s4 = "c3";
    String s5 = "b4";

    SortedSet mySet = new TreeSet(new StringDescendingComparator()); //like String descending comparator

    mySet.add(s1);
    mySet.add(s2);
    mySet.add(s3);
    mySet.add(s4);
    mySet.add(s5);

    System.out.println(mySet);
  }

  private static void printReverseInt() {
    String s1 = "d2";
    String s2 = "a5";
    String s3 = "e1";
    String s4 = "c3";
    String s5 = "b4";

    SortedSet mySet = new TreeSet(new ReverseIntComparator()); //think about your naming
    mySet.add(s1);
    mySet.add(s2);
    mySet.add(s3);
    mySet.add(s4);
    mySet.add(s5);

    System.out.println(mySet);
  }

  private static void printReverseAlpha() {
    String s1 = "d2";
    String s2 = "a5";
    String s3 = "e1";
    String s4 = "c3";
    String s5 = "b4";

    SortedSet mySet = new TreeSet(new ReverseAlphaComparator()); //like String descending comparator

    mySet.add(s1);
    mySet.add(s2);
    mySet.add(s3);
    mySet.add(s4);
    mySet.add(s5);

    System.out.println(mySet);
  }

}