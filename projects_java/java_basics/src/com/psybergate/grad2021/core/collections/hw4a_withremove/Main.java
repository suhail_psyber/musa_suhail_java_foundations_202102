package com.psybergate.grad2021.core.collections.hw4a_withremove;

import java.util.Iterator;
import java.util.List;

public class Main {
  public static void main(String[] args) {
    List myLL = new MyLinkedList();

    myLL.add("abc");
    myLL.add("xyz");
    myLL.add("I will be removed");
    myLL.add("def");
    myLL.add(true);
    myLL.add(5);

    myLL.remove("I will be removed");

    for (Iterator iterator = new MyLLIterator((MyLinkedList) myLL); iterator.hasNext(); ) {
      System.out.println(((Node) iterator.next()).getNodeElement());
    }
  }
}
