package com.psybergate.grad2021.core.collections.ce1a_v2;

public class ArrayStack extends AbstractStack {

  public static void main(String[] args) {
    ArrayStack myStack = new ArrayStack();
    System.out.println(myStack.size());
  }

  public Object[] objects = new Object[10]; /*start with 10 elements. make it 100 if size>10*/

  public int start = objects.length;

  @Override
  public boolean add(Object o) { //add logic if we are already at max capacity
    push(o);
    return false;
  }

  @Override
  public boolean contains(Object o) {
    return false;
  }

  @Override
  public boolean isEmpty() {
    return false;
  }

  @Override
  public int size() {
    return objects.length;
  }

  @Override
  public void push(Object o) {  }

  @Override
  public Object pop(Object o) {
    return objects[0]; //position needs to match the last object added
  }

  @Override
  public Object get(int position) {
    return objects[position];
  }

}
