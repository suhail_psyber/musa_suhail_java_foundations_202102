package com.psybergate.grad2021.core.collections.hw1a;

import java.util.Comparator;

public class DescendingComparator implements Comparator {

  @Override
  public int compare(Object o1, Object o2) {
    Customer c1 = (Customer) o1;
    Customer c2 = (Customer) o2;
    return -1*c1.getCustomerNumber().compareTo(c2.getCustomerNumber());
  }

}
