package com.psybergate.grad2021.core.collections.hw4a;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyLinkedList extends MyAbstractLinkedList {

  Node head;

  Node tail;

  private int lastIndex = -1;

  @Override
  public boolean add(Object o) {
    if (isEmpty()) {
      head = new Node(null, o);
      tail = head;
      return true;
    }
    Node addedNode = new Node(tail, o);
    tail = addedNode;
    return true;
  }

  @Override
  public boolean contains(Object o) {
    for (Iterator iter = new MyLLIterator(this); iter.hasNext(); ) {
      if (iter.next().equals(o)) { //our equals is in Node
        return true;
      }
    }
    return false;
  }

  public Object removeTail() { //also known as pop()
    if (head == null) {
      new NoSuchElementException("The LL is empty - nothing to remove");
    }
    Node removedNode = tail;
    tail = removedNode.previous;
    tail.next = null;
    return removedNode;
  }

  public int size() {
    return lastIndex + 1;
  }

  public boolean isEmpty() {
    return head == null;
    /**
     * how does this know if the head is null or not? when we add(), we set what the head is here MyLinkedList.head value is, if we don't add, it
     * defaults to null
     */
  }

  @Override
  public Iterator iterator() {
    return new MyLLIterator(this);
  }

}
