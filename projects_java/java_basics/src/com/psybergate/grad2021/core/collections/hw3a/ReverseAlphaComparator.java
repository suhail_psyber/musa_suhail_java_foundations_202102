package com.psybergate.grad2021.core.collections.hw3a;

import java.util.Comparator;

public class ReverseAlphaComparator implements Comparator {

  @Override
  public int compare(Object o1, Object o2) {
    return ((String) o2).charAt(0) - ((String) o1).charAt(0);
  }
}
