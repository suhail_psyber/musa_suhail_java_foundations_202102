package com.psybergate.grad2021.core.collections.hw3a;

import java.util.Comparator;

public class StringDescendingComparator implements Comparator {
  @Override
  public int compare(Object o1, Object o2) {
    String s1 = (String) o1;
    String s2 = (String) o2;
    return -1*s1.compareTo(s2);
    //return s2.compareTo(s1); less readable
    //+ve means first obj is greater than (usually - not specific to my case cause we *-1)
  }
}
