package com.psybergate.grad2021.core.collections.ce1a;

import java.util.Collection;
import java.util.Iterator;

public interface StackInterface extends Collection {
  /**
   * Attributes of MyStack Interface
   * 1) Can contain duplicates
   * 2) Naturally orders from top to bot (last one in is 1st when iterating through)
   * 3) Has the following methods: public void push(Object), public Object pop(), public Object get(int position)
   */

  /**
   * adds object to the top of the stack
   *
   * @param o
   */
  public void push(Object o);

  /**
   * removes and returns object from the top of the stack
   *
   * @param o
   * @return Object o
   */
  public Object pop(Object o);

  /**
   * returns the object corresponding to a certain position from the top of the stack
   *
   * @param position
   * @return Object
   */
  public Object get(int position);

  /*FROM COLLECTION*/
  @Override
  public int size();

  @Override
  public boolean isEmpty();

  @Override
  public boolean contains(Object o);

  @Override
  public boolean add(Object o);

  @Override
  public default Iterator iterator() {
    return null;
  }

  @Override
  public default Object[] toArray() {
    return new Object[0];
  }

  @Override
  public default Object[] toArray(Object[] a) {
    return new Object[0];
  }

  @Override
  public default boolean remove(Object o) {
    return false;
  }

  @Override
  public default boolean addAll(Collection c) {
    return false;
  }

  @Override
  public default void clear() {

  }

  @Override
  public default boolean retainAll(Collection c) {
    return false;
  }

  @Override
  public default boolean removeAll(Collection c) {
    return false;
  }

  @Override
  public default boolean containsAll(Collection c) {
    return false;
  }

}
