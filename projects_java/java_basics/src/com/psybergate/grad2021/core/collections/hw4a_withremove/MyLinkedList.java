package com.psybergate.grad2021.core.collections.hw4a_withremove;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyLinkedList extends MyAbstractLinkedList {

  Node head;

  Node tail;

  private int lastIndex = -1; //I should use this better and not just for size. Can increment when we make add an object and also use it for
  // removing from an index

  @Override
  public boolean add(Object o) {
    lastIndex++; //vs ++lastIndex
    if (isEmpty()) {
      head = new Node(null, o);
      tail = head;
      return true;
    }
    Node addedNode = new Node(tail, o);
    tail = addedNode;
    return true;
  }

  @Override
  public Object get(int index) {
    new IndexOutOfBoundsException();
    return this;
  }

  @Override
  public boolean contains(Object o) {
    for (Iterator iter = new MyLLIterator(this); iter.hasNext(); ) {
      if (((Node)iter.next()).getNodeElement().equals(o)) { //our equals is in Node
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean remove(Object o) { //will work on first duplicate we find
    if (head == null) {
      new NoSuchElementException("The LL is empty - nothing to remove");
    }

    if (contains(o)) {
      for (Iterator iter = new MyLLIterator(this); iter.hasNext(); ) {
        Node removedNode = ((Node)iter.next());
        if (removedNode.getNodeElement().equals(o)) {
          removedNode.previous.next = removedNode.next;
          removedNode.next.previous = removedNode.previous;
        }
      }
    }
    else {
        new NoSuchElementException("you are tyring to remove something that does not exist in the LL");
    }

    return true;
  }

  public Object removeTail() { //also known as pop()
    if (head == null) {
      new NoSuchElementException("The LL is empty - nothing to remove");
    }
    Node removedNode = tail;
    tail = removedNode.previous;
    tail.next = null;
    return removedNode;
  }

  public int size() {
    return lastIndex + 1;
  }

  public boolean isEmpty() {
    return head == null;
    /**
     * how does this know if the head is null or not? when we add(), we set what the head is here MyLinkedList.head value is, if we don't add, it
     * defaults to null
     */
  }

  @Override
  public Iterator iterator() {
    return new MyLLIterator(this);
  }

}
