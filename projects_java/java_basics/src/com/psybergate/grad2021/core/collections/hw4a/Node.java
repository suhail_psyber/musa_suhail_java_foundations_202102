package com.psybergate.grad2021.core.collections.hw4a;

import java.util.Objects;

public class Node {

  private Object nodeElement;

  Node next;

  Node previous;

  public Node(Node previous, Object nodeElement) { //we don't need to worry about the next one here because when we make one the next is null
    this.previous = previous;
    if (this.previous != null) {
      previous.next = this;
    }
    this.nodeElement = nodeElement;
    this.next = null;
  }

  public Object getNodeElement() {
    return nodeElement;
  }

  public boolean isTheHead() {
    return previous == null;
  } //is the first Node

  public boolean isTheTail() {
    return next == null;
  } //is the last Node

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    Node node = (Node) o;
    return Objects.equals(nodeElement, node.nodeElement);
  }

  @Override
  public int hashCode() {
    return Objects.hash(nodeElement);
  }
}
