package com.psybergate.grad2021.core.collections.ce1a_v2;

import java.util.Collection;

public interface StackInterface extends Collection {

  /**
   * adds object to the top of the stack
   *
   * @param o
   */
  public void push(Object o);

  /**
   * removes and returns object from the top of the stack
   *
   * @param o
   * @return Object o
   */
  public Object pop(Object o);

  /**
   * returns the object corresponding to a certain position from the top of the stack
   *
   * @param position
   * @return Object
   */
  public Object get(int position);

}
