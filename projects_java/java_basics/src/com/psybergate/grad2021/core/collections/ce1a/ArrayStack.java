package com.psybergate.grad2021.core.collections.ce1a;

public class ArrayStack implements StackInterface {

  /*MAKE A PARENTS CLASS TO CLEAN THIS NOT "DEFAULT" MODIFIER IN THE INTERFACE*/

  @Override
  public boolean add(Object o) {
    return false;
  }

  @Override
  public boolean contains(Object o) {
    return false;
  }

  @Override
  public boolean isEmpty() {
    return false;
  }

  @Override
  public int size() {
    return 0;
  }


  /*Personalized methods in StackInterface*/
  @Override
  public void push(Object o) {

  }

  @Override
  public Object pop(Object o) {
    return null;
  }

  @Override
  public Object get(int position) {
    return null;
  }

}
