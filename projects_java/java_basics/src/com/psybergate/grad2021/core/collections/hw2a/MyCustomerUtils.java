package com.psybergate.grad2021.core.collections.hw2a;

import java.util.HashMap;
import java.util.Map;

import static com.psybergate.grad2021.core.collections.hw2a.CustomerType.LOCAL;

public class MyCustomerUtils /*implements Map*/ {
  public static void main(String[] args) {
    Customer c1 = new Customer("101", LOCAL, "South Africa", "John Johnson", "2018-01-01");
    Customer c2 = new Customer("105", LOCAL, "South Africa", "John Johnson", "2018-01-01");
    Customer c3 = new Customer("107", LOCAL, "South Africa", "John Johnson", "2018-01-01");
    Customer c4 = new Customer("112", LOCAL, "South Africa", "John Johnson", "2018-01-01");
    Customer c5 = new Customer("109", LOCAL, "South Africa", "John Johnson", "2018-01-01");

    myMap.put(c1, c1.getCustomerNumber());
    myMap.put(c5, c5.getCustomerNumber());
    myMap.put(c3, c3.getCustomerNumber());
    myMap.put(c2, c2.getCustomerNumber());
    myMap.put(c4, c4.getCustomerNumber());

    printCustomerNumber(c5);
  }

  static Map myMap = new HashMap();

  private static void printCustomerNumber(Customer c) {
    System.out.println("Customer Number = " + myMap.get(c));
  }
}
