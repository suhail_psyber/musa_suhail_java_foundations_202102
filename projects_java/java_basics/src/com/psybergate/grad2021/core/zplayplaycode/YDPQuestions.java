package com.psybergate.grad2021.core.zplayplaycode;

public class YDPQuestions {
  //public static void main(String[] args) {
  //  /**
  //   * Q1 - How many parents and interfaces can a class extend and implement respectively?
  //   * A) 1 and many
  //   * B) 1 and 1
  //   * C) many and many
  //   * D) many and 1
  //   */
  //
  //  /**
  //   * Q2 - what would this print?
  //   * A) 50
  //   * B) 320
  //   * C) 40
  //   * D) 160
  //   */
  //
  //  int x = 10;
  //  int y = 5;
  //
  //  for (int i = 0; i < y; i++) {
  //    x += x;
  //  }
  //
  //  System.out.println("x = " + x);
  //
  //  /**
  //   * Q3 - Will this line compile?
  //   * A) yes, but only if we add the "default" constructor
  //   * B) yes, there is a "default" constructor already in Shape
  //   * C) no, there can only be ONE constructor and it does not match with the object we want to create
  //   * D) no, we cannot change the length so nothing matters
  //   */
  //
  //  Shape mySquare = new Shape();
  //
  //  //this is a separate, stand alone class - let's not give them inner classes
  //  public class Shape {
  //
  //    String Rectangle = "Square";
  //
  //    int length;
  //
  //    public Shape(int length) {
  //      this.length = 10;
  //    }
  //
  //  }
  //
  //  /**
  //   * Q4 - What will this return?
  //   * A) abc
  //   * B) def
  //   * c) abcdef
  //   * d) nothing
  //   */
  //  String str1 = "abc";
  //  str1.concat("def");
  //  System.out.println("str1 = " + str1);
  //
  //  /**
  //   * Q5 - Chris' problem of the week - calculate the perimeter of these stairs
  //   */
  //}


}
