package com.psybergate.grad2021.core.zplayplaycode.gena_ashraf;

public class Grad implements Comparable<Grad> {

  private String idNum;

  private String name;

  private String surname;

  public Grad(String idNum, String name, String surname) {
    this.idNum = idNum;
    this.name = name;
    this.surname = surname;
  }

  public String getIdNum() {
    return idNum;
  }

  @Override
  public boolean equals(Object gradObject) {
    //non nullability
    if (gradObject == null) {
      return false;
    }
    //identity
    if (this == gradObject) {
      return true;
    }
    //Different object types - Casting
    if (this.getClass() != gradObject.getClass()) {
      return false;
    }
    //Unique Identifier - premise of equals
    Grad myGradObject = (Grad) gradObject;
    return this.idNum.equals(myGradObject.idNum);
  }

  //personalizing the hasCode for this abstraction
  //@Override
  //public int hashCode() {
  //  return this.idNum.hashCode();
  //}

  public void setIdNum(String idNum) {
    this.idNum = idNum;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  @Override
  public String toString() {
    return "Grad{" + "idNum='" + idNum + '\'' + ", name='" + name + '\'' + ", surname='" + surname + '\'' + '}';
  }

  @Override
  public int compareTo(Grad o) {
    //what are you comparing.. not object! it would just call this method again bruh.
    return this.idNum.compareTo(o.idNum);
  }
}
