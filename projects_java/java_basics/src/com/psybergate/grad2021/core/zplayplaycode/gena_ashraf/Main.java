package com.psybergate.grad2021.core.zplayplaycode.gena_ashraf;

import com.psybergate.grad2021.core.Utils;

import java.util.Set;
import java.util.TreeSet;

public class Main {

  public static void main(String[] args) {
    System.out.println("Utils.random(17) = " + Utils.random(17));
    //entrySet - readup

    //Map<String, Grad> gradTable = new HashMap();

    /**
     * same as the lambda below
     */
    //Set<Grad> gradSetInner = new TreeSet<>(new Comparator<Grad>() {
    //  @Override
    //  public int compare(Grad o1, Grad o2) {
    //    return o1.getSurname().compareTo(o2.getSurname());
    //  }
    //});

    Set<Grad> gradSetLambda = new TreeSet<>((grad1, grad2) -> grad1.getSurname().compareTo(grad2.getSurname()));

    gradSetLambda.add(new Grad("5", "Irfaan", "Mohamed"));

    Grad g1 = new Grad("101", "Irfaan", "Moo");
    gradSetLambda.add(g1);
    boolean contains = gradSetLambda.contains(new Grad("101", "Jack", "Mo"));
    boolean eq = g1.equals(new Grad("101", "Jack", "JACCCCCCCCCCCK"));

    gradSetLambda.add(new Grad("101", "Irfaan", "Mo"));

    System.out.println("contains = " + contains);
    System.out.println("gradSet = " + gradSetLambda);
  }

}
