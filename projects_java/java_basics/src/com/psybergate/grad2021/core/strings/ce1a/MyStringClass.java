package com.psybergate.grad2021.core.strings.ce1a;

public class MyStringClass {
  public static void main(String[] args) {
    /**
     * The Modifiers for the String Class are "public final" - i.e. immutable
     */
    String s0 = "abc";
    s0.concat("def");
    System.out.println("s0 = " + s0); //prints "abc"


    /**
     * Basic String methods
     */
    String s1 = "abc";
    String s2 = "def";
    String s3a = "XYZ";
    String s3b = "xyz";
    String s4 = "abcdef";

    System.out.println("s1.charAt(1) = " + s1.charAt(1));
    System.out.println("s1.concat(s2) = " + s1.concat(s2)); //new object created - s1 is untouched
    System.out.println("s1.equalsIgnoreCase(s3a) = " + s1.equalsIgnoreCase(s3a));
    System.out.println("s3b.equalsIgnoreCase(s3a) = " + s3b.equalsIgnoreCase(s3a));
    System.out.println("s1.length() = " + s1.length());
    System.out.println("s1.replace(\"c\", \"Z\") = " + s1.replace('c', 'Z'));
    System.out.println("s1.replace(\"Z\", \"c\") = " + s1.replace("Z", "c"));
    System.out.println("s4.subString(1, 5) = " + s4.substring(1, 5)); //(including) start and (excluding) end index
    System.out.println("\"ABC\".toLowerCase() = " + "ABC".toLowerCase());
    System.out.println("\"abc\".toUpperCase() = " + "abc".toUpperCase());
    System.out.println("s1.toString() = " + s1.toString()); //returns the string itself... useful for objects but not the String Object itself.
    System.out.println("s1.trim() = " + s1.trim()); //removes starting and trailing WHITESPACE
  }
}
