package com.psybergate.grad2021.core.strings.ce2a;

public class StringMutabilityBufferAndBuilder {
  public static void main(String[] args) {

    /**
     * StringBuilder and StringBuffer offer the same methods and both "extends AbstractStringBuilder implements java.io.Serializable, CharSequence"
     * append()
     * delete()
     * insert()
     * replace()
     * reverse()
     * toString()
     */

    /*StringBuffer Class - LESS efficient but thread SAFE*/
    StringBuffer buffer1 = new StringBuffer();
    buffer1.append("My first name is Suhail");
    System.out.println("buffer1 = " + buffer1);
    StringBuffer buffer2 = new StringBuffer("My surname is");
    System.out.println("buffer2.append(\" Musa\") = " + buffer2.append(" Musa"));

    /*StringBuilder Class - MORE efficient but NOT thread safe*/
    StringBuilder builder1 = new StringBuilder("My name is still Suhail");
    System.out.println("builder1.delete(11,17) = " + builder1.delete(11, 17)); //including start, excluding end
    System.out.println("builder1.append(\" Musa\") = " + builder1.append(" Musa"));
    System.out.println("builder1.insert(18, \"Ahmed\") = " + builder1.insert(18, "Ahmed ")); //index to place new String
    System.out.println("builder1.replace(18, 52, \"\") = " + builder1.replace(18, 24, ""));
    System.out.println("builder1.reverse() = " + builder1.reverse());
    System.out.println("builder1.reverse() = " + builder1.reverse());
    System.out.println("builder1.toString() = " + builder1.toString()); //creates a copy... returns a new String object!!! (new object is immutable)
    String s1 = builder1.toString();
    //s1.append(); does not compile as it is a String and not a StringBuilder object
  }
}
