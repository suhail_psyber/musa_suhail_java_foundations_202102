package com.psybergate.grad2021.core.strings.hw2a;

import java.util.StringTokenizer;

public class StringTokenizing {
  public static void main(String[] args) {
    /**
     * Breaks up a string using a delimiter character. Split() is a better version of this as it can use REGEX
     */
    StringTokenizer tokens = new StringTokenizer(" Token 1 / Token 2 / Token 3 and Token 3 still / Token 4", "/");
    System.out.println(tokens.countTokens());

    for(;tokens.hasMoreTokens();) {
      System.out.println(tokens.nextToken());
    }
  }
}
