package com.psybergate.grad2021.core.strings.hw1a;

import java.util.Locale;

public class StringFormatting {
  public static void main(String[] args) {
    /**
     * Will print the string + format additional datatype to output
     * eg. can use it to change data from int to hex
     *
     * Format Specifier(Code to type at the end of the string) - Data Type to format - Output
     * %a - floating point (except BigDecimal) - Hex of floating point number
     * %b - Any type - true if non-null, false if null
     * %c - character (char) - Unicode character
     * %d - integer (byte to BigInt) - Decimal Integer
     * %e - floating point - decimal number in scientific notation
     * %f - floating point - decimal number // can put %n.rf where; n = number of whitespace to put before number and r = number of decimal units
     * %g - floating point - decimal number, possibly scientific depending on the precision and value
     * %h - any type - Hex String of value from hashCode()
     * %n - none - Platform specific line separator
     * %o - integer (byte to BigInt) - Octal number
     * %s - any type - String value
     * %t - Date/Time (inc long, Calender, Date, Temporal Accessor) - %t is prefix for Date/Time conversions... More flags are needed after this
     * %x - integer (byte to BigInt) - Hex string
     */
    String s1 = "Suhail";
    String s2 = String.format(Locale.getDefault(), "Hi, I'm %s %s", s1, "Musa");
    System.out.println("s1 = " + s1);
    System.out.println("s2 = " + s2);

    int i1 = 90;
    String i2 = String.format("And I am %15d", i1);
    //Locale.getDefault() is there by default with this overloaded method. The number after the % is whitespace
    System.out.println("i1 = " + i1);
    System.out.println("i2 = " + i2);

    double d1 = 26.0;
    String d2 = String.format("Just kidding I am actually %f", d1);
    System.out.println("d1 = " + d1);
    System.out.println("d2 = " + d2);
    if (d2 instanceof String) {
      System.out.println("d2 is a String");
    }

  }
}
