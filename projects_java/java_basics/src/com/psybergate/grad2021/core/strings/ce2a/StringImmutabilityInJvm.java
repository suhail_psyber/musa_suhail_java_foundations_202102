package com.psybergate.grad2021.core.strings.ce2a;

public class StringImmutabilityInJvm {
  public static void main(String[] args) {

    /**
     * String are immutable, thus when we assign a string to a reference, the JVM checks if this string object exists on the HEAP (interning
     * - cached in the Java String POOL), if it does,the new reference simply points to the existing object on the HEAP. We can avoid this by
     * explicitly stating we want a NEW string object using the new keyword as seen for object reference s4.
     */

    String s1 = "abc";
    String s2 = s1;
    String s3 = "abc";
    String s4 = new String("abc"); //makes a new string object on the heap
    String s5 = String.valueOf("abc"); //same as s1 and s3 - JVM does this in the background of those assignments

    /**
     * These equals are not the same as the Object class equals() - IT IS NOT THE DEFAULT IDENTITY EQUALS()
     * The String class has this overridden and goes character by character to see if it matches... this is why this returns true while == is false
     */
    System.out.println("s1.equals(s1) = " + s1.equals(s1));
    System.out.println("s1.equals(s2) = " + s1.equals(s2));
    System.out.println("s1.equals(s3) = " + s1.equals(s3));
    System.out.println("s1.equals(s4) = " + s1.equals(s4));
    System.out.println("s1.equals(s5) = " + s1.equals(s5));

    System.out.println("(s1 == s1) = " + (s1 == s1));
    System.out.println("(s1 == s2) = " + (s1 == s2));
    System.out.println("(s1 == s3) = " + (s1 == s3));
    System.out.println("(s1 == s4) = " + (s1 == s4)); //returns false as we specifically made a new String object on the heap
    System.out.println("(s1 == s5) = " + (s1 == s5));
  }

  /**
   * The key benefits of keeping this class (String) as immutable are caching, security, synchronization, and performance.
   *
   * Caching / Performance
   * String Objects can be cached into the Java String Pool on the heap to save space
   *
   * Security
   * If Strings were mutable, then by the time we execute the update (any additional methods to the specific user through his userName), we can't be
   * sure that the String we received (userName), even after performing security checks, would be safe.
   *
   * Synchronization
   * immutable objects, in general, can be shared across multiple threads running simultaneously. They're also thread-safe because if a thread changes
   * the value, then instead of modifying the same, a new String would be created in the String pool. Hence, Strings are safe for multi-threading
   */
}
