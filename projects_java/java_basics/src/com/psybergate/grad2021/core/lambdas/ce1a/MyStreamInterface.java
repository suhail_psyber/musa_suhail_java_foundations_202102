package com.psybergate.grad2021.core.lambdas.ce1a;

import java.util.stream.Stream;

public interface MyStreamInterface extends Stream {

  /**
   * A stream is basically a collection feeding values one at a time. It has order!
   *
   *
   * Look and read methods in Stream Interface, especially;
   *
   * 1) Filter - returns another stream
   * keeping only values that pass the predicate (a clause or condition that could be true or false)
   *
   *
   * 2) Map and its derivatives - returns another stream
   * the returned stream is the original AFTER experiencing the function method i.e it is the function output
   * stream (1, 2, 4) with function f(x) = 3x would map to stream (3, 6, 12).
   *
   *
   * 3) Reduce - reduces a collection to a single value using a function. Takes in a total (or start value, usually 0) and a function. Optional
   * does not require a start value as optional is a wrapper class kinda, it is a class that just stores a value.
   * Sum, min, max, average, and string concatenation are all special cases of reduction and have an alternate representation with the
   * method reference syntax (::). The function given must be associative or transitive as the order of the function with the inputs is not
   * guaranteed.
   *
   * Ps: reduce terminates streams, cant use it as a stream again
   *
   */
}
