package com.psybergate.grad2021.core.lambdas;

public class definition {

  /**
   * There exists a functional interface A - i.e has one abstract method
   *
   * A lambda is basically a sugar syntax way of implementing this functional interface as an INNER CLASS (ps: inner class is an object)
   * We then implement / override the method in interface A in the implementation (inner class / lambda)
   * We can now assign this object to a variable to use this version anywhere
   *
   * so now when we say object.methodA(), we are calling the overridden method on that object/implementation! each object/implementation would have a
   * different version of how it overwrote the functional interface
   *
   * If we assign the object to a variable, it has to be of type INTERFACE A or its children interfaces
   */

  public static void main(String[] args) {
  }
}
