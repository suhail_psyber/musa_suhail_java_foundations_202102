package com.psybergate.grad2021.core.concurrency.hw1_waitandnotify;

import java.util.ArrayList;
import java.util.List;

/**
 * The Writer writes to this board and the reader reads from it
 * Made to be a singleton -
 */
public class MessageBoard {

  private static final MessageBoard MESSAGE_BOARD = new MessageBoard(); //So we are always pointing to the same board

  public static MessageBoard getInstanceOfMessageBoard() {
    return MESSAGE_BOARD;
  }

  private List<String> myMessages = new ArrayList<>();

  private MessageBoard() { //singleton - this class cannot be instantiated anywhere else
  }

  public void addToMessageBoard(String message) {
    myMessages.add(message);
    notify();
  }

  public String readFromMessageBoard() throws InterruptedException {
    wait();
    if (myMessages.isEmpty())
      return null;
    return myMessages.remove(0); //removes from list AND RETURNS IT
  }

}
