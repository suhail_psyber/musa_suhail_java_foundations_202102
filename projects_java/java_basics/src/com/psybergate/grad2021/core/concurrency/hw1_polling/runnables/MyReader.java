package com.psybergate.grad2021.core.concurrency.hw1_polling.runnables;

import com.psybergate.grad2021.core.concurrency.hw1_polling.MessageBoard;

public class MyReader implements Runnable {

  @Override
  public void run() {
    MyReader reader = new MyReader();

    while (true) {
      try {
        System.out.println("Reading from Message Board...");
        reader.readMessage();
        Thread.sleep(2000);
      } catch (InterruptedException e) {

      }
    }

  }

  private void readMessage() {
    while (true) {
      String message = MessageBoard.getInstanceOfMessageBoard().readFromMessageBoard();
      if (message == null) {
        break;
      }
      System.out.println("Message read: " + message);
    }
  }

}
