package com.psybergate.grad2021.core.concurrency.hw1_polling;

import com.psybergate.grad2021.core.concurrency.hw1_polling.runnables.MyReader;
import com.psybergate.grad2021.core.concurrency.hw1_polling.runnables.MyWriter;

public class Client {

  public static void main(String[] args) throws InterruptedException {
    Thread myWriter = new Thread(new MyWriter());
    Thread myReader = new Thread(new MyReader());

    myWriter.start();
    myReader.start();

    myWriter.join();
    myReader.join();

  }
}
