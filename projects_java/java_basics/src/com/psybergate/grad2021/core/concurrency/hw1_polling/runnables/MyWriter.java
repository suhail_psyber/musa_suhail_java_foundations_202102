package com.psybergate.grad2021.core.concurrency.hw1_polling.runnables;

import com.psybergate.grad2021.core.Utils;
import com.psybergate.grad2021.core.concurrency.hw1_polling.MessageBoard;

public class MyWriter implements Runnable {

  /**
   * QUESTION - WHY DID CHRIS DO THIS IN A SEPARATE CLASS, I VIEW THIS AS THE JOB OF THE WRITER ITSELF TO BE ABLE TO WRITE IN ANOTHER THREAD
   * MUCH LIKE HOW WE HAVE OBJECTS THAT MIGHT NOT USE ALL ITS METHODS, WE DON'T HAVE TO USE THE RUN FOR SINGLE THREAD EXECUTIONS
   */
  @Override
  public void run() {
    int i = 0;
    MyWriter writer = new MyWriter();

    while (true) {
      try {
        String msg = "Random Message: " + Utils.random(1, 1000);
        writer.writeMessage(msg);
        Thread.sleep(5000);
        i++;

        if (i >= 20) {
          break;
        }

      } catch (InterruptedException e) {

      }
    }
  }

  public void writeMessage(String message) {
    MessageBoard.getInstanceOfMessageBoard().addToMessageBoard(message); //adding a message to the exiting message board
    System.out.println("Message written: " + message);
  }

}
