package com.psybergate.grad2021.core.concurrency.test;

public class Adder {

  private int value = 0;

  public Adder() {
  }

  public void add() {
    if (value > 0) {
      value = 1;
    } else {
      value = -1;
    }
  }

  public int getValue() {
    return value;
  }

}
