package com.psybergate.grad2021.core.concurrency.test;

public class Client {
  public static void main(String[] args) throws InterruptedException {
    /**
     * even number of adders makes in -ve
     * odd number of adders makes in +ve
     */
    Adder a1 = new Adder();


    /*ALWAYS PRINTS -1*/
    //Worker w1 = new Worker(a1);
    //w1.start();
    //w1.join();
    //
    //Worker w2 = new Worker(a1);
    //w2.start();
    //w1.join();
    //System.out.println(a1.getValue());

    /*MESSES UP COULD PRINT -1 OR 1*/
    Worker w1 = new Worker(a1);
    w1.start();
    Worker w2 = new Worker(a1);
    w2.start();

    w1.join();
    w1.join();
    System.out.println(a1.getValue());
  }

}

class Worker extends Thread {

  private Adder adder;

  public Worker(Adder adder) {
    this.adder = adder;
  }

  @Override
  public void run() {
    adder.add();
  }
}