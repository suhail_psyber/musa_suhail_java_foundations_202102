package com.psybergate.grad2021.core.concurrency.ce3a;

public class MyAdder implements Runnable {

  private int startNum;
  private int EndNum;
  private int total;

  public MyAdder(int startNum, int EndNum) {
    this.startNum = startNum;
    this.EndNum = EndNum;
  }

  @Override
  public void run() {
    sumTo(startNum, EndNum);
  }

  private int sumTo(int startNum, int endNum) {
    for (int i = startNum; i <= endNum; i++) {
      total += i;
    }
    return total;
  }

  public int getTotal() {
    return total;
  }
}

