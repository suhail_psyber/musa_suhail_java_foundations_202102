package com.psybergate.grad2021.core.concurrency.ce3a;

public class MainWithThreads {

  public static void main(String[] args) throws InterruptedException {
    long startTime = System.nanoTime();

    /*ToDo: CHANGE ME*/
    int startNum = 1;
    int endNum = 1000_000_000;
    int numOfThreads = 1;
    int span = (int) Math.floor(endNum / numOfThreads);

    int tempStart = startNum;
    int tempEnd = tempStart + span - 1;
    int totalSum = getTotalSum(endNum, numOfThreads, tempStart, span, tempEnd);

    long endTime = System.nanoTime();

    System.out.println("The sum of integers from " + startNum + " to " + endNum + " is: " + totalSum);
    long timeElapsed = endTime - startTime;
    System.out.println("timeElapsed = " + timeElapsed + "ns");
    System.out.println("timeElapsed = " + timeElapsed / 1000000 + "ms");

  }

  private static int getTotalSum(int endNum, int numOfThreads, int tempStart, int span, int tempEnd) throws InterruptedException {
    int totalSum = 0;

    for (int i = 1; i <= numOfThreads; i++) {

      if (i == numOfThreads) {
        tempEnd = endNum;
      }

      MyAdder runnable = new MyAdder(tempStart, tempEnd);
      Thread thread = new Thread(runnable);
      thread.start();
      thread.join();//makes main wait for this to thread to complete before moving on which doesn't have concurrency

      totalSum += runnable.getTotal();

      tempStart = tempEnd + 1;
      tempEnd = tempStart + span - 1;
    }

    return totalSum;
  }

}
