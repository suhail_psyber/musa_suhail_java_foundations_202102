package com.psybergate.grad2021.core.concurrency.ce1a.same_object;

public class Client2 {
  public static void main(String[] args) {

    Clazz2 clazz2 = new Clazz2("abc");

    /*MAIN THREAD*/
    System.out.println("I am the main thread");
    clazz2.method1();

    /*NEW THREAD*/
    Runnable r1 = clazz2;
    Thread t1 = new Thread(r1);
    t1.start();

    try {
      t1.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

  }

}
