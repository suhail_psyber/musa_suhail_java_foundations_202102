package com.psybergate.grad2021.core.concurrency.hw1_waitandnotify.runnables;

import com.psybergate.grad2021.core.Utils;
import com.psybergate.grad2021.core.concurrency.hw1_waitandnotify.MessageBoard;

public class MyWriter implements Runnable {

  private boolean isRunning = true;

  /**
   * QUESTION - WHY DID CHRIS DO THIS IN A SEPARATE CLASS, I VIEW THIS AS THE JOB OF THE WRITER ITSELF TO BE ABLE TO WRITE IN ANOTHER THREAD
   * MUCH LIKE HOW WE HAVE OBJECTS THAT MIGHT NOT USE ALL ITS METHODS, WE DON'T HAVE TO USE THE RUN FOR SINGLE THREAD EXECUTIONS
   */
  @Override
  public void run() {
    MyWriter writer = new MyWriter();

    for (int i = 0; i <= 20; i++) {
      String msg = "Random Message: " + Utils.random(1, 1000);
      writer.writeMessage(msg);
      i++;
    }

  }

  public void writeMessage(String message) {
    synchronized (MessageBoard.getInstanceOfMessageBoard()) {
      MessageBoard.getInstanceOfMessageBoard().addToMessageBoard(message); //adding a message to the exiting message board
    }
    System.out.println("Message written: " + message);
  }

}
