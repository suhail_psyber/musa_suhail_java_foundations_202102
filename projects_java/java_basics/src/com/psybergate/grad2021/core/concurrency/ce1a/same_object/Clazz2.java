package com.psybergate.grad2021.core.concurrency.ce1a.same_object;

public class Clazz2 implements Runnable {

  private String str;

  public Clazz2(String string) {
    this.str = string;
  }

  @Override
  public void run() {
    System.out.println("I am in Clazz2's run()");
    method1();
  }

  public void method1() {
    try {
      method2();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void method2() throws Exception {
    method3();
  }

  private void method3() throws Exception {
    System.out.println("CLAZZ2 throws an Exception next");
    throw new Exception();
  }

  public String getStr() {
    return str;
  }
}
