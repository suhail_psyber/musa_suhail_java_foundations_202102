package com.psybergate.grad2021.core.concurrency.ce1a.diff_objects;

public class Clazz1 implements Runnable {
  @Override
  public void run() {
    System.out.println("I am in Clazz1's run()");
    method1();
  }

  private void method1() {
    try {
      method2();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private void method2() throws Exception {
    method3();
  }

  private void method3() throws Exception {
    System.out.println("CLAZZ1 throws an Exception next");
    throw new Exception();
  }
}
