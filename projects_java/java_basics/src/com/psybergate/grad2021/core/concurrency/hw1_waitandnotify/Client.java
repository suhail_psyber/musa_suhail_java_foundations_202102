package com.psybergate.grad2021.core.concurrency.hw1_waitandnotify;

import com.psybergate.grad2021.core.concurrency.hw1_waitandnotify.runnables.MyReader;
import com.psybergate.grad2021.core.concurrency.hw1_waitandnotify.runnables.MyWriter;

public class Client {

  public static void main(String[] args) throws InterruptedException {
    Thread myWriter = new Thread(new MyWriter(), "WRITER_THREAD");
    Thread myReader = new Thread(new MyReader(), "READER_THREAD");

    myWriter.start();
    myReader.start();

    myWriter.join();
    myReader.join();

  }
}
