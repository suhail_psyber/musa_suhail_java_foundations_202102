package com.psybergate.grad2021.core.concurrency.ce1a.diff_objects;

import com.psybergate.grad2021.core.concurrency.ce1a.BadCodeException;

public class Client1 {
  public static void main(String[] args) {
    /*MAIN THREAD*/
    System.out.println("I am the main thread");
    method1();

    /*THREAD1 THREAD*/
    Runnable runnable1 = new Clazz1();
    Thread thread1 = new Thread(runnable1);
    thread1.start();

    /*THREAD2 THREAD*/
    Runnable runnable2 = new Runnable() {
      @Override
      public void run() {
        System.out.println("I am in an anonymous' inner class' run()");
        try {
          System.out.println("ANONYMOUS RUNNABLE throw an Exception next");
          throw new BadCodeException();
        } catch (BadCodeException e) {
          e.printStackTrace();
        }
      }
    };
    Thread thread2 = new Thread(runnable2);
    thread2.start();

    try {
      thread1.join();
      thread2.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

  }

  private static void method1() {
    try {
      method2();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static void method2() throws Exception {
    method3();
  }

  private static void method3() throws Exception {
    System.out.println("MAIN throws an Exception next");
    throw new BadCodeException();
  }

}
