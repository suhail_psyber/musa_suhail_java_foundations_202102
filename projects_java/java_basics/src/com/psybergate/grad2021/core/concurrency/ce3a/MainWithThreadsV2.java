package com.psybergate.grad2021.core.concurrency.ce3a;

import java.util.HashMap;
import java.util.Map;

public class MainWithThreadsV2 {

  public static void main(String[] args) throws InterruptedException {
    long startTime = System.nanoTime();

    /*ToDo: CHANGE ME*/
    int startNum = 1;
    int endNum = 1000_000;
    int numOfThreads = 50;
    int span = (int) Math.floor(endNum / numOfThreads);

    int totalSum = addConcurrently(startNum, endNum, numOfThreads, span);

    long endTime = System.nanoTime();
    long timeElapsed = endTime - startTime;

    System.out.println("The sum of integers from " + startNum + " to " + endNum + " is: " + totalSum);
    System.out.println("timeElapsed = " + timeElapsed / 1000000 + "ms");
  }

  private static int addConcurrently(int startNum, int endNum, int numOfThreads, int span) throws InterruptedException {
    int totalSum = 0;
    int tempStart = startNum;
    int tempEnd = tempStart + span - 1;

    Map<Thread, MyAdder> threadsAndRunnables = new HashMap<>();

    for (int i = 1; i <= numOfThreads; i++) {
      if (i == numOfThreads) {
        tempEnd = endNum;
      }
      MyAdder runnable = new MyAdder(tempStart, tempEnd);
      Thread thread = new Thread(runnable);
      thread.start();
      threadsAndRunnables.put(thread,runnable);
      tempStart = tempEnd + 1;
      tempEnd = tempStart + span - 1;
    }
    for (Thread key : threadsAndRunnables.keySet()) {
      key.join();
      totalSum += threadsAndRunnables.get(key).getTotal();
    }
    return totalSum;
  }

}
