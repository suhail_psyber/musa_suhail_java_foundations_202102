package com.psybergate.grad2021.core.concurrency.ce3a;

public class MainWithoutThreads {
  public static void main(String[] args) {
    long startTime = System.nanoTime();

    int startNum = 1;
    int endNum = 1000_000;
    int totalSum = sumTo(startNum, endNum);

    long endTime = System.nanoTime();
    long elapsedTime = endTime - startTime;
    System.out.println("The sum of integers from " + startNum + " to " + endNum + " is: " + totalSum);
    System.out.println("elapsedTime = " + elapsedTime + "ns");
    System.out.println("elapsedTime = " + elapsedTime / 1000000 + "ms");
  }

  private static int sumTo(int startNum, int endNum) {
    int total = 0;

    for (int i = startNum; i <= endNum; i++) {
      total += i;
    }

    return total;
  }
}
