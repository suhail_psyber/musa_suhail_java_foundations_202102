package com.psybergate.grad2021.core.concurrency.hw1_waitandnotify.runnables;

import com.psybergate.grad2021.core.concurrency.hw1_waitandnotify.MessageBoard;

public class MyReader implements Runnable {

  @Override
  public void run() {
    MyReader reader = new MyReader();

    while (true) {
      try {
        System.out.println("Reading from Message Board...");
        reader.readMessage();
      } catch (InterruptedException e) {
      }
    }

  }

  private void readMessage() throws InterruptedException {
    while (true) {
      synchronized (MessageBoard.getInstanceOfMessageBoard()) {
        String message = MessageBoard.getInstanceOfMessageBoard().readFromMessageBoard();
        if (message == null) {
          break;
        }
        System.out.println("Message read: " + message);
      }
    }
  }

}
