package com.psybergate.grad2021.core.langoverview.hw5a;

import com.psybergate.grad2021.core.langoverview.hw5a.diffpackage.*; //NEEDED

public class Hw5a_AccessModifiers extends Inheritance {
    public static void main(String[] args) {

        /**METHODS
         * Commented out code is not accessible*/
        //SamePackageNoInheritance.privateMethod();
        SamePackageNoInheritance.defaultMethod();
        SamePackageNoInheritance.protectedMethod();
        SamePackageNoInheritance.publicMethod();

        //Inheritance.privateMethod();
        //Inheritance.defaultMethod();
        Inheritance.protectedMethod();
        publicMethod();
        /*PS: we don't need to use the class name here because once we extend, it searches that class automatically*/

        //NoInheritance.privateMethod();
        //NoInheritance.defaultMethod();
        //NoInheritance.protectedMethod();
        NoInheritance.publicMethod();



        /**FIELDS (STATIC VARIABLES)
         * Can't just bring in the variables, we NEED to do something with it, thus we just print for now*/
        //System.out.println(SamePackageNoInheritance.str1);
        System.out.println(SamePackageNoInheritance.str2);
        System.out.println(SamePackageNoInheritance.str3);
        System.out.println(SamePackageNoInheritance.str4);

        //System.out.println(Inheritance.str1);
        //System.out.println(Inheritance.str2);
        System.out.println(Inheritance.str3);
        System.out.println(str4);
        /*PS: we don't need to use the class name here because once we extend, it searches that class automatically*/

        //System.out.println(SamePackageNoInheritance.str1);
        //System.out.println(NoInheritance.str2);
        //System.out.println(NoInheritance.str3);
        System.out.println(NoInheritance.str4);

    }
}
