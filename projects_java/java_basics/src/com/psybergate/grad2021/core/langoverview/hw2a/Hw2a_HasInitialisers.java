package com.psybergate.grad2021.core.langoverview.hw2a;

public class Hw2a_HasInitialisers {

    /** RUN ME to see Static Initialiser */
    public static void main(String[] args) {
        //code
    }

    /*Static Initialiser - braces with static keyword*/
    static {
        new RuntimeException();
        System.out.println("I exist on the class and run when the class in loaded by the JRE into the JVM");
        int classInt = 100;
        System.out.println("staticInt = " + classInt);
    }

    /*Initialiser (a.k.a Instance or Object Initialiser) - braces alone*/
    {
        System.out.println("I run when you make/create an object with the 'new' keyword");
        int objectInt = 5;
        System.out.println("objectInt = " + objectInt);
    }

    /*Static Fields or Variables are also called CLASS VARIABLES*/
}
