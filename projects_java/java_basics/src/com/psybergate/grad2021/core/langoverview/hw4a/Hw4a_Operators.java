package com.psybergate.grad2021.core.langoverview.hw4a;

public class Hw4a_Operators {

    public static void main(String[] args) {
        int a = 10; //primitive
        int b = 5; //primitive

        int c = b%a;
        c++;
        ++c;
        System.out.println("c = " + c);

        Integer x = new Integer(10);
        Integer xx = x;
        Integer y = new Integer(5);
        Integer newX = new Integer(10);

        if (a == 10) {
            System.out.println("primitive reference 'a' does have a value of '10'");
        }

        if (x == xx) {
            System.out.println("'x' and 'xx' both point to the same object");
        } else if (x.equals(xx)){
            System.out.println("'x' and 'xx point to TWO DIFFERENT objects but that just share the same state/value");
        }

        if (x == newX) {
            System.out.println("'x' and 'newX' both point to the same object");
        } else if (x.equals(newX)){
            System.out.println("'x' and 'newX point to TWO DIFFERENT objects but that just share the same state/value");
        }

        //These WON'T print as there is at least ONE FALSE
        if (true & true & true & false & true & true) {
            System.out.println("& - I check ALL the statements");
        }
        if (true && true && true && false && true && true) {
            System.out.println("& - I stop as soon as I find one statement that fails");
        }

        //These WILL print as there is at least ONE TRUE
        if (true | true | true | false | true | true) {
            System.out.println("| - I check ALL the statements");
        }
        if (true || true || true || false || true || true) {
            System.out.println("| - I stop as soon as I find one statement that fails");
        }


        //Ternary operator with a case and switch
        int t = a > b ? a : b;

        switch (t) {
            case 10:
                System.out.println("'a' was bigger than 'b': t = a = " + t);
                break;
            case 5:
                System.out.println("'a' was smaller than 'b': t = b = " + t);
                break;
        }



    }
}
