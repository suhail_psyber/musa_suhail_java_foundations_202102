package com.psybergate.grad2021.core.langoverview.hw7a;

public class Hw7a_Final {

    public static void main(String[] args) {

    }

    /*METHODS*/
    private final void finalMethod() {
        //Method cannot be overridden with @Override
    }

    private void nonFinalMethod() {
        //Method can be overridden with @Override
    }

    /*CONSTRUCTOR*/
    public Hw7a_Final() {

    }

    //public final Hw7a_Final() {
        //Constructors cannot be final - by def it should produce different objects
    //}



    /*CLASS*/
    final class Hw7a_FinalSubClass {
        //A final class means that you cannot inherit (extend) from this class
    }

}
