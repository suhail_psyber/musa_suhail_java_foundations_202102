package com.psybergate.grad2021.core.langoverview.hw5a;

public class SamePackageNoInheritance {

    private static String str1 = "pvtString";
    static String str2 = "defString";
    protected static String str3 = "protectedString";
    public static String str4 = "pubString";

    private static void privateMethod() {
        System.out.println("I can access Private methods");
    }

    static void defaultMethod() {
        System.out.println("I can access Default methods");
    }

    protected static void protectedMethod() {
        System.out.println("I can access Protected methods");
    }

    public static void publicMethod() {
        System.out.println("I can access Public methods");
    }
}
