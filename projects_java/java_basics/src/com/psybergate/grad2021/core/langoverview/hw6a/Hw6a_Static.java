package com.psybergate.grad2021.core.langoverview.hw6a;

public class Hw6a_Static {

    public static void main(String[] args) {
        System.out.println("New Object in main: ");
        System.out.println(new Hw6a_Static("abc").nonStatStr); //you cannot view the entire object through a print. You get a variable or invoke a method. Here the methods display first because they are called and printed during object creation, only after the object is created do we then print the string

        System.out.println("\nMethods in main: ");
        statMethod();
        //nonStatMethod(); //Fails to compile - non static method cannot be called in a static context (main is static)
    }

    /*VARIABLE DECLARATION*/
    private static String statStr = "This is a static string"; //Exists at class level
    private String nonStatStr = "xyz"; //Exists at Instance/Object level - no use making this here as it is needed as an input for the constructor

    /*CONSTRUCTORS*/
    public Hw6a_Static(String nonStatStr) {
        this.nonStatStr = nonStatStr; //Can only call on non-static variables as this constructs and object and is
        // non static itself
        statMethod();
        nonStatMethod();
    }

    /*static public Hw6a_Static(String statStr) {
        *This constructor cannot compile as a constructor is used to CREATE OBJECTS and by definition cannot solely exist at a
        *class level but has to be an INSTANCE METHOD (Constructors are a form of methods after all)
    } */

    /*METHODS*/
    private static void statMethod() {
        System.out.println("This is a static method / Class Method");
        //This cannot know what variables are available to it in terms of a new object unless those variables are static as well!!!
    }

    private void nonStatMethod () {
        System.out.println("This is a non static method / Instance Method");
        //Runs against an object and thus can see the variables in an object!!! We don't need to put it in as a parameter. An object knows its own state!!!
    }

}
