package com.psybergate.grad2021.core.langoverview.hw5a.diffpackage;

public class Inheritance {

    private static String str1 = "pvtString";
    static String str2 = "defString";
    protected static String str3 = "protectedString";
    public static String str4 = "pubString";

    private static void privateMethod() {
        System.out.println("I can access Private methods in a different PACKAGE with Inheritance");
    }

    static void defaultMethod() {
        System.out.println("I can access Default methods in a different PACKAGE with Inheritance");
    }

    protected static void protectedMethod() {
        System.out.println("I can access Protected methods in a different PACKAGE with Inheritance");
    }

    public static void publicMethod() {
        System.out.println("I can access Public methods in a different PACKAGE with Inheritance");
    }
}
