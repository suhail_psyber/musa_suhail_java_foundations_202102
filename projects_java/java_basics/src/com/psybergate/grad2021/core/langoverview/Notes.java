package com.psybergate.grad2021.core.langoverview;

public interface Notes {

    /**
     * 1) "FINAL" affects the REFERENCE and not the actual object... we cannot change the object reference but we can change the STATES of that object
     * 2) STATIC VARIABLES and INITIALISERS execute during CLASS LOADING (JAVA ClassLoader in part of the JRE and dynamically loads classes into the JVM) and NOT Object Creation
     * 3) INSTANCE VARIABLES and INITIALISERS execute when an OBJECT is CREATED with the "new" keyword
     * 4) Static FINAL vs Static - Both exist on the class but the difference is???
     * 5) You can put put "final" in the parameter of a constructor... This means that once you assign variables to a certain object, it cannot be changed in future!
     * 6) Static also CONSTRAINS polymorphism - you can't be both
     * 7) Static methods do not override (But it inherits!) even if the child has an overriding method - It will still take the parent static method and not the child static method
     * 8) The compiler only tracks the REFERENCE TYPE and the JVM (at runtime) runs against an object - and fails if we
     * cast an object to a different object ... Think of "Orders" example from Chris
     * 9) "public static final" variables HAVE to be assigned upon declaration - because of the final keyword? test out
     * 10)
     * */
}
