package com.psybergate.grad2021.core.binary.hw1a;

public class Hw1a_Binary {
    public static void main(String[] args) {

        /*PS: Find the source code of CLASSES using CTRL+N*/

        System.out.println("minByte = " + minByte);
        System.out.println("minByte2 = " + minByte2);
        System.out.println("maxByte = " + maxByte);
        System.out.println("maxByte2 = " + maxByte2);
        System.out.println("minShort = " + minShort);
        System.out.println("maxShort = " + maxShort);
        System.out.println("minInt = " + minInt);
        System.out.println("minInt2 = " + minInt2);
        System.out.println("maxInt = " + maxInt);
        System.out.println("maxInt2 = " + maxInt2);
        System.out.println("minLong = " + minLong);
        System.out.println("maxLong = " + maxLong);
    }

    /**The "0b" part tells java what you are writing in (in this case, binary),
    * However, Java still expects a value the size of an int (integer) and thus
    * we need to CAST the value, this CAST tells java how many bits to look at
    * when it wants to process the number*/

    /*Writing in hex form "0x", we need only the FIRST bit ON or OFF to represent +ve or -ve
    * so we start with 7, starting with F implies the first TWO bits are ON*/

    static byte minByte = (byte) 0b1000_0000; //1Byte, 2Hex, 8bits
    static byte minByte2 = (byte) 0x80;
    static byte maxByte = (byte) 0b0111_1111;
    static byte maxByte2 = (byte) 0x7F;
    static short minShort = (short) 0b1000_0000_0000_0000; //2Bytes, 4Hex, 16bits
    static short maxShort = (short) 0b0111_1111_1111_1111;
    //We are allowed to type more than the required bits for the cases above because java uses the memory of an INT by default
    static int minInt = 0b1000_0000_0000_0000_0000_0000_0000_0000; //4Bytes, 8Hex, 32bits
    static int minInt2 = 0x8000_0000; //1000 in binary = 8 in Hex
    static int maxInt = 0b0111_1111_1111_1111_1111_1111_1111_1111;
    static int maxInt2 = 0x7FFF_FFFF; //0111 in binary = 7 in Hex
    //We have to use the suffix "L" because the memory required is MORE than the default of INT
    static long minLong = 0x8000_0000_0000_0000L; //8Bytes, 16Hex, 64bits
    static long maxLong = 0x7FFF_FFFF_FFFF_FFFFL;



}
