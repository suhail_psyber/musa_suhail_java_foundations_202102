package com.psybergate.grad2021.core.enums;

public enum Test {
  //Enums are for FIXED number of Objects from this abstraction!!!
  TEST01, TEST02("abc"); // creates an object of Type Test with name TEST01 - /*public static final Test TEST03 = new Test();*/
  private String name;

  Test() { // private by default - default constructor

  }

  private Test(String name) { // private by default - constructor with a parameter
    this.name = name;
  }
}
