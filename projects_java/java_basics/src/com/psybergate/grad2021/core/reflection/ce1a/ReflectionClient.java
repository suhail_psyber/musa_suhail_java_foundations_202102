package com.psybergate.grad2021.core.reflection.ce1a;

import com.psybergate.grad2021.core.annotations.hw2.config.DomainClass;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionClient {
  public static void main(String[] args) throws ClassNotFoundException, InvocationTargetException, IllegalAccessException {
    System.out.println("Student.class = " + Student.class);
    System.out.println("new Student().getClass() = " + new Student().getClass());
    System.out.println("Class.forName(\"com.psybergate.grad2021.core.reflection.ce1a.Student\") = " + Class.forName("com.psybergate.grad2021.core.reflection.ce1a.Student"));
    System.out.println("");

    System.out.println("Student.class.getPackage() = " + Student.class.getPackage());
    System.out.println("new Student().getClass().getPackage() = " + new Student().getClass().getPackage());
    System.out.println("Class.forName(\"com.psybergate.grad2021.core.reflection.ce1a.Student\").getPackage() = " + Class.forName("com.psybergate.grad2021.core.reflection.ce1a.Student").getPackage());
    System.out.println("");

    System.out.println("Student.class.getAnnotation(DomainClass.class) = " + Student.class.getAnnotation(DomainClass.class));
    Annotation[] studentClassAnnotations = Student.class.getAnnotations();
    for (Annotation specificAnnotation : studentClassAnnotations) {
      System.out.println("specificAnnotation.annotationType().getName() = " + specificAnnotation.annotationType().getName());
    }
    System.out.println("");

    System.out.println("Student.class.getInterfaces()[0].getName() = " + Student.class.getInterfaces()[0].getName());
    System.out.println("");

    Field[] fields = Student.class.getFields(); //gives only public
    for (Field field : fields) {
      System.out.println("field.getName() = " + field.getName());
    }
    System.out.println("");

    Field[] declaredFields = Student.class.getDeclaredFields(); //gives all regardless of access
    for (Field declaredField : declaredFields) {
      System.out.println("declaredField.getName() = " + declaredField.getName());
    }
    System.out.println("");

    Constructor[] constructors = Student.class.getDeclaredConstructors();
    for (Constructor constructor : constructors) {
      System.out.println("constructor.getName() = " + constructor.getName());
      Class[] parameters = constructor.getParameterTypes();
      for (Class parameter : parameters) {
        System.out.println("parameter.getName() = " + parameter.getName());
      }
    }
    System.out.println("");

    Constructor[] publicConstructors = Student.class.getConstructors();
    for (Constructor publicConstructor : publicConstructors) {
      System.out.println("publicConstructor.getName() = " + publicConstructor.getName());
    }
    System.out.println("");

    Method[] methods = Student.class.getDeclaredMethods();
    for (Method method : methods) {
      System.out.println("method.getName() = " + method.getName());
      method.invoke(new Student());
    }
  }
}
