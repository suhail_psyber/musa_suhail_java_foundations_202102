package com.psybergate.grad2021.core.reflection.ce1a;

import com.psybergate.grad2021.core.annotations.hw2.config.DomainClass;

import java.io.Serializable;

@DomainClass
public class Student implements Serializable {

  public String studentNum;

  private String name;

  private String surname;

  private Boolean isNewStudent;

  private int age;

  public Student() {
    this("100", "john", "smith", true, 18);
  }

  public Student(String studentNum, String name, String surname, Boolean isNewStudent) {
    this(studentNum, name, surname, isNewStudent, 18);
  }

  private Student(String studentNum, String name, String surname, Boolean isNewStudent, int age) {
    this.studentNum = studentNum;
    this.name = name;
    this.surname = surname;
    this.isNewStudent = isNewStudent;
    this.age = age;
  }

  public void publicMethod() {
    System.out.println("I am a public method in student");
  }

  public static String publicStaticMethod() {
    return "I am a public static method in student";
  }

  private void privateMethod() {
    System.out.println("I am a private method in student");
  }

}
