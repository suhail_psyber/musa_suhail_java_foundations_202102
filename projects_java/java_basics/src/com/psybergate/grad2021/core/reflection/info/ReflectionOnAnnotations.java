package com.psybergate.grad2021.core.reflection.info;

import com.psybergate.grad2021.core.annotations.hw2.config.DomainClass;
import com.psybergate.grad2021.core.annotations.hw2.config.DomainProperty;
import com.psybergate.grad2021.core.annotations.hw2.domain.Customer;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ReflectionOnAnnotations {
  public static void main(String[] args) throws NoSuchMethodException, NoSuchFieldException {
    allClassAnnotations();
    specificClassAnnotation();
    allMethodAnnotations();
    specificMethodAnnotation();
    allFieldAnnotations();
    specificFieldAnnotation();
  }

  public static void allClassAnnotations() {
    Class myClass = Customer.class;
    Annotation[] annotations = myClass.getAnnotations();

    for (Annotation annotation : annotations) {
      if (annotation instanceof DomainClass) {
        DomainClass myAnnotation = (DomainClass) annotation;
        System.out.println("name: " + myAnnotation.name());
        System.out.println("value: " + myAnnotation.value());
      }
    }
  }

  /*In our homework example, we only have ONE annotation per class, if a class had 2 (such as deprecated) we can specify the annotation we care
  about here instead of getting ALL of them*/

  public static void specificClassAnnotation() {
    Class myClass = Customer.class;
    Annotation annotation = myClass.getAnnotation(DomainClass.class); //.getClass() only works on an OBJECT;

    if (annotation instanceof DomainClass) {
      DomainClass myAnnotation = (DomainClass) annotation;
      System.out.println("name: " + myAnnotation.name());
      System.out.println("value: " + myAnnotation.value());
    }
  }

  public static void allMethodAnnotations() {
    Method[] method = Customer.class.getDeclaredMethods(); //obtain method object - getMethods() would only work for static methods AND incl inherited
    Annotation[] annotations = method[0].getDeclaredAnnotations(); //gets ALL the annotations on a specific method
    //Can add an additional for loop here to run through ALL the methods in the method array - PS: we are only using the first method above
    for (Annotation annotation : annotations) {
      if (annotation instanceof DomainProperty) {
        DomainProperty myAnnotation = (DomainProperty) annotation;
        System.out.println("name: " + myAnnotation.nullable());
        System.out.println("value: " + myAnnotation.isKey());
      }
    }
  }

  public static void specificMethodAnnotation() throws NoSuchMethodException {
    Method method = Customer.class.getMethod("getCustomerNumber"); // obtain method object
    Annotation annotation = method.getAnnotation(DomainProperty.class); //could check for ANY annotation here

    if (annotation instanceof DomainProperty) {
      DomainProperty myAnnotation = (DomainProperty) annotation;
      System.out.println("name: " + myAnnotation.nullable());
      System.out.println("value: " + myAnnotation.isKey());
    }
  }

  public static void allFieldAnnotations() {
    Field[] fields = Customer.class.getDeclaredFields(); //getFields() would only work for static fields AND include inherited fields
    Annotation[] annotations = fields[1].getAnnotations(); //fields[1] = name

    for (Annotation annotation : annotations) {
      if (annotation instanceof DomainProperty) {
        DomainProperty myAnnotation = (DomainProperty) annotation;
        System.out.println("nullable: " + myAnnotation.nullable());
        System.out.println("unique: " + myAnnotation.isKey());
      }
    }
  }

  public static void specificFieldAnnotation() throws NoSuchFieldException {
    Field field = Customer.class.getDeclaredField("customerNumber"); //obtain field object
    Annotation annotation = field.getAnnotation(DomainProperty.class);

    if (annotation instanceof DomainProperty) {
      DomainProperty myAnnotation = (DomainProperty) annotation;
      System.out.println("name: " + myAnnotation.nullable());
      System.out.println("value: " + myAnnotation.isKey());
    }
  }

}
