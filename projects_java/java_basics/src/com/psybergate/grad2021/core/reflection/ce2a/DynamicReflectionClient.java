package com.psybergate.grad2021.core.reflection.ce2a;

import com.psybergate.grad2021.core.reflection.ce1a.Student;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class DynamicReflectionClient {
  public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {

    /*Creating a Student object with reflection*/
    Constructor<Student> defaultConstructor = Student.class.getConstructor();
    Student student1 = defaultConstructor.newInstance();

    Constructor<Student> fullConstructor = Student.class.getConstructor(String.class, String.class, String.class, Boolean.class);
    Student student2 = fullConstructor.newInstance("101", "Suhail", "Musa", true);

    /*Invoke a method - get a handle to a method then pass in an object to invoke it on*/
    Method invokeMethod = student1.getClass().getDeclaredMethod("publicMethod"); //we got a handle to the method
    invokeMethod.invoke(student2); //we can use this method on a different object/instance!!!


    Method invokeStaticeMethod = student1.getClass().getDeclaredMethod("publicStaticMethod");
    invokeStaticeMethod.invoke(Student.class);
    invokeStaticeMethod.invoke(student1); //will compile

    Method invokePrivateMethod = student1.getClass().getDeclaredMethod("privateMethod");
    //invokePrivateMethod.invoke(Student.class); //will not run as this object is not an instance of student but rather the class object
    invokePrivateMethod.invoke(student1); //will compile
  }
}
