package com.psybergate.grad2021.core.reflection.hw2a;

import java.util.ArrayList;

public class DynamicLoading {



  public static void main(String[] args) {
    ArrayList<String> months = (ArrayList<String>) Months.listOfMonths;

    int count = Months.count;

    for (String month : months) {
      System.out.println(++count + ". " + month);
    }
  }
}
