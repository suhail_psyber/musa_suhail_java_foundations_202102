package com.psybergate.grad2021.core.reflection.hw1a;

import com.psybergate.grad2021.core.reflection.ce1a.Student;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class Client {

  public static void main(String[] args) {
    Class studentClass = Student.class;

    /*Parent Class info*/
    System.out.println("Student.class.getSuperclass() = " + studentClass.getSuperclass());
    System.out.println("Student.class.getSuperclass() = " + studentClass.getClass().getSuperclass());
    System.out.println("");

    /*Modifiers info*/
    int classModifiers = studentClass.getModifiers();
    System.out.println("classModifiers = " + classModifiers);
    int modifier = studentClass.getModifiers();
    System.out.println("Modifier.toString(modifier) = " + Modifier.toString(modifier));
    System.out.println("");

    Field[] fields = studentClass.getDeclaredFields();
    for (Field field : fields) {
      int fieldModifiers = field.getModifiers();
      System.out.println("Field: " + field.getName() + " ... With Modifiers: " + Modifier.toString(fieldModifiers));
    }
    System.out.println("");

    Method[] methods = studentClass.getDeclaredMethods();
    for (Method method : methods) {
      int methodModifiers = method.getModifiers();
      String methodReturnType = method.getReturnType().getSimpleName();
      System.out.println("Method Name: " + method.getName() + " ... Returns a: " + methodReturnType + " ... With Modifiers: " + Modifier.toString(methodModifiers));
    }


  }

}
