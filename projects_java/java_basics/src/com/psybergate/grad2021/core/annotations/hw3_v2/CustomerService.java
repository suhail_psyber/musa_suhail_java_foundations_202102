package com.psybergate.grad2021.core.annotations.hw3_v2;

import com.psybergate.grad2021.core.annotations.hw2.domain.Customer;

import java.util.ArrayList;
import java.util.List;

import static com.psybergate.grad2021.core.annotations.hw3_v2.CustomerInfo.*;
import static com.psybergate.grad2021.core.annotations.hw3_v2.DatabaseManager.*;

public class CustomerService {

  public static void main(String[] args) {
    generateDb();

    List<Customer> customers = new ArrayList<>();

    saveCustomers(customers, 10);
    getCustomer(customers.get(2).getCustomerNumber());
  }

  private static void saveCustomers(List<Customer> customerList, int numOfCustomers) {
    addCustomers(customerList, numOfCustomers);
    DatabaseManager.saveCustomersToTableInDB(customerList);
    System.out.println(customerList.size() + " Customers added to the DB in table " + getSqlTableName());
  }

  private static void getCustomer(String customerNumber) {
    Customer customer = selectRowOfDataFromTable(customerNumber);
    System.out.println("customer.toString() = " + customer.toString());
    /*can add or remove transient information from a returned customer in selectRow.. method*/
  }
}
