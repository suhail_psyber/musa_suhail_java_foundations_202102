package com.psybergate.grad2021.core.annotations.hw3_v2;

import com.psybergate.grad2021.core.annotations.hw2.config.DomainClass;
import com.psybergate.grad2021.core.annotations.hw2.config.DomainProperty;
import com.psybergate.grad2021.core.annotations.hw2.domain.Customer;

import java.lang.reflect.Field;
import java.sql.*;
import java.util.List;

public class DatabaseManager {

  static void generateDb() {
    dropTable();
    createTableInDb();
  }

  private static void createTableInDb() { //generateDatabase() in homework
    System.out.println("Creating Table...");
    try {
      Connection c = connectToDb();
      Statement stmt = c.createStatement();

      Field[] fields = Customer.class.getDeclaredFields();
      String sqlPrefix = "CREATE TABLE " + getSqlTableName() + " ";
      String values = getSqlTableValues(fields);
      stmt.executeUpdate(sqlPrefix + values);

      stmt.close();
      c.commit();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Table created");
  }

  static void saveCustomersToTableInDB(List<Customer> customers) {
    System.out.println("Inserting data...");
    try {
      Connection c = connectToDb();
      Statement stmt = c.createStatement();

      String sqlPrefix = "INSERT INTO " + getSqlTableName() + " (customer_number, first_name, surname, date_of_birth) ";
      for (Customer customer : customers) {
        StringBuilder values = getSqlRowValues(customer);
        stmt.executeUpdate(sqlPrefix + values);
      }

      stmt.close();
      c.commit();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Data added successfully");
  }

  static Customer selectRowOfDataFromTable(String primaryKey) {
    System.out.println("Selecting row(s)...");
    Customer customer = null;
    try {
      Connection c = connectToDb();
      Statement stmt = c.createStatement();

      ResultSet rs = stmt.executeQuery("SELECT * FROM " + getSqlTableName() + " WHERE " + "(Customer_Number = '" + primaryKey + "');");
      while (rs.next()) {
        String id = rs.getString("customer_number");
        String name = rs.getString("first_name");
        String surname = rs.getString("surname");
        int dob = rs.getInt("date_of_birth");
        customer = new Customer(id, name, surname, dob, true);
      }

      rs.close();
      stmt.close();
      c.close();
      System.out.println("Data retrieved successful");
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    } finally {
      return customer;
    }
  }

  private static void dropTable() {
    System.out.println("Dropping Table...");
    try {
      Connection c = connectToDb();
      Statement stmt = c.createStatement();

      String sql = "DROP TABLE IF EXISTS " + getSqlTableName() + ";";
      stmt.executeUpdate(sql);

      stmt.close();
      c.commit();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Table dropped");
  }

  /*Utility Methods*/
  private static Connection connectToDb() throws ClassNotFoundException, SQLException {
    Connection c;
    Class.forName("org.postgresql.Driver");
    c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/grad2021", "postgres", "admin");
    c.setAutoCommit(false);
    return c;
  }

  static String getSqlTableName() {
    return Customer.class.getAnnotation(DomainClass.class).name();
  }

  private static String getSqlTableValues(Field[] fields) throws NoSuchFieldException {
    String values = "";

    for (Field field : fields) {

/*    DomainProperty test1 = Customer.class.getDeclaredField("age").getAnnotation(DomainProperty.class);
      System.out.println("test1 = " + test1);
      above is a code-check for the statement below
      When this gets to age (any field that does NOT have the DomainProperty annotation, we get - DomainProperty fieldAnnotation = null;*/

      DomainProperty fieldAnnotation = field.getAnnotation(DomainProperty.class);
      if (fieldAnnotation != null) {
        String colName = fieldAnnotation.name();
        String colType = " " + fieldAnnotation.type();
        String colConstraints = "";

        if (fieldAnnotation.isKey()) {
          colConstraints = " PRIMARY KEY";
        }
        if (!fieldAnnotation.nullable()) {
          colConstraints += " NOT NULL";
        }

        values += colName + colType + colConstraints + ", ";
      }
    }
    values = "(" + values.substring(0, values.length() - 2) + ");";
    return values;
  }

  private static StringBuilder getSqlRowValues(Customer customer) {
    return new StringBuilder("VALUES (" + "'" + customer.getCustomerNumber() + "', " + "'" + customer.getName() + "', " + "'" + customer.getSurname() + "', " + customer.getDateOfBirth() + ");");
  }

}
