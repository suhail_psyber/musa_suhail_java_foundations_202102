package com.psybergate.grad2021.core.annotations.hw2.config;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;

@Retention(RetentionPolicy.RUNTIME)
@Target({TYPE}) //see what other ones belong here
public @interface DomainClass {

  String name() default "default_tbl";

  int value() default 0;

}
