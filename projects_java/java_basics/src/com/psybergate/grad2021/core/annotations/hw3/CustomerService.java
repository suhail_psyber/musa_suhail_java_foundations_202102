package com.psybergate.grad2021.core.annotations.hw3;

import com.psybergate.grad2021.core.annotations.hw2.config.DomainClass;
import com.psybergate.grad2021.core.annotations.hw2.domain.Customer;

import java.util.ArrayList;
import java.util.List;

import static com.psybergate.grad2021.core.annotations.hw3.CustomerInfo.*;
import static com.psybergate.grad2021.core.annotations.hw3.DatabaseManager.*;

public class CustomerService {



  public static void main(String[] args) {
    dropTable();
    createTableInDb();

    List<Customer> customers = new ArrayList<>();
    saveCustomer(customers);
  }

  private static void saveCustomer(List<Customer> customers) {
    /**
     * Vary Number of Customers here
     */
    int customerLimit = 10;

    for (int numberOfCustomers = 1; numberOfCustomers <= customerLimit; numberOfCustomers++) {
      Customer c1 = new Customer(Rng(999,100) + "", nameGenerator("first"), nameGenerator("last"), dOBGenerator(), true);
      customers.add(c1);
      DatabaseManager.saveCustomerToTableInDB(c1);
      /*QUESTION - It might be better to pass in the entire list out of the forloop and add them all at once - that way we only make one connection
      and we can simply add a for loop in saveCustomerToTableInDB() to iterate through the list*/
    }

    System.out.println(customerLimit + " Customers added to the DB in table " + Customer.class.getAnnotation(DomainClass.class).name());
  }

  private static void viewCustomer(String customerNumber) {

    //crate a connection
    //use the select thing for which number
    //close connection
    String result = "";
    System.out.println(result);
  }

}
