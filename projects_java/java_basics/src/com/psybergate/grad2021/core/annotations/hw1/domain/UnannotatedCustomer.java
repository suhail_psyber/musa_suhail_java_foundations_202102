package com.psybergate.grad2021.core.annotations.hw1.domain;

public class UnannotatedCustomer {

  public String customerNumber;

  public String name;

  public String surname;

  public Integer dateOfBirth; //ddmmyyyy

  public int age;

  public UnannotatedCustomer(String customerNumber, String name, String surname, Integer dateOfBirth, int age) {
    this.customerNumber = customerNumber;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
    this.age = age;
  }
}
