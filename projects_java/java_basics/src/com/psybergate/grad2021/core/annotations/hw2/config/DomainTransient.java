package com.psybergate.grad2021.core.annotations.hw2.config;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({METHOD, FIELD, LOCAL_VARIABLE}) //see what other ones belong here
public @interface DomainTransient {

  //can we add something that tells us that this data needs to be calculated when pull the table/ objects from the db

}
