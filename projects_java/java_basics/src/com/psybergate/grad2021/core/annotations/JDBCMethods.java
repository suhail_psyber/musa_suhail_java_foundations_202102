package com.psybergate.grad2021.core.annotations;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class JDBCMethods {

  public static void main(String args[]) {
    dropTable();
    //connectingToDatabase(); //connect to testdb (an existing DB)
    createTableInDb(); //create TABLENAME_tbl in testdb
    insertRowOfDataToTable(); //add row(s) of data to TABLENAME_tbl
    //selectRowOfDataFromTable(); //return the rows of data from TABLENAME_tbl
    //updateRowOfDataInTable(); //edit a row of data in TABLENAME_tbl
    //deleteRowOfDataFromTable(); //delete the rows of data from TABLENAME_tbl
  }

  /**
   * DBMS (DataBaseManagementSystems) - A Server has many DBs which have many Schemas which have many tables which have many columns/rows
   *
   * DML (Data Manipulation Language) - Manipulates what we VIEW not the actual data
   * executeUpdate is for INSERT, DELETE, and UPDATE - returns and int (number of rows effected)
   * executeQuery is for SELECTING - returns the information as requested in a ResultSet - it creates another relation (table)
   */

  /**
   * JDBC (Java DB Connectivity) is an API that lets JAVA talk to a DB - in this case, postgres - using a library
   * If we should decide to use influxDB, we will need a different library so that JAVA can talk to that DB.
   * PS: an API converts information from one form to another ... like a waiter taking an order is simply given a meal name but then tells the cook
   * what ingredients he will need to use, temperatures to cook at, length of cooking etc
   *
   * 1) every method first establishes a connection, creates a statement object (so we can talk in SQL syntax), and is then given a SQL String
   * 2) commented out methods in update and delete also return the data AFTER processing (effectively uses retrieve method)
   */

  private static void connectingToDatabase() {
    Connection c = null;
    try {
      Class.forName("org.postgresql.Driver");
      c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/testdb", "postgres", "admin");
    } catch (Exception e) {
      e.printStackTrace();
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("***Opened database successfully***");
  }

  private static void createTableInDb() {
    Connection c = null;
    Statement stmt = null;
    try {
      Class.forName("org.postgresql.Driver");
      c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/testdb", "postgres", "admin");
      System.out.println("***Opened database successfully***");

      stmt = c.createStatement();
      /*Hard coding the columns of the table we want. The sequence is the column name, Type, and nullability*/
      String sql = "CREATE TABLE TABLENAME_tbl " + "(ID INT PRIMARY KEY NOT NULL," + "NAME TEXT NOT NULL, " + "AGE INT NOT NULL, " + "ADDRESS CHAR" +
              "(20), " + "SALARY REAL);";
      stmt.executeUpdate(sql);
      stmt.close();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Table created successfully");
  }

  private static void insertRowOfDataToTable() {
    Connection c = null;
    Statement stmt = null;
    try {
      Class.forName("org.postgresql.Driver");
      c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/testdb", "postgres", "admin");
      c.setAutoCommit(false);
      System.out.println("***Opened database successfully***");

      stmt = c.createStatement();
      String sqlPrefix = "INSERT INTO TABLENAME_tbl (ID,NAME,AGE,ADDRESS,SALARY) ";

      String values = "VALUES (1, 'Paul', 32, 'California', 20000.00 );";
      stmt.executeUpdate(sqlPrefix + values);

      values = "VALUES (2, 'Allen', 25, 'Texas', 15000.00 );";
      stmt.executeUpdate(sqlPrefix + values);

      values = "VALUES (3, 'Teddy', 23, 'Norway', 20000.00 );";
      stmt.executeUpdate(sqlPrefix + values);

      stmt.close();
      c.commit();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Record(s)/Row(s) added successfully");
  }

  private static void selectRowOfDataFromTable() {
    Connection c = null;
    Statement stmt = null;
    try {
      Class.forName("org.postgresql.Driver");
      c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/testdb", "postgres", "admin");
      c.setAutoCommit(false);
      System.out.println("***Opened database successfully***");

      stmt = c.createStatement();
      ResultSet rs = stmt.executeQuery("SELECT * FROM TABLENAME_tbl;");
      while (rs.next()) {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        int age = rs.getInt("age");
        String address = rs.getString("address");
        float salary = rs.getFloat("salary");
        System.out.println("ID = " + id + " | NAME = " + name + " | AGE = " + age + " | ADDRESS = " + address + " | SALARY = " + salary);
      }
      rs.close();

      stmt.close();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Retrieval successful");
  }

  private static void updateRowOfDataInTable() { //this actually deletes and adds the row back in (as seen by it appearing last)
    Connection c = null;
    Statement stmt = null;
    try {
      Class.forName("org.postgresql.Driver");
      c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/testdb", "postgres", "admin");
      c.setAutoCommit(false);
      System.out.println("***Opened database successfully***");

      stmt = c.createStatement();
      String sql = "UPDATE TABLENAME_tbl set SALARY = 25000.00 where ID=1;";
      stmt.executeUpdate(sql);
      c.commit();

      ResultSet rs = stmt.executeQuery("SELECT * FROM TABLENAME_tbl;");
      while (rs.next()) {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        int age = rs.getInt("age");
        String address = rs.getString("address");
        float salary = rs.getFloat("salary");
        System.out.println("ID = " + id + " | NAME = " + name + " | AGE = " + age + " | ADDRESS = " + address + " | SALARY = " + salary);
      }
      rs.close();

      stmt.close();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Update successful");
  }

  private static void deleteRowOfDataFromTable() {
    Connection c = null;
    Statement stmt = null;
    try {
      Class.forName("org.postgresql.Driver");
      c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/testdb", "postgres", "admin");
      c.setAutoCommit(false);
      System.out.println("***Opened database successfully***");

      stmt = c.createStatement();
      String sql = "DELETE from TABLENAME_tbl where ID = 2;";
      stmt.executeUpdate(sql);
      c.commit();

      /*ResultSet rs = stmt.executeQuery("SELECT * FROM TABLENAME_tbl;");
      while (rs.next()) {
        int id = rs.getInt("id");
        String name = rs.getString("name");
        int age = rs.getInt("age");
        String address = rs.getString("address");
        float salary = rs.getFloat("salary");
        System.out.println("ID = " + id + "| NAME = " + name + "| AGE = " + age + "| ADDRESS = " + address + "| SALARY = " + salary);
      }
      rs.close();*/

      stmt.close();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Row(s) deleted successfully");
  }

  private static void dropTable() {
    Connection c = null;
    Statement stmt = null;
    try {
      Class.forName("org.postgresql.Driver");
      c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/testdb", "postgres", "admin");
      c.setAutoCommit(false);
      System.out.println("***Opened database successfully***");

      stmt = c.createStatement();
      String sql = "DROP TABLE IF EXISTS TABLENAME_tbl;";
      stmt.executeUpdate(sql);
      c.commit();

      stmt.close();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Table deleted successfully");
  }
}
