package com.psybergate.grad2021.core.annotations.hw2.domain;

import com.psybergate.grad2021.core.annotations.hw2.config.DomainClass;
import com.psybergate.grad2021.core.annotations.hw2.config.DomainProperty;
import com.psybergate.grad2021.core.annotations.hw2.config.DomainTransient;

import java.time.LocalDate;

import static java.time.temporal.ChronoUnit.YEARS;

@DomainClass(name = "customer_table")
public class Customer {

  /**
   * Needs to be unique - it is the primary key
   */
  @DomainProperty(name = "Customer_Number", isKey = true)
  private String customerNumber;

  @DomainProperty(name = "First_Name", isKey = false, nullable = false)
  private String name;

  /**
   * Could be Null ... like Madonna for example does not have a surname
   */
  @DomainProperty(name = "Surname", nullable = true)
  private String surname;

  /**
   * Takes the form of yyyymmdd
   */
  @DomainProperty(name = "Date_Of_Birth", type = "INT") //same as name
  private Integer dateOfBirth;

  @DomainTransient
  private int age;

  public Customer(String customerNumber, String name, String surname, Integer dateOfBirth, boolean wantTransient) {
    this.customerNumber = customerNumber;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
    //other DomainProperties can be assigned here
    if (wantTransient) {
      StringBuilder dob = new StringBuilder("" + dateOfBirth);
      StringBuilder parsableDate = dob.insert(6, "-").insert(4, "-");
      this.age = (int) LocalDate.parse(parsableDate).until(LocalDate.now(), YEARS);
      //other DomainTransients can be assigned here
    }
  }

  public String getCustomerNumber() {
    return customerNumber;
  }

  public void setCustomerNumber(String customerNumber) {
    this.customerNumber = customerNumber;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public Integer getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(Integer dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  @Override
  public String toString() {
    if (age != 0 ) {
      return "{ " + "Customer Number: " + customerNumber + ", name: " + name + ", surname: " + surname + ", dateOfBirth: " + dateOfBirth + ", " +
              "age: " + age + " }";
    }
    return "{ " + "Customer Number: " + customerNumber + ", name: " + name + ", surname: " + surname + ", dateOfBirth: " + dateOfBirth + " }";
  }
}
