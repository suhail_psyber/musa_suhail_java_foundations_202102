package com.psybergate.grad2021.core.annotations.hw2.config;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({METHOD, FIELD, LOCAL_VARIABLE}) //see what other ones belong here
public @interface DomainProperty {

  String name() default "column";

  String type() default "TEXT";

  boolean nullable() default false;

  boolean isKey() default false;
}
