package com.psybergate.grad2021.core.annotations.hw1.config;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.CONSTRUCTOR;

@Retention(RetentionPolicy.RUNTIME)
@Target({CONSTRUCTOR}) //see what other ones belong here
public @interface DomainClassV1 {
}
