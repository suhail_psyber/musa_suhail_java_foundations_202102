package com.psybergate.grad2021.core.annotations.hw3_v2;

import com.psybergate.grad2021.core.annotations.hw2.domain.Customer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomerInfo {

  public static void addCustomers(List<Customer> customerList, int numOfCustomers) {
    List<String> listOfCustomerNumbers = customerNumbersGenerator(numOfCustomers);

    for (int i = 0; i < numOfCustomers; i++) {
      customerList.add(new Customer(listOfCustomerNumbers.get(i), nameGenerator("first"), nameGenerator("last"), dOBGenerator(), true));
    }
  }

  /*UTILITY METHODS*/
  public static int Rng(int highLimit, int lowLimit) {
    return (int) (Math.floor((Math.random() * (highLimit - lowLimit)) + lowLimit));
  }

  public static List<String> customerNumbersGenerator(int numberOfCustomers) {
    int currentCustomerNumber = 1000;
    List<String> listOfCustomerNumbers = new ArrayList<>();

    for (int i = 0; i < numberOfCustomers; i++) {
      listOfCustomerNumbers.add("CN" + currentCustomerNumber);
      currentCustomerNumber += Rng(10, 1);
    }
    System.out.println(listOfCustomerNumbers);
    return listOfCustomerNumbers;
  }

  public static String nameGenerator(String firstOrLast) {
    String randomName = null;
    List<String> names = Arrays.asList("Satori", "Max", "Naruto", "Eren", "Goku", "Sasuke", "Gohan", "Edward", "Peter", "Ragnar");

    switch (firstOrLast) {
      case "first":
        randomName = names.get(Rng(9, 0));
        break;
      case "last":
        if (Math.random() > 0.2) { //80% of people get a surname and 20% don't
          randomName = names.get(Rng(9, 0));
        }
        break;
      default:
        throw new IllegalStateException("Unexpected value: Please type in \"first\" or \"last\"");
    }

    return randomName;
  }

  public static Integer dOBGenerator() {
    int year = Rng(2005, 1980);
    int month = Rng(12, 1);
    int date = Rng(28, 1); //I'm going to ignore dates past this for logical consistency - complete after everything works

    String strDOB = "";

    strDOB += year;

    if (month < 10) {
      strDOB += "0" + month;
    } else {
      strDOB += month;
    }

    if (date < 10) {
      strDOB += "0" + date;
    } else {
      strDOB += date;
    }

    return Integer.parseInt(strDOB);
  }

}
