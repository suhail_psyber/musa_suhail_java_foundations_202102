package com.psybergate.grad2021.core.annotations.hw3;

import com.psybergate.grad2021.core.annotations.hw2.config.DomainClass;
import com.psybergate.grad2021.core.annotations.hw2.config.DomainProperty;
import com.psybergate.grad2021.core.annotations.hw2.domain.Customer;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class DatabaseManager {

/*  public static void generateDatabase() {
    //not sure how to generate in JAVA as yet. I have made a DB manually in postgres called "grad2021"
  }*/

  static void createTableInDb() {
    Connection c;
    Statement stmt;
    try {
      Class.forName("org.postgresql.Driver");
      c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/grad2021", "postgres", "admin");
      System.out.println("***Connected to database***");

      Field[] fields = Customer.class.getDeclaredFields();
      String sqlPrefix = "CREATE TABLE " + getSqlTableName() + " ";
      String values = getSqlTableValues(fields);

      stmt = c.createStatement();
      stmt.executeUpdate(sqlPrefix + values);
      stmt.close();

      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Table created");
  }

  static void saveCustomerToTableInDB(Customer customer) {
    Connection c = null;
    Statement stmt = null;
    try {
      Class.forName("org.postgresql.Driver");
      c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/grad2021", "postgres", "admin");
      c.setAutoCommit(false);
      System.out.println("***Opened database successfully***");

      stmt = c.createStatement();
      String sqlPrefix = "INSERT INTO " + getSqlTableName() + " (customer_number, first_name, surname, date_of_birth) ";

      StringBuilder values = getSqlRowValues(customer);

      stmt.executeUpdate(sqlPrefix + values);

      stmt.close();
      c.commit();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Record(s)/Row(s) added successfully");
  }

  static void dropTable() {
    Connection c = null;
    Statement stmt = null;
    try {
      Class.forName("org.postgresql.Driver");
      c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/grad2021", "postgres", "admin");
      c.setAutoCommit(false);
      System.out.println("***Connected to database***");

      String sql = "DROP TABLE IF EXISTS " + getSqlTableName() + ";";
      stmt = c.createStatement();
      stmt.executeUpdate(sql);
      c.commit();

      stmt.close();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Table deleted");
  }

  /*Utility Methods*/
  private static String getSqlTableValues(Field[] fields) {
    String values = "";
    for (Field field : fields) {
      String colName = "";
      String colType = "";
      String colConstraints = "";

      Annotation annotation = field.getAnnotation(DomainProperty.class);
      if (annotation instanceof DomainProperty) {
        DomainProperty thisAnnotation = (DomainProperty) annotation;
        colName = thisAnnotation.name();
        colType = " " + thisAnnotation.type();

        if (thisAnnotation.isKey()) {
          colConstraints = " PRIMARY KEY";
        }
        if (!thisAnnotation.nullable()) {
          colConstraints += " NOT NULL, ";
        } else {
          colConstraints += ", ";
        }

        values += colName + colType + colConstraints;

      }

    }

    values = "(" + values.substring(0, values.length() - 2) + ");";
    return values;
  }

  private static StringBuilder getSqlRowValues(Customer customer) {
    return new StringBuilder("VALUES (" + "'" + customer.getCustomerNumber() + "', " + "'" + customer.getName() + "', " + "'" + customer.getSurname() + "', " + customer.getDateOfBirth() + ");");
  }

  private static String getSqlTableName() {
    String myTableName = Customer.class.getAnnotation(DomainClass.class).name();

    return myTableName;
  }

}
