package com.psybergate.grad2021.core.databases.hw3.b;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static com.psybergate.grad2021.core.databases.hw2a_v2_without_objects.DataBaseFileManager.ACCOUNT_TBL_NAME;
import static com.psybergate.grad2021.core.databases.hw2a_v2_without_objects.DataBaseFileManager.FILE_PATH;

public class FileGenerator_B {
  public static void main(String[] args) throws IOException {
    reviewableAccounts();
  }

  private static void reviewableAccounts() throws IOException {
    File file = new File(FILE_PATH + "Reviewable_Accounts.txt");
    FileWriter fw = new FileWriter(file);
    String line = "ALTER TABLE " + ACCOUNT_TBL_NAME + " ADD needs_to_be_reviewed BOOLEAN NOT NULL DEFAULT false;\n";
    fw.write(line);

    line = "UPDATE " + ACCOUNT_TBL_NAME + " SET needs_to_be_reviewed = true WHERE (rand_balance < 2000 AND CURRENT_DATE - start_date >= 90);\n";
    fw.write(line);

    fw.close();
  }
}
