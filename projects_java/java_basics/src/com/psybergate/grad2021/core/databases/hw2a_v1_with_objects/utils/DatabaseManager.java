package com.psybergate.grad2021.core.databases.hw2a_v1_with_objects.utils;

import com.psybergate.grad2021.core.databases.hw2a_v1_with_objects.domain.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.List;

public class DatabaseManager {

  public static void main(String[] args) {
    dropTables();
  }

  public static final String JDBC_DRIVER = "org.postgresql.Driver";

  private static final String DB_NAME = "jdbc:postgresql://localhost:5432/grad2021";

  private static final String DB_USER = "postgres";

  private static final String DB_PASSWORD = "admin";

  private static final String CUSTOMER_TBL_NAME = "Customers_1"; //raw data

  private static final String ACCOUNT_TYPE_TBL_NAME = "Account_Type_1"; //raw data

  private static final String TRANSACTION_TYPE_TBL_NAME = "Transaction_Type_1"; //raw data

  private static final String ACCOUNT_TBL_NAME = "Accounts_2"; //based off of other table(s)

  private static final String TRANSACTION_TBL_NAME = "Transactions_2"; //based off of other table(s)

  public static void setupTables() {
    dropTables();
    createTables();

    List fixedData = Arrays.asList(AccountType.Current, AccountType.Savings, AccountType.Credit, TransactionType.Deposit, TransactionType.Withdrawal);
    insertFixedDataToTableInDB(fixedData);
  }

  private static void dropTables() {
    System.out.println("Dropping Tables...");
    try {
      Connection c = connectToDb();
      Statement stmt = c.createStatement();

      String sql1 = "DROP TABLE IF EXISTS " + CUSTOMER_TBL_NAME + " CASCADE;";
      stmt.executeUpdate(sql1);

      String sql2 = "DROP TABLE IF EXISTS " + ACCOUNT_TYPE_TBL_NAME + " CASCADE;";
      stmt.executeUpdate(sql2);

      String sql3 = "DROP TABLE IF EXISTS " + TRANSACTION_TYPE_TBL_NAME + " CASCADE;";
      stmt.executeUpdate(sql3);

      String sql4 = "DROP TABLE IF EXISTS " + ACCOUNT_TBL_NAME + " CASCADE;";
      stmt.executeUpdate(sql4);

      String sql5 = "DROP TABLE IF EXISTS " + TRANSACTION_TBL_NAME + " CASCADE;";
      stmt.executeUpdate(sql5);

      stmt.close();
      c.commit();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Tables dropped!");
  }

  private static void createTables() {
    System.out.println("Creating Tables...");
    try {
      Connection c = connectToDb();
      Statement stmt = c.createStatement();

      String sql1 = "CREATE TABLE " + CUSTOMER_TBL_NAME + " (Customer_Num TEXT PRIMARY KEY, name TEXT, surname TEXT, dob DATE, city TEXT);";
      stmt.executeUpdate(sql1);

      String sql2 = "CREATE TABLE " + ACCOUNT_TYPE_TBL_NAME + " (Account_Type TEXT PRIMARY KEY);";
      stmt.executeUpdate(sql2);

      String sql3 = "CREATE TABLE " + TRANSACTION_TYPE_TBL_NAME + " (Transaction_Type TEXT PRIMARY KEY);";
      stmt.executeUpdate(sql3);

      String sql4 = "CREATE TABLE " + ACCOUNT_TBL_NAME + " (Account_Num TEXT PRIMARY KEY, Account_Type TEXT, Customer_Num TEXT, Start_Date DATE, Rand_Balance DECIMAL, FOREIGN KEY(Customer_Num) REFERENCES " + CUSTOMER_TBL_NAME + ", FOREIGN KEY(Account_type) REFERENCES " + ACCOUNT_TYPE_TBL_NAME + ");";
      stmt.executeUpdate(sql4);

      String sql5 = "CREATE TABLE " + TRANSACTION_TBL_NAME + " (Transaction_ID INT GENERATED ALWAYS AS IDENTITY, Account_Num TEXT, Transaction_Type TEXT, Transaction_Date DATE, FOREIGN KEY(Account_Num) REFERENCES " + ACCOUNT_TBL_NAME + ");";
      stmt.executeUpdate(sql5);

      stmt.close();
      c.commit();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Tables created!");
  }

  public static void insertVariableDataToTableInDB(List myList) {
    System.out.println("Inserting data...");
    try {
      Connection c = connectToDb();
      Statement stmt = c.createStatement();

      for (Object element : myList) {
        if (element instanceof Customer) {
          Customer c1 = (Customer) element;
          String values = " VALUES('" + c1.getCustomerNum() + "', '" + c1.getName() + "', '" + c1.getSurname() + "', '" + c1.getDateOfBirth() + "', '" + c1.getCity() + "');";
          stmt.executeUpdate("INSERT INTO " + CUSTOMER_TBL_NAME + values);
        }
        if (element instanceof Account) {
          Account acc1 = (Account) element;
          String values =
                  " VALUES('" + acc1.getAccountNum() + "', '" + acc1.getAccountType().name() + "', '" + acc1.getCustomer().getCustomerNum() + "', '" + acc1.getOpeningDate() + "', '" + acc1.getBalance() + "');";
          stmt.executeUpdate("INSERT INTO " + ACCOUNT_TBL_NAME + values);
        }
        if (element instanceof Transaction) {
          Transaction tt1 = (Transaction) element;
          String values =
                  " VALUES('" + tt1.getAccount().getAccountNum() + "', '" + tt1.getTransactionType() + "', '" + tt1.getTransactionDate() + "');";
          stmt.executeUpdate("INSERT INTO " + TRANSACTION_TBL_NAME + "(Account_Num, Transaction_Type, Transaction_Date)" + values);
        }
      }

      stmt.close();
      c.commit();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Data added successfully");
  }

  public static void insertFixedDataToTableInDB(List myList) {
    System.out.println("Inserting data...");
    try {
      Connection c = connectToDb();
      Statement stmt = c.createStatement();

      for (Object element : myList) {
        if (element instanceof AccountType) {
          String sqlPrefix = "INSERT INTO " + ACCOUNT_TYPE_TBL_NAME;
          AccountType accountType1 = (AccountType) element;
          String values = " VALUES('" + accountType1.name() + "');";
          stmt.executeUpdate(sqlPrefix + values);
        }
        if (element instanceof TransactionType) {
          String sqlPrefix = "INSERT INTO " + TRANSACTION_TYPE_TBL_NAME;
          TransactionType transactionType1 = (TransactionType) element;
          String values = " VALUES('" + transactionType1.name() + "');";
          stmt.executeUpdate(sqlPrefix + values);
        }
      }

      stmt.close();
      c.commit();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
    System.out.println("Data added successfully");
  }

  private static Connection connectToDb() throws ClassNotFoundException, SQLException {
    Connection c;
    Class.forName(JDBC_DRIVER);
    c = DriverManager.getConnection(DB_NAME, DB_USER, DB_PASSWORD);
    c.setAutoCommit(false);
    return c;
  }

/*  private static void createCustomerTable() {
    try {
      Connection c = connectToDb();
      Statement stmt = c.createStatement();

      String sql = "CREATE TABLE " + CUSTOMER_TBL_NAME + " (Customer_Num TEXT PRIMARY KEY, name TEXT, surname TEXT, address TEXT);";
      stmt.executeUpdate(sql);

      stmt.close();
      c.commit();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
  }

  private static void createAccountTable() {
    try {
      Connection c = connectToDb();
      Statement stmt = c.createStatement();

      String sql = "CREATE TABLE " + ACCOUNT_TBL_NAME + " (Account_Num TEXT PRIMARY KEY, Account_Type TEXT, Customer_Num TEXT UNIQUE, Start_Date DATE, Rand_Balance DECIMAL);";
      stmt.executeUpdate(sql);

      stmt.close();
      c.commit();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
  }

  private static void createTransactionTable() {
    try {
      Connection c = connectToDb();
      Statement stmt = c.createStatement();

      String sql = "CREATE TABLE " + TRANSACTION_TBL_NAME + " (Account_Num TEXT, Transaction_Type TEXT, Transaction_Date DATE);";
      stmt.executeUpdate(sql);

      stmt.close();
      c.commit();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
  }

  private static void dropCustomerTable() {
    try {
      Connection c = connectToDb();
      Statement stmt = c.createStatement();

      String sql = "DROP TABLE IF EXISTS " + CUSTOMER_TBL_NAME + ";";
      stmt.executeUpdate(sql);

      stmt.close();
      c.commit();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
  }

  private static void dropAccountTable() {
    try {
      Connection c = connectToDb();
      Statement stmt = c.createStatement();

      String sql = "DROP TABLE IF EXISTS " + ACCOUNT_TBL_NAME + ";";
      stmt.executeUpdate(sql);

      stmt.close();
      c.commit();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
  }

  private static void dropTransactionTable() {
    try {
      Connection c = connectToDb();
      Statement stmt = c.createStatement();

      String sql = "DROP TABLE IF EXISTS " + TRANSACTION_TBL_NAME + ";";
      stmt.executeUpdate(sql);

      stmt.close();
      c.commit();
      c.close();
    } catch (Exception e) {
      System.err.println(e.getClass().getName() + ": " + e.getMessage());
      System.exit(0);
    }
  }*/

}
