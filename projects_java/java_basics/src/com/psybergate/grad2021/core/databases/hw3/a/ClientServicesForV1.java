package com.psybergate.grad2021.core.databases.hw3.a;

import com.psybergate.grad2021.core.Utils;
import com.psybergate.grad2021.core.databases.hw2a_v1_with_objects.domain.Account;
import com.psybergate.grad2021.core.databases.hw2a_v1_with_objects.domain.AccountType;
import com.psybergate.grad2021.core.databases.hw2a_v1_with_objects.domain.Customer;
import com.psybergate.grad2021.core.databases.hw2a_v1_with_objects.domain.Transaction;
import com.psybergate.grad2021.core.databases.hw2a_v1_with_objects.utils.DatabaseManager;
import com.psybergate.grad2021.core.databases.AccountInformationGenerator;

import java.util.ArrayList;
import java.util.List;

import static com.psybergate.grad2021.core.databases.hw2a_v1_with_objects.utils.DatabaseManager.setupTables;
import static com.psybergate.grad2021.core.databases.AccountInformationGenerator.*;
import static com.psybergate.grad2021.core.CustomerInformationGenerator.*;

public final class ClientServicesForV1 {

  public static void main(String[] args) {
    setupTables();

    saveCustomers();
    saveAccounts();
    saveTransactions();
  }

  static List customers = new ArrayList();

  static List accounts = new ArrayList();

  static List transactions = new ArrayList();

  static final int NUM_OF_CUSTOMERS = 20;

  static final int NUM_OF_TOTAL_ACCOUNTS = 50;

  static final int NUM_OF_TRANSACTIONS = 100;

  public static void createCustomers() {
    List<String> listOfCustomerNumbers = generateCN(NUM_OF_CUSTOMERS);

    for (int i = 0; i < NUM_OF_CUSTOMERS; i++) {
      customers.add(new Customer(listOfCustomerNumbers.get(i), generateName(), generateSurname(), Utils.randomDate(), generateCity()));
    }
  }

  private static void saveCustomers() {
    createCustomers();
    DatabaseManager.insertVariableDataToTableInDB(customers);
    System.out.println(customers.size() + " Customers added to the DB");
  }

  public static void createAccounts() {
    List<String> listOfAccountNumbers = generateAN(NUM_OF_TOTAL_ACCOUNTS);
    int increment;
    int customerIndex = 0;

    for (int i = 0; i < NUM_OF_TOTAL_ACCOUNTS; i += increment) { //assigning random accounts to customer (max 8)
      int maxAccountsPerCustomer = AccountInformationGenerator.Rng(8, 0);
      int counter = 0;
      if (customerIndex > NUM_OF_CUSTOMERS - 1) {
        customerIndex = 0;
      }
      Customer c1 = (Customer) customers.get(customerIndex);

      while (counter <= maxAccountsPerCustomer) {
        AccountType accountType = generateAccountType();

        if (i + counter < NUM_OF_TOTAL_ACCOUNTS) {
          accounts.add(new Account(listOfAccountNumbers.get(i + counter), accountType, c1, Utils.randomDate(), generateBalance(accountType.name())));
        }
        counter++;
      }

      customerIndex++;
      increment = counter;
    }
  }

  private static void saveAccounts() {
    createAccounts();
    DatabaseManager.insertVariableDataToTableInDB(accounts);
    System.out.println(accounts.size() + " Accounts added to the DB");
  }

  public static void createTransactions() {
    for (int i = 0; i < NUM_OF_TRANSACTIONS; i++) {
      Account account = (Account) accounts.get(AccountInformationGenerator.Rng(NUM_OF_TOTAL_ACCOUNTS - 1, 0));
      transactions.add(new Transaction(account, generateTransactionType(), Utils.randomDate(2015, 2020)));

    }
  }

  private static void saveTransactions() {
    createTransactions();
    DatabaseManager.insertVariableDataToTableInDB(transactions);
    System.out.println(transactions.size() + " Transactions added to the DB");
  }

}
