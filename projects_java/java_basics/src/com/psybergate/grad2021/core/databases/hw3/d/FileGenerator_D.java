package com.psybergate.grad2021.core.databases.hw3.d;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static com.psybergate.grad2021.core.databases.hw2a_v2_without_objects.DataBaseFileManager.*;

public class FileGenerator_D {
  public static void main(String[] args) throws IOException {
    createViewCurrentAccountsFile();
  }

  private static void createViewCurrentAccountsFile() throws IOException {
    File file = new File(FILE_PATH + "View_Current_Accounts.txt");
    FileWriter fw = new FileWriter(file);

    /*basic view creation*/
    //String line = "CREATE VIEW current_accounts AS SELECT account_num, account_type, customer_num, rand_balance FROM " + ACCOUNT_TBL_NAME + " " + "WHERE (account_type = 'Current');\n";
    //fw.write(line);

    /*additional operations - join*/
    String line = "SELECT account_num, account_type, customer_num, transaction_type FROM " + ACCOUNT_TBL_NAME + " INNER JOIN " + TRANSACTION_TBL_NAME + " ON accounts.account_num = transactions.account_num;\n ";
    fw.write(line);

    fw.close();

    /**
     * AS - renames a column for the view. e.g customer_num AS id shows id as the column header of the view
     *
     */
  }
}
