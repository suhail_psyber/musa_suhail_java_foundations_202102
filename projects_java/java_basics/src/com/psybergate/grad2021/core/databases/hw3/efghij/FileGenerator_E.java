package com.psybergate.grad2021.core.databases.hw3.efghij;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static com.psybergate.grad2021.core.databases.hw2a_v2_without_objects.DataBaseFileManager.*;

public class FileGenerator_E {
  public static void main(String[] args) throws IOException {
    createSelectsFile();
  }

  private static void createSelectsFile() throws IOException {
    File file = new File(FILE_PATH + "Selects_E.txt");
    FileWriter fw = new FileWriter(file);

    /*E*/
    String line = "SELECT * FROM " + ACCOUNT_TBL_NAME + " WHERE customer_num = 'CN1000';\n ";
    fw.write(line);
    line = "SELECT * FROM " + CUSTOMER_TBL_NAME + " WHERE city = 'johannesburg;\n "; //just customers
    line = "SELECT * FROM " + CUSTOMER_TBL_NAME + " LEFT OUTER JOIN " + ACCOUNT_TBL_NAME + " ON customers.customer_num = accounts.customer_num " + "WHERE" + " city = johannesburg"; //full sql with join
    fw.write(line);
    line = "SELECT * FROM " + ACCOUNT_TBL_NAME + " LEFT JOIN " + TRANSACTION_TBL_NAME + " ON transactions.account_num = accounts.account_num " + "WHERE rand_balance > 6000";
    fw.write(line);

    /*F*/
    line = "SELECT * FROM " + CUSTOMER_TBL_NAME + " WHERE first_name LIKE 'J%';\n "; //the 'J%' part means a name starting with J... the % is filler
    fw.write(line);

    /*G*/
    line = "SELECT customer.first_name, count(account.customer_num), sum(account.rand_balance) FROM " + CUSTOMER_TBL_NAME + " JOIN " + ACCOUNT_TBL_NAME + " ON customers.customer_num = accounts.customer_num GROUP BY customers.customer_num;\n ";//part 1
    line = "SELECT customer.first_name, count(account.customer_num), sum(account.rand_balance) FROM " + CUSTOMER_TBL_NAME + " JOIN " + ACCOUNT_TBL_NAME + " ON customers.customer_num = accounts.customer_num GROUP BY customers.customer_num HAVING (count(accounts.customer_num)>5 AND sum(account.balance)>6000;\n ";//part 2
    //HAVING filters the grouping, counts, sums that we do
    fw.write(line);

    /*H*/
    line = "SELECT customer.first_name, min(account.rand_balance), max(account.rand_balance) FROM " + CUSTOMER_TBL_NAME + " LEFT JOIN " + ACCOUNT_TBL_NAME + " ON customers.customer_num = accounts.customer_num GROUP BY customers.customer_num;\n ";//part 1
    line = "SELECT customer.first_name, min(account.rand_balance), max(account.rand_balance) FROM " + CUSTOMER_TBL_NAME + " LEFT JOIN " + ACCOUNT_TBL_NAME + " ON customers.customer_num = accounts.customer_num GROUP BY customers.customer_num HAVING min(account.rand_balance)>1000 AND max(accounts.rand_balance)>8000;\n ";//part 2
    fw.write(line);

    /*I - this problem is tricky*/
    line = "SELECT account_num, rand_balance, account_type FROM " + ACCOUNT_TBL_NAME + " WHERE (account_type = 'Savings' or account_type = 'Credit');\n";
    line = "SELECT first_name, accounts.account_num, accounts.rand_balance FROM " + CUSTOMER_TBL_NAME + " JOIN " + ACCOUNT_TBL_NAME + " ON " + "customers.customer_num = accounts.customer_num WHERE (accounts.account_type = " + "'Savings' or " + "accounts.account_type = 'Credit');\n";
    fw.close();

  }
}
