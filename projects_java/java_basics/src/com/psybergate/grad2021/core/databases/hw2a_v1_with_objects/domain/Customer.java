package com.psybergate.grad2021.core.databases.hw2a_v1_with_objects.domain;

import java.time.LocalDate;

public class Customer {

  private String customerNum;

  private String name;

  private String surname;

  private LocalDate dateOfBirth;

  private String city;

  public Customer(String customerNum, String name, String surname, LocalDate dateOfBirth, String city) {
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
    this.city = city;
  }

  public String getCustomerNum() {
    return customerNum;
  }

  public void setCustomerNum(String customerNum) {
    this.customerNum = customerNum;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public LocalDate getDateOfBirth() {
    return dateOfBirth;
  }

  public void setDateOfBirth(LocalDate dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  @Override
  public String toString() {
    return "{" + "customerNum='" + customerNum + '\'' + ", name='" + name + '\'' + ", surname='" + surname + '\'' + ", dateOfBirth=" + dateOfBirth + ", city='" + city + '\'' + '}';
  }
}
