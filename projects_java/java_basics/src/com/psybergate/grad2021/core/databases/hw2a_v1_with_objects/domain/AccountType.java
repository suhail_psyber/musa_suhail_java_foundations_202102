package com.psybergate.grad2021.core.databases.hw2a_v1_with_objects.domain;

public enum AccountType {
  Current("Current"), Savings("Savings"), Credit("Credit");

  java.lang.String accountTypeString;

  private AccountType(java.lang.String accountTypeString) {
    this.accountTypeString = accountTypeString;
  }

}
