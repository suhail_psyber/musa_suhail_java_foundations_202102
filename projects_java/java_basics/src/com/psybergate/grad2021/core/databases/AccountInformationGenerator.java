package com.psybergate.grad2021.core.databases;

import com.psybergate.grad2021.core.databases.hw2a_v1_with_objects.domain.AccountType;
import com.psybergate.grad2021.core.databases.hw2a_v1_with_objects.domain.TransactionType;

import java.util.ArrayList;
import java.util.List;

public class AccountInformationGenerator {

  /*UTILITY METHODS*/
  public static int Rng(int highLimit, int lowLimit) {
    return (int) (Math.floor((Math.random() * (highLimit - lowLimit)) + lowLimit));
  }

  public static List<String> generateAN(int numberOfAccounts) {
    int currentAccountNumber = 10;
    List<String> listOfAccountNumbers = new ArrayList<>();

    for (int i = 0; i < numberOfAccounts; i++) {
      listOfAccountNumbers.add("AN" + currentAccountNumber);
      currentAccountNumber += Rng(10, 1);
    }
    return listOfAccountNumbers;
  }

  public static int generateBalance(String accountType) {
    int balance = Rng(10_000, 0);

    if (accountType.equalsIgnoreCase("current") || accountType.equalsIgnoreCase("credit"))
      if (Math.random() <= 0.2) {
        balance = -1 * balance;
      }

    return balance;
  }

  public static AccountType generateAccountType() {
    int accTypeID = Rng(4, 1);
    AccountType accountType = null;

    if (accTypeID == 1) {
      accountType = AccountType.Current;
    }
    if (accTypeID == 2) {
      accountType = AccountType.Credit;
    }
    if (accTypeID == 3) {
      accountType = AccountType.Savings;
    }

    return accountType;
  }

  public static TransactionType generateTransactionType() {
    int transTypeID = Rng(3, 1);
    TransactionType transactionType = null;

    if (transTypeID == 1) {
      transactionType = TransactionType.Deposit;
    }
    if (transTypeID == 2) {
      transactionType = TransactionType.Withdrawal;
    }

    return transactionType;
  }

}

