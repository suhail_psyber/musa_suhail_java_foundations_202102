package com.psybergate.grad2021.core.databases.hw2a_v1_with_objects.domain;

import java.time.LocalDate;

public class Account {

  private java.lang.String accountNum;

  private AccountType accountType;

  private Customer customer;

  private LocalDate openingDate;

  private int balance;

  public Account(java.lang.String accountNum, AccountType accountType, Customer customer, LocalDate openingDate, int balance) {
    this.accountNum = accountNum;
    this.accountType = accountType;
    this.customer = customer;
    this.openingDate = openingDate;
    this.balance = balance;
  }

  public java.lang.String getAccountNum() {
    return accountNum;
  }

  public void setAccountNum(java.lang.String accountNum) {
    this.accountNum = accountNum;
  }

  public AccountType getAccountType() {
    return accountType;
  }

  public void setAccountType(AccountType accountType) {
    this.accountType = accountType;
  }

  public Customer getCustomer() {
    return customer;
  }

  public void setCustomer(Customer string) {
    this.customer = customer;
  }

  public LocalDate getOpeningDate() {
    return openingDate;
  }

  public void setOpeningDate(LocalDate openingDate) {
    this.openingDate = openingDate;
  }

  public int getBalance() {
    return balance;
  }

  public void setBalance(int balance) {
    this.balance = balance;
  }
}
