package com.psybergate.grad2021.core.databases.hw2a_v1_with_objects.domain;

import java.time.LocalDate;

public class Transaction {

  private Account account;

  private TransactionType transactionType;

  private LocalDate transactionDate;

  public Transaction(Account account, TransactionType transactionType, LocalDate transactionDate) {
    this.account = account;
    this.transactionType = transactionType;
    this.transactionDate = transactionDate;
  }

  public Account getAccount() {
    return account;
  }

  public void setAccount(Account account) {
    this.account = account;
  }

  public TransactionType getTransactionType() {
    return transactionType;
  }

  public void setTransactionType(TransactionType transactionType) {
    this.transactionType = transactionType;
  }

  public LocalDate getTransactionDate() {
    return transactionDate;
  }

  public void setTransactionDate(LocalDate transactionDate) {
    this.transactionDate = transactionDate;
  }
}
