package com.psybergate.grad2021.core.databases.hw2a_v1_with_objects.domain;

public enum TransactionType {
  Deposit("Deposit"), Withdrawal("Withdrawal");

  java.lang.String transactionTypeString;

  TransactionType(java.lang.String transactionTypeString) {
    this.transactionTypeString = transactionTypeString;
  }


}
