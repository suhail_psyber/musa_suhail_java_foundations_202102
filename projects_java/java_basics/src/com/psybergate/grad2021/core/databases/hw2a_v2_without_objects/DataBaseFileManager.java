package com.psybergate.grad2021.core.databases.hw2a_v2_without_objects;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class DataBaseFileManager {

  public static final String CUSTOMER_TBL_NAME = "Customers"; //raw data

  public static final String ACCOUNT_TYPE_TBL_NAME = "Account_Types"; //raw data

  public static final String TRANSACTION_TYPE_TBL_NAME = "Transaction_Types"; //raw data

  public static final String ACCOUNT_TBL_NAME = "Accounts"; //based off of other table(s)

  public static final String TRANSACTION_TBL_NAME = "Transactions"; //based off of other table(s)

  public static final String FILE_PATH = "C:\\myworkbench\\mymentoring\\java_foundations_202102\\project_modules\\java_basics\\bin\\target\\com\\psybergate\\grad2021\\core\\databases\\hw3\\";

  public static void createSetupTables() throws IOException {
    System.out.println("Creating Tables...");

    File setupFile = new File(FILE_PATH + "Setup.txt");
    FileWriter setupFW = new FileWriter(setupFile);

    String dropAccountType = "DROP TABLE IF EXISTS " + ACCOUNT_TYPE_TBL_NAME + " CASCADE;\n";
    String accountType = "CREATE TABLE " + ACCOUNT_TYPE_TBL_NAME + " (Account_Type TEXT PRIMARY KEY);\n";
    setupFW.write(dropAccountType);
    setupFW.write(accountType);

    String accountTypeData = "INSERT INTO " + ACCOUNT_TYPE_TBL_NAME + " VALUES (" + addQuotes("Current") + ");\n";
    setupFW.write(accountTypeData);
    accountTypeData = "INSERT INTO " + ACCOUNT_TYPE_TBL_NAME + " VALUES (" + addQuotes("Savings") + ");\n";
    setupFW.write(accountTypeData);
    accountTypeData = "INSERT INTO " + ACCOUNT_TYPE_TBL_NAME + " VALUES (" + addQuotes("Credit") + ");\n";
    setupFW.write(accountTypeData);

    String dropTransactionType = "DROP TABLE IF EXISTS " + TRANSACTION_TYPE_TBL_NAME + " CASCADE;\n";
    String transactionType = "CREATE TABLE " + TRANSACTION_TYPE_TBL_NAME + " (Transaction_Type TEXT PRIMARY KEY);\n";
    setupFW.write(dropTransactionType);
    setupFW.write(transactionType);

    String transactionTypeData = "INSERT INTO " + ACCOUNT_TYPE_TBL_NAME + " VALUES (" + addQuotes("Deposit") + ");\n";
    setupFW.write(transactionTypeData);
    transactionTypeData = "INSERT INTO " + ACCOUNT_TYPE_TBL_NAME + " VALUES (" + addQuotes("Withdrawal") + ");\n";
    setupFW.write(transactionTypeData);

    setupFW.close();
    System.out.println("Tables created!");
  }

  public static StringBuilder addQuotes(Object givenString) {
    return new StringBuilder((String) givenString).insert(0, "'").append("'"); //DoB is not a String but we will still add quotes
  }

}
