package com.psybergate.grad2021.core.databases.hw3.a;

import com.psybergate.grad2021.core.Utils;
import com.psybergate.grad2021.core.databases.AccountInformationGenerator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import static com.psybergate.grad2021.core.databases.AccountInformationGenerator.*;
import static com.psybergate.grad2021.core.databases.hw2a_v2_without_objects.DataBaseFileManager.*;
import static com.psybergate.grad2021.core.CustomerInformationGenerator.*;

public final class ClientServicesForV2 {

  public static void main(String[] args) throws IOException {
    createSetupTables();
    createDataTables();
    dropAllTables();

    //createCustomersFile();
    //createAccountsFile();
    //createTransactionsFile();
  }

  static final int NUM_OF_CUSTOMERS = 20;

  static List<String> listOfCustomerNumbers = generateCN(NUM_OF_CUSTOMERS);

  static final int NUM_OF_TOTAL_ACCOUNTS = 50;

  static List<String> listOfAccountNumbers = generateAN(NUM_OF_TOTAL_ACCOUNTS);

  static final int NUM_OF_TRANSACTIONS = 100;

  /**
   * While it would be better to have different files for the different tables, this method consolidates them for ease going forward.
   * To import into psql use:
   * \i C:/myworkbench/mymentoring/java_foundations_202102/project_modules/java_basics/bin/target/com/psybergate/grad2021/core/databases/hw3/
   */
  private static void createDataTables() throws IOException {
    File dataFile = new File(FILE_PATH + "Data.txt");
    FileWriter fw = new FileWriter(dataFile);
    String line = "";

    /*CUSTOMERS*/
    line = "DROP TABLE IF EXISTS " + CUSTOMER_TBL_NAME + " CASCADE;\n";
    fw.write(line);

    line = "CREATE TABLE " + CUSTOMER_TBL_NAME + " (Customer_Num TEXT PRIMARY KEY, name TEXT, surname TEXT, dob DATE, city TEXT);\n";
    fw.write(line);

    for (int i = 0; i < NUM_OF_CUSTOMERS; i++) {
      line = "INSERT INTO " + CUSTOMER_TBL_NAME + " VALUES(" + addQuotes(listOfCustomerNumbers.get(i)) + "," + addQuotes(generateName()) + "," + addQuotes(generateSurname()) + "," + addQuotes(Utils.randomDate()) + "," + addQuotes(generateCity()) + ");\n";
      fw.write(line);
    }

    fw.write("\n");
    fw.write("\n");
    fw.write("\n");

    /*ACCOUNTS*/
    line = "DROP TABLE IF EXISTS " + ACCOUNT_TBL_NAME + " CASCADE;\n";
    fw.write(line);

    line = "CREATE TABLE " + ACCOUNT_TBL_NAME + " (Account_Num TEXT PRIMARY KEY, Account_Type TEXT NOT NULL, Customer_Num TEXT NOT NULL, Start_Date" +
            " DATE, " + "Rand_Balance DECIMAL, FOREIGN KEY(Customer_Num) REFERENCES " + CUSTOMER_TBL_NAME + ", FOREIGN KEY(Account_type) REFERENCES " + ACCOUNT_TYPE_TBL_NAME + ");";
    fw.write(line);

    int increment;
    int customerIndex = 0;

    for (int i = 0; i < NUM_OF_TOTAL_ACCOUNTS; i += increment) { //assigning random accounts to customer (max 8)
      int maxAccountsPerCustomer = AccountInformationGenerator.Rng(8, 0);
      int counter = 0;
      if (customerIndex > NUM_OF_CUSTOMERS - 1) { //reset if we reach the end
        customerIndex = 0;
      }

      while (counter <= maxAccountsPerCustomer) {
        if (i + counter < NUM_OF_TOTAL_ACCOUNTS) {
          String accountType = generateAccountType().name();
          line = "INSERT INTO " + ACCOUNT_TBL_NAME + " VALUES (" + addQuotes(listOfAccountNumbers.get(i + counter)) + "," + addQuotes(accountType) + "," + addQuotes(listOfCustomerNumbers.get(customerIndex)) + "," + addQuotes(Utils.randomDate()) + "," + generateBalance(accountType) + ");\n";
          fw.write(line);
        }
        counter++;
      }

      customerIndex++;
      increment = counter;
    }

    fw.write("\n");
    fw.write("\n");
    fw.write("\n");

    /*TRANSACTIONS*/
    line = "DROP TABLE IF EXISTS " + TRANSACTION_TBL_NAME + " CASCADE;\n";
    fw.write(line);

    line = "CREATE TABLE " + TRANSACTION_TBL_NAME + " (Transaction_ID INT GENERATED ALWAYS AS IDENTITY, Account_Num TEXT, Transaction_Type TEXT, Transaction_Date DATE, FOREIGN KEY(Account_Num) REFERENCES " + ACCOUNT_TBL_NAME + ");";
    fw.write(line);

    for (int i = 0; i < NUM_OF_TRANSACTIONS; i++) {
      String accNum = listOfAccountNumbers.get(AccountInformationGenerator.Rng(NUM_OF_TOTAL_ACCOUNTS - 1, 0));
      line =
              "INSERT INTO " + TRANSACTION_TBL_NAME + "(Account_Num, Transaction_Type, Transaction_Date) VALUES (" + addQuotes(accNum) + "," + addQuotes(generateTransactionType().name()) + "," + addQuotes(Utils.randomDate(2015, 2020)) + ");\n";
      //line = "INSERT INTO " + TRANSACTION_TBL_NAME + " VALUES (" + addQuotes(accNum) + "," + addQuotes(generateTransactionType().name()) + "," + addQuotes(generateDate(2015, 2020)) + ");\n";
      fw.write(line);
    }

    fw.close();
  }

  private static void dropAllTables() throws IOException {
    File file = new File(FILE_PATH + "Drop_Tables.txt");
    FileWriter fw = new FileWriter(file);
    String sql = "";

    sql = "DROP TABLE IF EXISTS " + CUSTOMER_TBL_NAME + " CASCADE;\n";
    fw.write(sql);

    sql = "DROP TABLE IF EXISTS " + ACCOUNT_TYPE_TBL_NAME + " CASCADE;\n";
    fw.write(sql);

    sql = "DROP TABLE IF EXISTS " + TRANSACTION_TYPE_TBL_NAME + " CASCADE;\n";
    fw.write(sql);

    sql = "DROP TABLE IF EXISTS " + ACCOUNT_TBL_NAME + " CASCADE;\n";
    fw.write(sql);

    sql = "DROP TABLE IF EXISTS " + TRANSACTION_TBL_NAME + " CASCADE;\n";
    fw.write(sql);

    fw.close();
  }

  public static void createCustomersFile() throws IOException {

    File customerFile = new File(FILE_PATH + "Customers.txt");
    System.out.println(customerFile.getCanonicalPath());
    FileWriter customerFileWriter = new FileWriter(customerFile);
    String line = "DROP TABLE IF EXISTS " + CUSTOMER_TBL_NAME + " CASCADE;\n";
    customerFileWriter.write(line);

    line = "CREATE TABLE " + CUSTOMER_TBL_NAME + " (Customer_Num TEXT PRIMARY KEY, name TEXT, surname TEXT, dob DATE, city TEXT);\n";
    customerFileWriter.write(line);

    for (int i = 0; i < NUM_OF_CUSTOMERS; i++) {
      line = "INSERT INTO " + CUSTOMER_TBL_NAME + " VALUES(" + addQuotes(listOfCustomerNumbers.get(i)) + "," + addQuotes(generateName()) + "," + addQuotes(generateSurname()) + "," + addQuotes(Utils.randomDate()) + "," + addQuotes(generateCity()) + ");\n";
      customerFileWriter.write(line);
    }

    customerFileWriter.close();
    System.out.println("Customer File Created");
  }

  public static void createAccountsFile() throws IOException {
    File accountsFile = new File(FILE_PATH + "Accounts.txt");
    FileWriter accountsFW = new FileWriter(accountsFile);
    String line = "DROP TABLE IF EXISTS " + ACCOUNT_TBL_NAME + " CASCADE;\n";
    accountsFW.write(line);

    line = "CREATE TABLE " + ACCOUNT_TBL_NAME + " (Account_Num TEXT PRIMARY KEY, Account_Type TEXT, Customer_Num TEXT, Start_Date DATE, " + "Rand_Balance DECIMAL, FOREIGN KEY(Customer_Num) REFERENCES " + CUSTOMER_TBL_NAME + ", FOREIGN KEY(Account_type) REFERENCES " + ACCOUNT_TYPE_TBL_NAME + ");";
    accountsFW.write(line);

    int increment;
    int customerIndex = 0;

    for (int i = 0; i < NUM_OF_TOTAL_ACCOUNTS; i += increment) { //assigning random accounts to customer (max 8)
      int maxAccountsPerCustomer = AccountInformationGenerator.Rng(8, 0);
      int counter = 0;
      if (customerIndex > NUM_OF_CUSTOMERS - 1) { //reset if we reach the end
        customerIndex = 0;
      }

      while (counter <= maxAccountsPerCustomer) {
        if (i + counter < NUM_OF_TOTAL_ACCOUNTS) {
          String accountType = generateAccountType().name();
          line = "INSERT INTO " + ACCOUNT_TBL_NAME + " VALUES (" + addQuotes(listOfAccountNumbers.get(i + counter)) + "," + addQuotes(accountType) + "," + addQuotes(listOfCustomerNumbers.get(customerIndex)) + "," + addQuotes(Utils.randomDate()) + "," + generateBalance(accountType) + ");\n";
          accountsFW.write(line);
        }
        counter++;
      }

      customerIndex++;
      increment = counter;
    }

    accountsFW.close();
  }

  public static void createTransactionsFile() throws IOException {
    File transactionsFile = new File(FILE_PATH + "Transactions.txt");
    FileWriter transactionsFW = new FileWriter(transactionsFile);
    String line = "DROP TABLE IF EXISTS " + TRANSACTION_TBL_NAME + " CASCADE;\n";
    transactionsFW.write(line);

    line = "CREATE TABLE " + TRANSACTION_TBL_NAME + " (Transaction_ID INT GENERATED ALWAYS AS IDENTITY, Account_Num TEXT, Transaction_Type TEXT, Transaction_Date DATE, FOREIGN KEY(Account_Num) REFERENCES " + ACCOUNT_TBL_NAME + ");";
    transactionsFW.write(line);

    for (int i = 0; i < NUM_OF_TRANSACTIONS; i++) {
      String accNum = listOfAccountNumbers.get(AccountInformationGenerator.Rng(NUM_OF_TOTAL_ACCOUNTS - 1, 0));
      line =
              "INSERT INTO " + TRANSACTION_TBL_NAME + "(Account_Num, Transaction_Type, Transaction_Date) VALUES (" + addQuotes(accNum) + "," + addQuotes(generateTransactionType().name()) + "," + addQuotes(Utils.randomDate(2015, 2020)) + ");\n";
      //line = "INSERT INTO " + TRANSACTION_TBL_NAME + " VALUES (" + addQuotes(accNum) + "," + addQuotes(generateTransactionType().name()) + "," + addQuotes(generateDate(2015, 2020)) + ");\n";
      transactionsFW.write(line);
    }

    transactionsFW.close();
  }

  public static StringBuilder addQuotes(Object obj) {
    if (obj == null) {
      obj = "";
    }
    if (obj instanceof LocalDate) {
      obj = obj + "";
    }
    return new StringBuilder((String) obj).insert(0, "'").append("'"); //DoB is not a String but we will still add quotes
  }

}
