package com.psybergate.grad2021.core.databases.hw3.c;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static com.psybergate.grad2021.core.databases.hw2a_v2_without_objects.DataBaseFileManager.*;

public class FileGenerator_C {
  public static void main(String[] args) throws IOException {
    createDeleteLowestBalanceFile();
  }

  private static void createDeleteLowestBalanceFile() throws IOException {
    File file = new File(FILE_PATH + "Delete_lowest_balance.txt");
    FileWriter fw = new FileWriter(file);

    /*DELETE LINE BUT IT COULD STILL REFERENCE IT AS A FOREIGN KEY IN A FUTURE TABLE SO NOT GOOD*/
    //String line = "DELETE FROM " + ACCOUNT_TBL_NAME + " WHERE rand_balance = (SELECT MIN(rand_balance) FROM accounts);\n";
    //fw.write(line);

    /*Make the foreign key references of this rows primary key DELETE CASCADE ... use when making the foreign key!!!*/
    /*DROP fkey*/
    String line = "ALTER TABLE " + TRANSACTION_TBL_NAME + " DROP CONSTRAINT transactions_account_num_fkey;\n";
    fw.write(line);

    /*ADD constraint fkey ... ON DELETE CASCADE*/
    line = "ALTER TABLE " + TRANSACTION_TBL_NAME + " ADD CONSTRAINT transactions_account_num_fkey FOREIGN KEY (account_num) REFERENCES " + ACCOUNT_TBL_NAME + " ON DELETE CASCADE;\n\n";
    fw.write(line);

    /*Now delete row from parent*/
    line = "DELETE FROM " + ACCOUNT_TBL_NAME + " WHERE (rand_balance = (SELECT MIN(rand_balance) FROM accounts));\n";
    fw.write(line);

    fw.close();
  }
}
