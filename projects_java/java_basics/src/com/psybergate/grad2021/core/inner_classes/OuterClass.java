package com.psybergate.grad2021.core.inner_classes;

public class OuterClass {

  public static void main(String[] args) {
    /**Static Inner Class - no outer object required - can access private variables*/
    //System.out.println("StaticInnerClass.string1 = " + StaticInnerClass.string1); //non-static variable
    //System.out.println("StaticInnerClass.getString1() = " + StaticInnerClass.getString1()); //non-static method
    System.out.println("StaticInnerClass.string2 = " + StaticInnerClass.string2);
    System.out.println("StaticInnerClass.getString2() = " + StaticInnerClass.getString2());
    System.out.println("");

    /**Object Inner Class - Object required - cannot access private variables*/
    //System.out.println(new ObjectInnerClass().string); //will not compile - needs an OUTER object
    System.out.println(new OuterClass().new ObjectInnerClass().string);
    System.out.println(new OuterClass().new ObjectInnerClass().getString());

    /**Anonymous Classes - instances/objects made on the fly WITH a class/abstraction defined... cannot be created later, only ONE object of its
     * type exists*/
    Object anonymousObject1 = new Object() {
      public boolean isAnonymous = true;

      public boolean getIsAnonymous() {
        return isAnonymous;
      }
    };

    //anonymousObject1.getIsAnonymous(); //will not compile

    Object anonymousObject2 = new Object() {
      public boolean isAnonymous = true;

      public boolean getIsAnonymous() {
        return isAnonymous;
      }
    }.isAnonymous; //calling the method right here works too

  }

  static class StaticInnerClass {

    private String string = "Static Inner Class with instance getter";

    private static String string2 = "Static inner Class with static getter";

    public String getString() {
      return string;
    }

    public static String getString2() {
      return string2;
    }
  }

  class ObjectInnerClass {
    public String string = "Object Inner Class";

    //private static String string2 = "Static inner Class with static getter"; //won't compile cannot have a static variable on an InstanceInnerclass

    public String getString() {
      return string;
    }

    //public static String getString2() { //won't compile cannot have a static method on an InstanceInnerclass
    //  return string2;
    //}
  }
}
