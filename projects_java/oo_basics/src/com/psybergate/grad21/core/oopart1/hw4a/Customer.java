package com.psybergate.grad21.core.oopart1.hw4a;

public class Customer {

  public static void main(String[] args) {
    Customer comA = new Customer("Company", "Check", 30, 1_000_000, "001");
    //Customer comB = new Customer("Company", "Credit", 10, 2_000_000, "002");
    Customer perA = new Customer("Person", "Credit", 25, 10_000, "003");
    //Customer perB = new Customer("Person", "Savings", 17, 50_000);

    comA.getDetails();
    //comB.getDetails();
    perA.getDetails();
    //perB.getDetails();
  }

  private static final int MIN_AGE = 18;

  private String typeOfCustomer;
  private String typeOfAccount;
  private int age;
  private int balance;
  private String customerNum; //Only for Companies

  public Customer(String typeOfCustomer, String typeOfAccount, int age, int balance) {
    checkAge(age);
    this.typeOfCustomer = typeOfCustomer;
    this.age = age;
    this.typeOfAccount = typeOfAccount;
    this.balance = balance;
  }

  public Customer(String typeOfCustomer, String typeOfAccount, int age, int deposit, String customerNum) {
    this(typeOfCustomer, typeOfAccount, age, deposit);
    this.customerNum = customerNum;
    //checkAge(); /* Not needed as it calls the "this method" which runs completely (including checkAge()) and then adds the new info of customerNum*/
  }

  private void checkAge(int age) {
    if (age < MIN_AGE) {
      throw new RuntimeException("Too young to make an account... Must be 18 or older");
    }
  }

  private void getDetails() {
    System.out.println("Details for " + getClass().getSimpleName() + " are: ");
    System.out.println("Type Of Customer = " + typeOfCustomer);
    System.out.println("Type Of Account = " + typeOfAccount);
    System.out.println("age = " + age);
    System.out.println("balance = " + balance + "\n");
  }

}
