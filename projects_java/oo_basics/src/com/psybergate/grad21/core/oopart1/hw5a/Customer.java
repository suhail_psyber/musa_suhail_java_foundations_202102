package com.psybergate.grad21.core.oopart1.hw5a;

import java.util.*;

public class Customer {

  private static final int MIN_AGE = 18;

  private String customerID;
  private int age;
  private List<Account> accounts; //We initialize this as this so only Account objects can be entered into the list

  public Customer(String customerID, int age, List<Account> accounts) {
    checkAge(age);
    this.customerID = customerID;
    this.age = age;
    this.accounts = accounts;
  }

  public void print() {
    System.out.println("Customer ID: " + customerID + "\n" +
                       "Customer Age: " + getAge() + "\n"); //We can use a getter or the variable directly
    for (Account account : accounts) {
      System.out.println("account: " + account);
    }
  }

  private void checkAge(int age) {
    if (age < MIN_AGE) {
      throw new RuntimeException("Too young to make an account... Must be 18 or older");
    }
  }

  public int getAge() {
    return age;
  }
}
