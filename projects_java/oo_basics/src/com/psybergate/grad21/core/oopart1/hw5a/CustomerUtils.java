package com.psybergate.grad21.core.oopart1.hw5a;


import java.util.*;

public class CustomerUtils {

  public static void main(String[] args) {
    //make a list of accounts
    List<Account> accounts = new ArrayList<>();
    accounts.add(new SavingsAccount("001S", 9999, 10000));
    accounts.add(new CurrentAccount("001C", 99, 10));
    accounts.add(new SavingsAccount("002S", 8888, 20000));
    //make a customer that takes in the list of accounts AND additional info
    Customer suhail = new Customer("123456", 25, accounts);
    printBalances(suhail);
  }

  private static void printBalances(Customer customer) {
      customer.print();
  }
}
