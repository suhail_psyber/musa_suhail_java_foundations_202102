package com.psybergate.grad21.core.oopart1.hw5a;

public class CurrentAccount extends Account {

  private int overdraft;

  public CurrentAccount(String accountNum, int balance, int overdraft) {
    super(accountNum, balance);
    this.overdraft = overdraft;
  }

  @Override
  public String toString() {
    return super.toString() + "\noverdraft = " + overdraft + "\n";
  }
}
