package com.psybergate.grad21.core.oopart1.hw5a;

public class SavingsAccount extends Account {

  private int minBalance;

  public SavingsAccount(String accountNum, int balance, int minBalance) {
    super(accountNum, balance);
    this.minBalance = minBalance;
  }

  @Override
  public String toString() {
    return super.toString()/*this part says we want to implement the toString() from our PARENT*/ + "\n" + "minBalance = " + minBalance + "\n";
  }
}
