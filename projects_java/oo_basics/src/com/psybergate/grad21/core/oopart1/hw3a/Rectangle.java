package com.psybergate.grad21.core.oopart1.hw3a;

public class Rectangle {
  public static void main(String[] args) {
    Rectangle rect1 = new Rectangle(1, 201);
    Rectangle rect2 = new Rectangle(99, 199);
    Rectangle rect3 = new Rectangle(10, 20);


    rect1.printArea();
    rect2.printArea();
    rect3.printArea();

    //    List rectangleList = new ArrayList<>();
    //    rectangleList.add(new Rectangle(1, 201));
    //    rectangleList.add(new Rectangle(99, 199));
    //    rectangleList.add(new Rectangle(10, 20));
    //    rectangleList.printArea();
  }

  private static final int MAX_LENGTH = 250;

  private static final int MAX_WIDTH = 150;

  private static final int MAX_AREA = 15000;

  private int length;
  private int width;


  public Rectangle(int side1, int side2) {
    this.length = Math.max(side1, side2);
    this.width = Math.min(side1, side2);
    checkParamLimits(); // Are the object made in this case? YES, it is after the assignment of length and width
  }

  private void checkParamLimits() {
    if (length > MAX_LENGTH) {
      System.out.println("length should not exceed " + MAX_LENGTH + " units");
      throw new RuntimeException(); // The method run AFTER object creation (PS: look at the flow of the constructor)
    }

    if (width > MAX_WIDTH) {
      System.out.println("width should not exceed " + MAX_WIDTH + " units");
      throw new RuntimeException();
    }
  }

  private int calcArea() {
    return length * width;
  }

  private void printArea() {
    if (calcArea() > MAX_AREA) {
      System.out.println("Area exceeds max area of " + MAX_AREA);
      throw new RuntimeException();
    } else {
      System.out.println("Area = " + calcArea());
    }
  }


  //  private void printArea() { // Make this work later - task is about OO not syntax and List manipulation
  //    ListIterator<Rectangle> rectangleList;
  //    rectangleList = rectangleList.Iterator();
  //    for (Rectangle rectangle : rectangleList) {
  //      if (calcArea() > MAX_AREA) {
  //        System.out.println("Area exceeds max area of " + MAX_AREA);
  //      } else {
  //        System.out.println("Area = " + calcArea());
  //      }
  //    }
  //  }


}

