package com.psybergate.grad21.core.oopart1.hw5a;

public class Account {

  private String accountNum;
  private int balance;

  public Account(String accountNum, int balance) {
    this.accountNum = accountNum;
    this.balance = balance;
  }

  @Override
  public String toString() { //we are overridding the toString() which can print objects - from the Object class and personalizing it for our
                             // scenario.
    return  getClass().getSimpleName() + "\n" +
            "accountNum = " + accountNum + '\n' +
            "balance = " + balance;
  }
}