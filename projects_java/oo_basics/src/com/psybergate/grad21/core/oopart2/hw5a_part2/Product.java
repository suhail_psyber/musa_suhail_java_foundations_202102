package com.psybergate.grad21.core.oopart2.hw5a_part2;

import java.util.HashMap;
import java.util.Map;

public class Product {

  private String productName;

  private MyEnumClass.Size size;

  public int randPricePerUnit;

  public Product(String productName, MyEnumClass.Size size, int randPricePerUnit) {
    this.productName = productName;
    this.size = size;
    this.randPricePerUnit = randPricePerUnit;
  }

  public static Map<MyEnumClass.Products, Product> getProductsWithEnum() {
    Map<MyEnumClass.Products, Product> products = new HashMap<>();

    products.put(MyEnumClass.Products.SMALLBICPEN, smallBicPen);
    products.put(MyEnumClass.Products.BICPEN, bicPen);
    products.put(MyEnumClass.Products.SMALLSTEADTLERPEN, smallSteadtlerPen);
    products.put(MyEnumClass.Products.STEADTLERPEN, steadtlerPen);
    products.put(MyEnumClass.Products.SMALLBICERASER, smallBicEraser);
    products.put(MyEnumClass.Products.BICERASER, bicEraser);
    products.put(MyEnumClass.Products.BICHIGHLIGHTERPACK, bicHighlighterPack);
    products.put(MyEnumClass.Products.STEADTLERHIGHLIGHTERPACK, steadtlerHighlighterPack);

    return products;
  }

  static Product smallBicPen = new Product("Bic Pen (Small)", MyEnumClass.Size.SMALL, 5);
  static Product bicPen = new Product("Bic Pen", MyEnumClass.Size.STANDARD, 10);
  static Product smallSteadtlerPen = new Product("Steadtler Pen (Small)", MyEnumClass.Size.SMALL, 20);
  static Product steadtlerPen = new Product("Steadtler Pen", MyEnumClass.Size.STANDARD, 30);
  static Product smallBicEraser = new Product("Bic Eraser (Small)", MyEnumClass.Size.SMALL, 3);
  static Product bicEraser = new Product("Bic Eraser", MyEnumClass.Size.STANDARD, 8);
  static Product bicHighlighterPack = new Product("Bic Highlighter Pack", MyEnumClass.Size.STANDARD, 70);
  static Product steadtlerHighlighterPack = new Product("Steadtler Highlighter Pack", MyEnumClass.Size.STANDARD, 100);

  @Override
  public String toString() {
    return productName;
  }
}
