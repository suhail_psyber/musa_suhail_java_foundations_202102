package com.psybergate.grad21.core.oopart2.hw5a_part1;

import java.time.LocalDate;

public class Customer {

  private final String customerID; //unique (validation to check if unique in constructor or leave up to database???)

  private MyEnumClass.TypeOfCustomer typeOfCustomer;

  private String country;

  private String fullName;

  private final LocalDate dateOfCreation;
  /* input as yyyy-mm-dd */

  private int totalPurchaseValue; //summed based of all orders (using /orderNumbers/ and customerNumbers)

  public Customer(String customerID, MyEnumClass.TypeOfCustomer typeOfCustomer, String country, String fullName, String dateOfCreation) {
    this.customerID = customerID; /**Q - HOW DO I GET THIS TO AUTOMATICALLY INCREMENT RATHER THAN INPUTTING IT???*/
    this.typeOfCustomer = typeOfCustomer;
    this.country = country;
    this.fullName = fullName;
    this.dateOfCreation = LocalDate.parse(dateOfCreation);
  }

  @Override
  public String toString() {
    return "Customer ID: " + getCustomerID() + '\n' +
            "Full Name: " + getFullName() + '\n' +
            "Country: " + getCountry() + '\n' +
            "Type Of Customer: " + getTypeOfCustomer() + '\n' +
            "Date Of Creation (yyyy-mm-dd): " + getDateOfCreation() + "\n" +
            "********************************";
  }

  public void print() {
    System.out.println(toString());
  }

  /*GETTERS*/

  public String getCustomerID() {
    return customerID;
  }

  public MyEnumClass.TypeOfCustomer getTypeOfCustomer() {
    return typeOfCustomer;
  }

  public String getCountry() {
    return country;
  }

  public String getFullName() {
    return fullName;
  }

  public LocalDate getDateOfCreation() {
    return dateOfCreation;
  }

  public int getTotalPurchaseValue() {
    return totalPurchaseValue;
  }

  public void setTotalPurchaseValue(int orderValue) {
    this.totalPurchaseValue += orderValue;
  }
}
