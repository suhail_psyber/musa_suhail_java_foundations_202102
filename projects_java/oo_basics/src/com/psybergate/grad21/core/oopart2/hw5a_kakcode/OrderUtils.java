package com.psybergate.grad21.core.oopart2.hw5a_kakcode;

public class OrderUtils {
  public static void main(String[] args) {
    /*create customers*/
    Customer c1 = new Customer("101","Local","South Africa", "John Johnson");

    /*create orders*/
    Order o1L = new LocalOrder("101", "001L", "South Africa", 0);//specific discount and import duties need to come from elsewhere - like a method,
    o1L.addOrderItem("pencil","bic",5);
    o1L.addOrderItem("pencil","staedtler",30);
    o1L.addOrderItem("eraser","bic",15);

    Order o2L = new LocalOrder("101", "002L", "New Zealand", 10);
    o2L.addOrderItem("sharpener","bic",50);

    /*Utils*/
//    c1.print();
    o1L.print();
    System.out.println("-----------------------------");
    o2L.print();

    /*  Product smallBicPen = new Product("Bic Pen", true, 5);
    Product bicPen = new Product("Bic Pen", false, 10);
    Product smallSteadtlerPen = new Product("Steadtler Pen", true, 20);
    Product steadtlerPen = new Product("Steadtler Pen", false, 30);
    Product smallBicEraser = new Product("Bic Eraser", true, 3);
    Product bicEraser = new Product("Bic Eraser", false, 8);
    Product bicHighlighterPack = new Product("Bic Highlighter Pack", false, 70);
    Product steadtlerHighlighterPack = new Product("Steadtler Highlighter Pack", false, 100);

    List<Product> products = new ArrayList<Product>();
    Collections.addAll(products, smallBicPen, bicPen, smallSteadtlerPen, steadtlerPen, smallBicEraser, bicEraser, bicHighlighterPack,
                       steadtlerHighlighterPack);*/
  }

}
