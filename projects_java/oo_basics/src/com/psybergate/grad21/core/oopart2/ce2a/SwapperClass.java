package com.psybergate.grad21.core.oopart2.ce2a;

public class SwapperClass {
  public static void main(String[] args) {
    SwapperClass.someMethod();
  }

  public static void someMethod() {
    SomeObject a1 = new SomeObject(1);
    SomeObject b1 = new SomeObject(2);
    a1.print();
    b1.print();
    System.out.println("");

    SwapperClass.swap(a1, b1);
    a1.print();
    b1.print();
    System.out.println("");

    ObjectWrapper owa1 = new ObjectWrapper(a1);
    ObjectWrapper owb1 = new ObjectWrapper(b1);

    SwapperClass.swap2(owa1, owb1);
    owa1.obj.print();
    owb1.obj.print();
    /*question - a1 and a2 still point to their original objects!!! how do we make it such that writing a1.print() prints 2*/
  }

  /*This swop method creates COPIES of the references so it does not work*/
  public static void swap(SomeObject so1, SomeObject so2) {
    System.out.println("Ran Basic Swap");
    SomeObject temp = so1;

    SomeObject a1 = so2;
    SomeObject b1 = temp;
  }

  /*We need to create a WRAPPER class and use that to swap object references*/
  public static void swap2(ObjectWrapper ow1, ObjectWrapper ow2) {
    System.out.println("Ran Wrapper Swap");
    SomeObject temp = ow1.obj;

    ow1.obj = ow2.obj;
    ow2.obj = temp;
  }
}
