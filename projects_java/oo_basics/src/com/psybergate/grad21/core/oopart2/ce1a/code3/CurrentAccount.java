package com.psybergate.grad21.core.oopart2.ce1a.code3;

import java.util.Currency;

public class CurrentAccount extends Account {

  public static void main(String[] args) {
    Account currentAccount1 = new CurrentAccount(123, 456, "Suhail", "Musa");
    //Account currentAccount = new CurrentAccount(123,456);
    //CurrentAccount currentAccount2 = new CurrentAccount(123, 456);
    /*currentAccount2 and 3 line does not compile because the PARENT CONSTRUCTOR is not inherited*/
  }

  private String name;
  private String surname;

  public CurrentAccount(int accountNum, int balance, String name, String surname) {
    super(accountNum, balance); //when this runs, the constructor in the PARENT runs including the print();
    this.name = name;
    this.surname = surname;
  }
}
