package com.psybergate.grad21.core.oopart2.demo2a;

public class IdentityAndEquality {
  public static void main(String[] args) {

    /*PRIMITIVES*/
/*    int p1 = 1;
    int p2 = 1;
    int p3 = p2;
    System.out.println("(p1 == p2) = " + (p1 == p2));
    System.out.println("(p1 == p2) = " + (p1 == p3));
    System.out.println("(p1 == p2) = " + (p2 == p3));
    System.out.println("");*/
    //equals won't work on above as it is an OBJECT method and those are primitives

    /*WRAPPER CLASSES*/
/*    Integer iwc1 = Integer.valueOf(1);
    Integer iwc2 = Integer.valueOf(1);
    Integer iwc3 = iwc2;
    System.out.println("(iwc1 == iwc2) = " + (iwc1 == iwc2));
    System.out.println("iwc1.equals(iwc2) = " + iwc1.equals(iwc2));
    System.out.println("(iwc1 == iwc2) = " + (iwc1 == iwc3));
    System.out.println("iwc1.equals(iwc2) = " + iwc1.equals(iwc3));
    System.out.println("(iwc1 == iwc2) = " + (iwc2 == iwc3));
    System.out.println("iwc1.equals(iwc2) = " + iwc2.equals(iwc3));
    System.out.println("");*/

    String swc1 = "abc";
    String swc2 = "Abc"; //memory and immutability
    String swc3 = swc1;
    System.out.println("swc1.equalsIgnoreCase(swc2) = " + swc1.equalsIgnoreCase(swc2));
    String swc1b = new String("abc");
    System.out.println("(swc1 == swc2) = " + (swc1 == swc2));
    System.out.println("swc1.equals(swc2) = " + swc1.equals(swc2));
    System.out.println("(swc1 == swc3) = " + (swc1 == swc3));
    System.out.println("swc1.equals(swc3) = " + swc1.equals(swc3));
    System.out.println("(swc2 == swc3) = " + (swc2 == swc3));
    System.out.println("swc2.equals(swc3) = " + swc2.equals(swc3));
    System.out.println("(swc1 == swc1b) = " + (swc1 == swc1b)); //This is false
    System.out.println("swc1.equals(swc1b) = " + swc1.equals(swc1b));
    System.out.println("");

    /*OBJECTS*/
/*    TestObject o1 = new TestObject("red");
    TestObject o2 = new TestObject("red");
    TestObject o3 = o2;
    System.out.println("(o1 == o2) = " + (o1 == o2));
    System.out.println("o1.equals(o2) = " + o1.equals(o2));
    System.out.println("(o1 == o3) = " + (o1 == o3));
    System.out.println("o1.equals(o3) = " + o1.equals(o3));
    System.out.println("(o2 == o3) = " + (o2 == o3));
    System.out.println("o2.equals(o3) = " + o2.equals(o3));*/
  }

}
