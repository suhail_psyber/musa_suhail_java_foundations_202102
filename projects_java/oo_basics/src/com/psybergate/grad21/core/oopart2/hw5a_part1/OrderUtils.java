package com.psybergate.grad21.core.oopart2.hw5a_part1;

import static com.psybergate.grad21.core.oopart2.hw5a_part1.MyEnumClass.Products.*;

public class OrderUtils {
  public static void main(String[] args) {

    /*CREATE CUSTOMERS*/
    Customer c1 = new Customer("101", MyEnumClass.TypeOfCustomer.LOCAL, "South Africa", "John Johnson", "1994-05-31");

    /*CREATE ORDERS - Input "Products."*/
    Order o1L = new LocalOrder(c1, "1", 10);
    o1L.addOrderItem(BICHIGHLIGHTERPACK, 2);
    o1L.addOrderItem(BICPEN, 10);

    /*PRINTS*/
    //c1.print();
    o1L.print();

  }

}
