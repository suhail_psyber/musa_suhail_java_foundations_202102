package com.psybergate.grad21.core.oopart2.hw4a;

public class ChildClass extends ParentClass {
  public static void main(String[] args) {
    ParentClass parent = new ChildClass();
    //ParentClass.print(); //printing through a specified class
    //parent.print(); //printing through the reference - does not behave polymorphically (looks at Obj ref type)
    print(); //printing through CURRENT class main is in

    ChildClass child = new ChildClass();
    //ChildClass.print(); //printing through a specified class
    //child.print(); //printing through the reference - does not behave polymorphically (looks at Obj ref type)
    print(); //printing through CURRENT class main is in
  }

  public static void print() {
    System.out.println("I am a static method in the CHILD");
  }

  /*Static methods are BOUND during compile time and thus depends on the REFERENCE TYPE used*/
}
