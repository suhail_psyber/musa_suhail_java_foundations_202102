package com.psybergate.grad21.core.oopart2.demo1a;

public class Shape {

  private String colour;

  public Shape(String colour) {
    this.colour = colour;
  }

  public static void onlyInParent() {
    System.out.println("Method only exists in PARENT");
  }

  public static void publicParentPrivateChild() {
    System.out.println("public method in the PARENT with private Child");
  }

  public static void publicParentPublicChild() {
    System.out.println("public method in the PARENT with Public Child");
  }

  private static void privateParentPrivateChild() {
    System.out.println("private method in the PARENT");
  }
}
