package com.psybergate.grad21.core.oopart2.hw3a;

import java.util.Objects;

public class Customer {

  public static void main(String[] args) {
    Customer sasukeInc = new Customer("Company", "Current", 25, 10_000, "003");
    Customer narutoInc = new Customer("Company", "Current", 37, 50_000, "003");
    Customer sakuraInc = sasukeInc;

    //    narutoInc.getDetails();
    //    sasukeInc.getDetails();

    System.out.println("narutoInc.equals(sasukeInc) = " + narutoInc.equals(sasukeInc));
    System.out.println("narutoInc == sasukeInc = " + (narutoInc == sasukeInc));
    System.out.println("sakuraInc.equals(sasukeInc) = " + sakuraInc.equals(sasukeInc));
    System.out.println("sakuraInc == sasukeInc = " + (sakuraInc == sasukeInc));
  }

  private static final int MIN_AGE = 18;

  private String typeOfCustomer;
  private String typeOfAccount;
  private int age;
  private int balance;
  private String customerNum; //Only for Companies

  public Customer(String typeOfCustomer, String typeOfAccount, int age, int balance) {
    checkAge(age);
    this.typeOfCustomer = typeOfCustomer;
    this.age = age;
    this.typeOfAccount = typeOfAccount;
    this.balance = balance;
  }

  public Customer(String typeOfCustomer, String typeOfAccount, int age, int balance, String customerNum) {
    this(typeOfCustomer, typeOfAccount, age, balance);
    if (typeOfCustomer == "Company") {
      this.customerNum = customerNum;
    } else {
      this.customerNum = null;
    }
  }

  private void checkAge(int age) {
    if (age < MIN_AGE) {
      throw new RuntimeException("Too young to make an account... Must be 18 or older");
    }
  }

  private void getDetails() {
    System.out.println("Details for " + getClass().getSimpleName() + " are: ");
    System.out.println("Type Of Customer = " + typeOfCustomer);
    System.out.println("Type Of Account = " + typeOfAccount);
    System.out.println("age = " + age);
    System.out.println("balance = " + balance + "\n");
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true; //Reflexivity
    if (this == null || obj == null) return false; //non-nullability
    if (obj instanceof Customer) {
      return this.customerNum.equals(((Customer)obj).customerNum); //our domain
    }
    return false;
  }

  @Override
  public int hashCode() {
    return Objects.hash(customerNum);
  }

}
