package com.psybergate.grad21.core.oopart2.hw5a_part2final;

public enum ProductSize {
  SMALL, STANDARD
}
