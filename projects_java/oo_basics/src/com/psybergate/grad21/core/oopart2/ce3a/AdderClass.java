package com.psybergate.grad21.core.oopart2.ce3a;

import java.math.BigDecimal;

import static com.psybergate.grad21.core.oopart2.ce3a.Money.*;

public class AdderClass {
  public static void main(String[] args) {
    Money a = new Money(5);
    a.randValue = BigDecimal.valueOf(95);

    AdderClass.add(a.randValue); //return 595 instead of 100
  }

  private static void add(BigDecimal bigDecimal) {
    System.out.println("integer + bigDecimal = " + integer + bigDecimal);
  }
}
