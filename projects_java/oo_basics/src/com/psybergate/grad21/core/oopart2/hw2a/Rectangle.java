package com.psybergate.grad21.core.oopart2.hw2a;

import java.util.Objects;

public class Rectangle {
  public static void main(String[] args) {
    Rectangle r1 = new Rectangle(100, 5);
    Rectangle r2 = new Rectangle(100, 5);
    Rectangle r3 = r2;

    /*HASH CODES*/
    System.out.println("r1 = " + r1.hashCode());
    System.out.println("r2 = " + r2.hashCode());
    System.out.println("r3 = " + r3.hashCode());

    /*EQUIVALENCE*/
    System.out.println("r1.equals(r2): " + r1.equals(r2)); /*Question - WHY IS IT RETURNING FALSE??Online Answer The original equals takes in an
                                                           OBJECT type and not Rectangle Type... but doesn't that defeat the purpose of the method??*/
    System.out.println("r1.equals(r3): " + r1.equals(r3));
    System.out.println("r2.equals(r3): " + r2.equals(r3));

    /*IDENTITY*/
    System.out.println("(r1 == r2): " + (r1 == r2));
    System.out.println("(r1 == r3): " + (r1 == r3));
    System.out.println("(r2 == r3): " + (r2 == r3));
  }

  private static final int MAX_AREA = 15000;

  private int length;
  private int width;


  public Rectangle(int side1, int side2) {
    this.length = Math.max(side1, side2);
    this.width = Math.min(side1, side2);
  }

  private int calcArea() {
    return length * width;
  }

  private void printArea() {
    if (calcArea() > MAX_AREA) {
      System.out.println("Area exceeds max area of " + MAX_AREA);
      throw new RuntimeException();
    } else {
      System.out.println("Area = " + calcArea());
    }
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true; //Reflexivity
    if (this == null || obj == null) return false; //non-nullability
    if (obj instanceof Rectangle) {
      return (this.length == ((Rectangle) obj).length) && (this.width == ((Rectangle) obj).width);
    }
    return false;
  }

  @Override
  public int hashCode() {
    return Objects.hash(length, width);
  }
}

