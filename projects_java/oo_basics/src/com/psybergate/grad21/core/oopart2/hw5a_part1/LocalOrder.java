package com.psybergate.grad21.core.oopart2.hw5a_part1;

public class LocalOrder extends Order {

  private int specifiedDiscount; //Make this a category instead for part 3. this way they specify the type of discount during order instance creation

  public LocalOrder(Customer customer, String orderNumber, int specifiedDiscount) {
    super(customer, orderNumber, "South Africa");
    this.specifiedDiscount = specifiedDiscount;
  }

  public static boolean isLocalOrder() {
    return true;
  }

  /**I will change discount... maybe add import none the less to keep the receipt consistent and just make it R0S*/
  public void print() {
    System.out.println(super.toString().replace("[", "").replace("]", "") + "\n" + "Discount: " + getSpecifiedDiscount() + "%" +
            "\n" + "Total Due: R");
  }

  /*GETTERS*/

  public int getSpecifiedDiscount() {
    return specifiedDiscount;
  }

}
