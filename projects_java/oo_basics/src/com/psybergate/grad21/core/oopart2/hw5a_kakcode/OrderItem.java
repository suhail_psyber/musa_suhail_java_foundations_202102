package com.psybergate.grad21.core.oopart2.hw5a_kakcode;

public class OrderItem {

  private String product;

  private String brand; //so you can order pencils... but a Staedtler costs more than a Bic

  private int quantity;

  private static int uniqueItemCount;

  public OrderItem(String product, String brand, int quantity) {
    this.product = product;
    this.brand = brand;
    this.quantity = quantity;
  }

  /*PRODUCTS*/

//  public int costingBicPencils(int quantity) {
//    int pricePerUnitInRands = 10;
//    return quantity * pricePerUnitInRands;
//  }

  @Override
  public String toString() {
    return "\n" +
            getUniqueItemCount() + ". " + "{" + "Product = " + product  + ", Brand = " + brand + ", Quantity = " + quantity + '}';
  }

  public void print() {
    System.out.println(toString());
  }

  /*GETTERS*/

  public String getProduct() {
    return product;
  }

  public String getBrand() {
    return brand;
  }

  public int getQuantity() {
    return quantity;
  }

  public int getUniqueItemCount() {
    return uniqueItemCount;
  }

  public static void setUniqueItemCount(int uniqueItemCount) {
    uniqueItemCount = uniqueItemCount;
  }
}
