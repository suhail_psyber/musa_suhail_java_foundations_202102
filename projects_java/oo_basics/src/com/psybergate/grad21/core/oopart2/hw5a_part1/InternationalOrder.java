package com.psybergate.grad21.core.oopart2.hw5a_part1;

public class InternationalOrder extends Order {

  private int importDuties;

  public InternationalOrder(Customer customer, String orderNumber, String shipToCountry, int importDuties) {
    super(customer, orderNumber, shipToCountry);
    this.importDuties = importDuties;
  }

  public static boolean isInternationalOrder() {
    return true;
  }

  public void print() {
    System.out.println(super.toString().replace("[", "").replace("]", "") + "\n" + "Discount: " + getImportDuties() + " R" +
            "\n" + "Total Due: R");
  }
  /*GETTERS*/

  public int getImportDuties() {
    return importDuties;
  }
}
