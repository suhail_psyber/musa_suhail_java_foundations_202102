package com.psybergate.grad21.core.oopart2.hw5a_kakcode;

import java.time.LocalDate;

public class Customer {

  private final String customerID; //unique (validation to check if unique in constructor or leave up to database - out of scope)

  private final String typeOfCustomer; //Local or International

  private String country;

  private String fullName;

  private final LocalDate dateOfCreation;

  //private String allOrders; //list of orderNums - use a list or string concat?

  private int totalPurchaseValue; //summed based of all orders (using /orderNumbers/ and customerNumbers)

  public Customer(String customerID, String typeOfCustomer, String country, String fullName) {
    this.customerID = customerID;
    this.typeOfCustomer = typeOfCustomer;
    this.country = country;
    this.fullName = fullName;
    this.dateOfCreation = LocalDate.now();
  }

  @Override
  public String toString() {
    return  "Customer ID: " + getCustomerID() + '\n' +
            "Full Name: " + getFullName() + '\n' +
            "Country: " + getCountry() + '\n' +
            "Type Of Customer: " + getTypeOfCustomer() + '\n' +
            "Date Of Creation (yyyy-mm-dd): " + getDateOfCreation();
  }

  public void print() {
    System.out.println(toString());
  }

  /*GETTERS*/

  public String getCustomerID() {
    return customerID;
  }

  public String getTypeOfCustomer() {
    return typeOfCustomer;
  }

  public String getCountry() {
    return country;
  }

  public String getFullName() {
    return fullName;
  }

  public LocalDate getDateOfCreation() {
    return dateOfCreation;
  }

  public int getTotalPurchaseValue() {
    return totalPurchaseValue;
  }

  public void setTotalPurchaseValue(int orderValue) {
    this.totalPurchaseValue += orderValue;
  }
}
