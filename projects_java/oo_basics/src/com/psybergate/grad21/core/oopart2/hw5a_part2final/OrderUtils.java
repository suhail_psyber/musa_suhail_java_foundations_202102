package com.psybergate.grad21.core.oopart2.hw5a_part2final;

import static com.psybergate.grad21.core.oopart2.hw5a_part2final.CustomerType.*;
import static com.psybergate.grad21.core.oopart2.hw5a_part2final.ProductCatalogue.*;

public class OrderUtils {
  public static void main(String[] args) {

    /*CREATE CUSTOMERS AND ORDERS*/
    Customer l1 = new Customer("101", LOCAL, "South Africa", "John Johnson", "2018-01-01");
    Order o1L = new LocalOrder(l1, "1");
    o1L.addOrderItem(BICHIGHLIGHTERPACK, 200);
    o1L.addOrderItem(BICPEN, 100);

    //Customer i2 = new Customer("202", INTERNATIONAL, "Spain", "Julio Vasquez", "2018-02-02");
    //Order o2I = new InternationalOrder(i2, "2", "Portugal");
    //o2I.addOrderItem(STEADTLERHIGHLIGHTERPACK, 5000);
    //o2I.addOrderItem(BICPEN, 2000);
    //o2I.addOrderItem(STEADTLERPEN, 4000);


    /*PRINTS*/
    l1.print();
    o1L.print();

    //    i2.print();
    //    o2I.print();

    //don't save ALL variables
    //validation check to be fixed

  }

}
