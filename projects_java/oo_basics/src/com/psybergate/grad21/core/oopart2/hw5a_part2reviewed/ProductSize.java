package com.psybergate.grad21.core.oopart2.hw5a_part2reviewed;

public enum ProductSize {
  SMALL, STANDARD
}
