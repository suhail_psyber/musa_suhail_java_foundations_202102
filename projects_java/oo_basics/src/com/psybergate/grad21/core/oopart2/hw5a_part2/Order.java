package com.psybergate.grad21.core.oopart2.hw5a_part2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Order {

  protected final String orderNumber;

  protected Customer customer;

  protected final LocalDate dateOfPurchase = LocalDate.now();

  protected String shipToCountry;

  List<OrderItem> order = new ArrayList<>();

  protected int orderTotal;

  protected int totalDue;

  protected int discountPercentage;

  public Order(Customer customer, String orderNumber, String shipToCountry) {
    /**Q - validation will fail? will the check execute the isLocalOrderMethod()? Class loading has occurred but what does that mean
     for static methods? should I move it into a static initialiser???*/
    //validation();
    this.customer = customer;
    this.orderNumber = orderNumber;
    this.shipToCountry = shipToCountry;
  }

  private void validation() {
    if ((customer.getTypeOfCustomer() == MyEnumClass.TypeOfCustomer.LOCAL) && LocalOrder.isLocalOrder()) {
      System.out.println("Order is acceptable. Proceeding...");
    } else if ((customer.getTypeOfCustomer() == MyEnumClass.TypeOfCustomer.INTERNATIONAL) && InternationalOrder.isInternationalOrder()) {
      System.out.println("Order is acceptable. Proceeding...");
    } else {
      throw new RuntimeException("Order type and Customer type mismatch. Please align order type");
    }
  }

  public void addOrderItem(MyEnumClass.Products product, int quantity) {
    order.add(new OrderItem(Product.getProductsWithEnum().get(product), quantity));
  }

  public int getOrderTotal() {
    for (OrderItem orderItem : order) {
      orderTotal += orderItem.getOrderItemTotal();
    }
    return orderTotal;
  }

  public int getRandDiscount() {
    discountPercentage = (int) determineDiscountPercentage();
    return (int) discountPercentage * orderTotal / 100;
  }

  protected double determineDiscountPercentage() {
    return 0;
  }

  public int getTotalDue() {
    return orderTotal;
  }

  public String getCustomer() {
    return customer.getCustomerID();
  }

  public String getOrderNumber() {
    return orderNumber;
  }

  public String getShipToCountry() {
    return shipToCountry;
  }

  public LocalDate getDateOfPurchase() {
    return dateOfPurchase;
  }

  public String getOrder() {
    String theirOrder = "";
    for (OrderItem orderItems : order) {
      theirOrder += orderItems.toString();
    }
    return theirOrder;
  }

  @Override
  public String toString() {
    return "CustomerID: " + getCustomer() + '\n' +
            "Order Number: " + getOrderNumber() + '\n' +
            "Ship To Country: " + getShipToCountry() + '\n' +
            "Date Of Purchase: " + getDateOfPurchase() + '\n' +
            "\n" +
            "Order: " + getOrder() + '\n' +
            "\n" +
            "Total Order Value: R" + getOrderTotal();
  }

  public void print() {
    System.out.println(toString());
  }

}
