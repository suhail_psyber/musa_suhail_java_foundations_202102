package com.psybergate.grad21.core.oopart2.hw5a_part2;

import static java.time.temporal.ChronoUnit.YEARS;

public class LocalOrder extends Order {

  public LocalOrder(Customer customer, String orderNumber) {
    super(customer, orderNumber, "South Africa");
  }

  public static boolean isLocalOrder() {
    return true;
  }

  @Override
  public double determineDiscountPercentage() {
    long customerTenure = customer.getDateOfCreation().until(dateOfPurchase, YEARS);
    System.out.println("customerTenure = " + customerTenure);
    if (customerTenure < 2) {
      return 0.0;
    } else if (customerTenure >= 2 && customerTenure <= 5) {
      return 7.5;
    }
    return 12.5;
  }

  @Override
  public int getTotalDue() {
    totalDue = orderTotal - getRandDiscount();
    return totalDue;
  }

  public void print() {
    System.out.println(super.toString().replace("[", "").replace("]", "") + "\n" + "-Discount: R" + getRandDiscount() +
            "\n" + "+Import Duties: R0" + "\n" + "Total Due: R" + getTotalDue());
  }

}
