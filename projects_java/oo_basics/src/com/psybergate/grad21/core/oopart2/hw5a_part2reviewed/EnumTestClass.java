package com.psybergate.grad21.core.oopart2.hw5a_part2reviewed;

public enum EnumTestClass {
  //Enums are for FIXED number of Objects from this abstraction!!!
  TEST01, TEST02("abc"); // createst an object of Type Test with name TEST01 - /*public static final Test TEST03 = new Test();*/
  private String name;

  EnumTestClass() { // private by default - default constructor

  }

  private EnumTestClass(String name) { // private by default - constructor with a parameter
    this.name = name;
  }
}
