package com.psybergate.grad21.core.oopart2.hw5a_part2final;

public class OrderItem {

  private ProductCatalogue product;

  private int quantity;

  public OrderItem(ProductCatalogue product, int quantity) {
    this.product = product;
    this.quantity = quantity;
  }

  public int getOrderItemTotal() {
    return quantity * product.randPricePerUnit;
  }

  @Override
  public String toString() {
    return "\n" + "{" + "Product = " + product + ", Quantity = " + quantity + ", Total Cost = " + getOrderItemTotal() + "}";
  }

  public void print() {
    System.out.println(toString());
  }

}
