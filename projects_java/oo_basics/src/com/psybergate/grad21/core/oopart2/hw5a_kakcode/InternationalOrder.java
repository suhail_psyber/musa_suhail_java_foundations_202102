package com.psybergate.grad21.core.oopart2.hw5a_kakcode;

public class InternationalOrder extends Order {

  private int importDuties;

  public InternationalOrder(String customerID, String orderNumber, String shipToCountry, int importDuties) {
    super(customerID, orderNumber, shipToCountry);
    this.importDuties = importDuties;
  }


  /*GETTERS*/

  public int getImportDuties() {
    return importDuties;
  }

  public void print() {
    System.out.println(super.toString() + "\n" + "Import Duties: R"+ getImportDuties());
  }
}
