package com.psybergate.grad21.core.oopart2.hw5a_part2;

import static com.psybergate.grad21.core.oopart2.hw5a_part2.MyEnumClass.Products.*;
import static com.psybergate.grad21.core.oopart2.hw5a_part2.MyEnumClass.TypeOfCustomer.*;

public class OrderUtils {
  public static void main(String[] args) {

    /*CREATE CUSTOMERS AND ORDERS*/
    Customer l1 = new Customer("101", MyEnumClass.TypeOfCustomer.LOCAL, "South Africa", "John Johnson", "2018-01-01");
    Order o1L = new LocalOrder(l1, "1");
    o1L.addOrderItem(BICHIGHLIGHTERPACK, 200);
    o1L.addOrderItem(BICPEN, 100);

    Customer i2 = new Customer("202", INTERNATIONAL, "Spain", "Julio Vasquez", "2018-02-02");
    Order o2I = new InternationalOrder(i2, "2", "Portugal");
    o2I.addOrderItem(STEADTLERHIGHLIGHTERPACK, 5000);
    o2I.addOrderItem(BICPEN, 2000);
    o2I.addOrderItem(STEADTLERPEN, 4000);


    /*PRINTS*/
//    l1.print();
//    o1L.print();

        i2.print();
        o2I.print();

  }

}
