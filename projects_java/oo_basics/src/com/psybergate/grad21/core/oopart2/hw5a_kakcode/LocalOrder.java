package com.psybergate.grad21.core.oopart2.hw5a_kakcode;

public class LocalOrder extends Order{

  private int specifiedDiscount;

  public LocalOrder(String customerID, String orderNumber, String shipToCountry, int specifiedDiscount) {
    super(customerID, orderNumber, shipToCountry);
    this.specifiedDiscount = specifiedDiscount;
  }

  /*GETTERS*/

  public int getSpecifiedDiscount() {
    return specifiedDiscount;
  }

  public void print() {
    System.out.println(super.toString().replace("[", "").replace("]","") + "\n" + "Discount: "+ getSpecifiedDiscount() + "%");
  }
}
