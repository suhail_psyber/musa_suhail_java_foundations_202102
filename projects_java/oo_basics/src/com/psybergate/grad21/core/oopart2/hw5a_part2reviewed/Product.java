package com.psybergate.grad21.core.oopart2.hw5a_part2reviewed;

import java.util.HashMap;
import java.util.Map;

import static com.psybergate.grad21.core.oopart2.hw5a_part2reviewed.ProductSize.*;

public class Product {

  private String productName;

  private ProductSize size;

  public int randPricePerUnit;

  public Product(String productName, ProductSize size, int randPricePerUnit) {
    this.productName = productName;
    this.size = size;
    this.randPricePerUnit = randPricePerUnit;
  }

  public static Map<ProductList, Product> getProductsWithEnum() {
    Map<ProductList, Product> products = new HashMap<>();

    products.put(ProductList.SMALLBICPEN, new Product("Bic Pen (Small)", SMALL, 5));
    products.put(ProductList.BICPEN, new Product("Bic Pen", STANDARD, 10));
    products.put(ProductList.SMALLSTEADTLERPEN, new Product("Steadtler Pen (Small)", SMALL, 20));
    products.put(ProductList.STEADTLERPEN, new Product("Steadtler Pen", STANDARD, 30));
    products.put(ProductList.SMALLBICERASER, new Product("Bic Eraser (Small)", SMALL, 3));
    products.put(ProductList.BICERASER, new Product("Bic Eraser", STANDARD, 8));
    products.put(ProductList.BICHIGHLIGHTERPACK, new Product("Bic Highlighter Pack", STANDARD, 70));
    products.put(ProductList.STEADTLERHIGHLIGHTERPACK, new Product("Steadtler Highlighter Pack", STANDARD, 100));

    return products;
  }

  @Override
  public String toString() {
    return productName;
  }
}
