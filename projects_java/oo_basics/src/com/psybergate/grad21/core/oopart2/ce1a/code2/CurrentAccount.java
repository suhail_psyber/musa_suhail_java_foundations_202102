package com.psybergate.grad21.core.oopart2.ce1a.code2;

public class CurrentAccount extends Account {
  public static void main(String[] args) {
    Account account = new CurrentAccount(10, 9405, "Eren","Jaeger","Attack Titen","Paradis");
  }

  private String name2;
  private String city;

  public CurrentAccount(int age, int accountNum, String name, String surName, String name2, String city) {
    super(age, accountNum, name, surName);
    System.out.println("\nChild constructor - pre assignment");
    print();
    this.name2 = name2;
    this.city = city;
    System.out.println("\nChild constructor - post assignment");
    print();
  }

  @Override
  public void print() {
    System.out.println("this.age = " + this.age);
    System.out.println("this.accountNum = " + this.accountNum);
    System.out.println("this.name = " + this.name);
    System.out.println("this.surName = " + this.surName);
    System.out.println("this.name2 = " + this.name2);
    System.out.println("this.city = " + this.city);
  }
}
