package com.psybergate.grad21.core.oopart2.demo1a;

public class SquareConstructor extends Shape {
  public static void main(String[] args) {
    //new SquareConstructor("red");
    /*if inherited then this would be syntactically supported -  this is how you show constructors are not inherited, and if it something is not inherited,
    it cannot be overridden as inheritance and polymorphism go hand in hand*/
  }

  private int length;
  private int width;

  //Actual constructor for SquareConstructor
  public SquareConstructor(String Colour, int length, int width) {
    super(Colour);
    this.length = length;
    this.width = width;
  }

  //attempting to Override Shape Constructor
/*  @Override
  public SquareConstructor(String Colour, int length, int width) {
    super(Colour);
    this.length = length;
    this.width = width;
  }*/


  /*cannot override something that isn't even inherited in the first place*/
}
