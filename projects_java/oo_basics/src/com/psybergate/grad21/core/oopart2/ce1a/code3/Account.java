package com.psybergate.grad21.core.oopart2.ce1a.code3;

public class Account {

  private int accountNum;
  private int balance;

  public Account(int accountNum, int balance) {
    this.accountNum = accountNum;
    this.balance = balance;
    print();
  }

  public void print() {
    System.out.println("I run during my construction and my children's");
  }
}
