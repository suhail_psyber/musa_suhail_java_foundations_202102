package com.psybergate.grad21.core.oopart2.ce2a;

public class SomeObject {
  private int num;

  public int getNum() {
    return num;
  }

  public SomeObject(int num) {
    this.num = num;
  }

  public void print() {
    System.out.println("getNum() = " + getNum());
  }
}
