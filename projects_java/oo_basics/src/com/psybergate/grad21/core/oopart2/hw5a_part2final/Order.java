package com.psybergate.grad21.core.oopart2.hw5a_part2final;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Order {

  protected final String orderNumber;

  protected Customer customer;

  protected final LocalDate orderDate = LocalDate.now();

  protected String shipToCountry;

  List<OrderItem> order = new ArrayList<>();

  protected double discountPercentage;

  protected double orderTotal; //keep this? or calc based off of the order components (items) everytime

  public Order(Customer customer, String orderNumber, String shipToCountry) {
    validation(customer);
    this.customer = customer;
    this.orderNumber = orderNumber;
    this.shipToCountry = shipToCountry;
  }

  //don't write technical things to test domain concepts so make customer = order not some enum
  private void validation(Customer customer) {
    System.out.println("Validating Order Type...");
    if ((customer.getCustomerType() == LocalOrder.orderType())) {
      System.out.println("Local Order is acceptable. Proceeding 1...");
    } else if ((customer.getCustomerType() == InternationalOrder.orderType())) {
      System.out.println("International Order is acceptable. Proceeding 2...");
    } else {
      throw new RuntimeException("Order type and Customer type mismatch. Please align order type to customer");
    }
  }

  public void addOrderItem(ProductCatalogue product, int quantity) {
    order.add(new OrderItem(product, quantity));
  }

  public double getOrderTotal() {
    for (OrderItem orderItem : order) {
      orderTotal += orderItem.getOrderItemTotal();
    }
    return orderTotal;
  }

  public double getRandDiscount() {
    discountPercentage = (int) determineDiscountPercentage();
    return (int) discountPercentage * orderTotal / 100;
  }

  protected double determineDiscountPercentage() {
    return 0;
  }

  public double getTotalDue() {
    return orderTotal;
  } //This changes polymorphically in the children

  public String getCustomer() {
    return customer.getCustomerNumber();
  }

  public String getOrderNumber() {
    return orderNumber;
  }

  public String getShipToCountry() {
    return shipToCountry;
  }

  public LocalDate getOrderDate() {
    return orderDate;
  }

  public String getOrder() {
    String theirOrder = "";
    for (OrderItem orderItems : order) {
      theirOrder += orderItems.toString();
    }
    return theirOrder;
  }

  @Override
  public String toString() {
    return "CustomerID: " + getCustomer() + '\n' + "Order Number: " + getOrderNumber() + '\n' + "Ship To Country: " + getShipToCountry() + '\n' + "Date Of Purchase: " + getOrderDate() + '\n' + "\n" + "Order: " + getOrder() + '\n' + "\n" + "Total Order Value: R" + getOrderTotal();
  }

  public void print() {
    System.out.println(toString());
  }

}
