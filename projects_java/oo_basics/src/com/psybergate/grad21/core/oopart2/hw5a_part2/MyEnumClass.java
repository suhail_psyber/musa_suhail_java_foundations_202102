package com.psybergate.grad21.core.oopart2.hw5a_part2;

public final class MyEnumClass {

  enum TypeOfCustomer {LOCAL, INTERNATIONAL}

  enum Size {SMALL, STANDARD}

  enum Products {SMALLBICPEN, BICPEN, SMALLSTEADTLERPEN, STEADTLERPEN, SMALLBICERASER, BICERASER, BICHIGHLIGHTERPACK, STEADTLERHIGHLIGHTERPACK}

}
