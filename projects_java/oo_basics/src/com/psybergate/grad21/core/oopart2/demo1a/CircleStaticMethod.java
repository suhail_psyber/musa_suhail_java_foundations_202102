package com.psybergate.grad21.core.oopart2.demo1a;

public class CircleStaticMethod extends Shape {
  public static void main(String[] args) {
    Shape parentRefType = new CircleStaticMethod("red",5);
    CircleStaticMethod childRefType = new CircleStaticMethod("red", 5);

    Shape.onlyInParent(); //this is how you call a static method at a class level
    parentRefType.onlyInParent();
    childRefType.onlyInParent();
    System.out.println("");
    //parentRefType.privateParentPrivateChild(); //private method - fails on call
    childRefType.privateParentPrivateChild();
    System.out.println("");
    parentRefType.publicParentPrivateChild();
    //childRefType.publicParentPrivateChild(); //compile error during method creation - cannot lower access modifier
    System.out.println("");
    parentRefType.publicParentPublicChild();
    childRefType.publicParentPublicChild(); //The method written here HIDES the parent method with the same name
  }

  private int radius;

  public CircleStaticMethod(String Colour, int radius) {
    super(Colour);
    this.radius = radius;
  }

  //attempting to override static methods - cannot be done - we invoke method HIDING
  //method used is decided at COMPILE TIME due to STATIC keyword - it depends on the *object REFERENCE TYPE* used during object creation

  public static void publicParentPublicChild() {
    System.out.println("HIDING OCCURED - public method in the CHILD with Public Parent");
  }

  //does not compile, cannot HIDE using a lower Access modifier
//  private static void publicParentPrivateChild() {
//    System.out.println("method that exists ONLY in the PARENT");
//  }

  private static void privateParentPrivateChild() {
    System.out.println("Private method in the CHILD with Private Parent");
  }

}
