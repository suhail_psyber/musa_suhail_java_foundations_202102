package com.psybergate.grad21.core.oopart2.demo2a;

public class TestObject {

  public String colour;

  public TestObject(String colour) {
    this.colour = colour;
  }

  public boolean equals(Object object) {
    if (!(object instanceof TestObject)) {
      return false;
    }
    return this.colour == ((TestObject) object).colour;
  }
}
