package com.psybergate.grad21.core.oopart2.hw5a_part2reviewed;

public class InternationalOrder extends Order {

  private static final int IMPORT_DUTIES_PERCENTAGE = 2;

  public InternationalOrder(Customer customer, String orderNumber, String shipToCountry) {
    super(customer, orderNumber, shipToCountry);
  }

  public static CustomerType orderType() {
    return CustomerType.INTERNATIONAL;
  }

  @Override
  public double determineDiscountPercentage() {

    if (orderTotal < 500_000) {
      return 0;
    } else if (orderTotal <= 1000_000) {
      return 5;
    }

    return 10;
  }
  //test - write one discount determination at a parent level (but leave it this way- just test) - abstraction

  @Override
  public double getTotalDue() {
    double totalDue = orderTotal - discountPercentage + getImportDuties();
    return totalDue;
  }

  public double getImportDuties() {
    return orderTotal * IMPORT_DUTIES_PERCENTAGE / 100;
  }

  @Override
  public void print() {
    System.out.println(super.toString().replace("[", "").replace("]", "") + "\n" + "-Discount: R" + getRandDiscount() + "\n" +
            "+Import Duties: R" + getImportDuties() + "\n" + "Total Due: R" + getTotalDue());
  }
}
