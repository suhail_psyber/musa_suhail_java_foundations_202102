package com.psybergate.grad21.core.oopart2.ce1a.code2;

public class Account {

  public int age;
  protected int accountNum;
  protected String name;
  protected String surName;

  public Account(int age, int accountNum, String name, String surName) {
    System.out.println("Parent constructor - pre assignment");
    print();
    this.age = age;
    this.accountNum = accountNum;
    this.name = name;
    this.surName = surName;
    System.out.println("\nParent constructor - post assignment");
    print();
  }

  public void print() {
  }
}
