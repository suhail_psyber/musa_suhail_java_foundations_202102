package com.psybergate.grad21.core.oopart2.hw5a_part2;

public class InternationalOrder extends Order {

  private static final int IMPORT_DUTIES_PERCENTAGE = 2;

  public InternationalOrder(Customer customer, String orderNumber, String shipToCountry) {
    super(customer, orderNumber, shipToCountry);
  }

  public static boolean isInternationalOrder() {
    return true;
  }

  @Override
  public double determineDiscountPercentage() {
    if (orderTotal < 500_000) {
      return 0;
    } else if (orderTotal >= 500_000 && orderTotal <= 1000_000) {
      return 5;
    }
    return 10;
  }

  @Override
  public int getTotalDue() {
    totalDue = (int) orderTotal - discountPercentage + getImportDuties();
    return totalDue;
  }

  public int getImportDuties() {
    return orderTotal * IMPORT_DUTIES_PERCENTAGE / 100;
  }

  /** Q - WHY DOESN'T THIS WORK??? */
/*  public int getImportDuties() {
    return (orderTotal * (IMPORT_DUTIES_PERCENTAGE / 100));
  }*/

  @Override
  public void print() {
    System.out.println(super.toString().replace("[", "").replace("]", "") + "\n" + "-Discount: R" + getRandDiscount() + "\n" +
            "+Import Duties: R" + getImportDuties() +
            "\n" + "Total Due: R" + getTotalDue());
  }
}
