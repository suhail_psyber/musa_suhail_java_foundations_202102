package com.psybergate.grad21.core.oopart2.demo1a;

public class Scrapbook {
  public static void main(String args[]) {
    ChildClass object1 = new ChildClass();
    //calling display() method by parent class object
    object1.display();
  }
}

//parent class
class ParentClass {
  //we cannot override the display() method
  public static void display() {
    System.out.println("display() method of the PARENT class.");
  }
}

//child class
class ChildClass extends ParentClass {
  //the same method also exists in the ParentClass
  //it does not override, actually it is method hiding
  public static void display() { //if we hide this, both ParentClass and ChildClass reference types would show the PARENT display()
    System.out.println("display() method in CHILD Class.");
  }
}
