package com.psybergate.grad21.core.oopart2.ce1a.code1;

import com.psybergate.grad21.core.oopart2.ce1a.code1.Account;

public class CurrentAccount extends Account {

  /**
   * Code does not compile because the default constructor in CurrentAccount implicitly has "super();" as it's first line shown below
   * public CurrentAccount() {
   *   super();
   * }
   *
   * There is only ONE constructor in Account which HAS PARAMETER, accountNum, and thus there is no constructor in Account that matches this super()
   **/

  /*works if we have the following*/
  public CurrentAccount(int accountNum) {
    super(accountNum);
  }
}
