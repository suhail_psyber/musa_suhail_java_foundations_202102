package com.psybergate.grad21.core.oopart2.hw5a_part2final;

public class LocalOrder extends Order {

  public LocalOrder(Customer customer, String orderNumber) {
    super(customer, orderNumber, "South Africa");
  }

  public static CustomerType orderType() {
    return CustomerType.LOCAL;
  }

  @Override
  public double determineDiscountPercentage() {

    int customerTenure = customer.getCustomerTenure();

    if (customerTenure < 2) {
      return 0.0;
    } else if (customerTenure <= 5) {
      return 7.5;
    }

    return 12.5;
  }

  @Override
  public double getTotalDue() {
    double totalDue = orderTotal - getRandDiscount();
    return totalDue;
  }

  public void print() {
    System.out.println(super.toString().replace("[", "").replace("]", "") + "\n" + "-Discount: R" + getRandDiscount() +
            "\n" + "+Import Duties: R0" + "\n" + "Total Due: R" + getTotalDue());
  }

}
