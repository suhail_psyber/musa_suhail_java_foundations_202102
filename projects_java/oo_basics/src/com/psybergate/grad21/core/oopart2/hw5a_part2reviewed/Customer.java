package com.psybergate.grad21.core.oopart2.hw5a_part2reviewed;

import java.time.LocalDate;

import static java.time.temporal.ChronoUnit.YEARS;

public class Customer {

  private final String customerNumber;

  private CustomerType customerType;

  private String country;

  private String fullName;

  private final LocalDate registrationDate;
  /* input as yyyy-mm-dd */

  public Customer(String customerNumber, CustomerType customerType, String country, String fullName, String registrationDate) {
    this.customerNumber = customerNumber; /**Q - HOW DO I GET THIS TO AUTOMATICALLY INCREMENT RATHER THAN INPUTTING IT??? parse a string? remove final?*/
    this.customerType = customerType;
    this.country = country;
    this.fullName = fullName;
    this.registrationDate = LocalDate.parse(registrationDate);
  }

  public String getCustomerNumber() {
    return customerNumber;
  }

  public CustomerType getCustomerType() {
    return customerType;
  }

  public String getCountry() {
    return country;
  }

  public String getFullName() {
    return fullName;
  }

  public LocalDate getRegistrationDate() {
    return registrationDate;
  }

  @Override
  public String toString() {
    return "Customer ID: " + getCustomerNumber() + '\n' +
            "Full Name: " + getFullName() + '\n' +
            "Country: " + getCountry() + '\n' +
            "Type Of Customer: " + getCustomerType() + '\n' +
            "Date Of Creation (yyyy-mm-dd): " + getRegistrationDate() + "\n" +
            "********************************";
  }

  public int getCustomerTenure() {
    long customerTenure = getRegistrationDate().until(LocalDate.now(), YEARS);
    return (int) customerTenure;
  }

  public void print() {
    System.out.println(toString());
  }

}
