package com.psybergate.grad21.core.oopart2.hw5a_kakcode;

import java.time.LocalDate;
import java.util.*;

public class Order {

  protected final String customerID;

  protected final String orderNumber;

  protected String shipToCountry;

  protected LocalDate dateOfPurchase = LocalDate.now();

  List<OrderItem> order = new ArrayList<>(); //list of orderItems

  public Order(String customerID, String orderNumber, String shipToCountry) { //Local
    this.customerID = customerID;
    this.orderNumber = orderNumber;
    this.shipToCountry = shipToCountry;
  }

  int uniqueItemCount = 0; /*use this to display the order properly i.e 1) Pencils, Bic, 5
                                                                    2) Pens, Staedtler, 3 etc.*/

  public void addOrderItem(String product, String brand, int quantity) {
    uniqueItemCount++;
    OrderItem.setUniqueItemCount(uniqueItemCount++);
    order.add(new OrderItem(product, brand, quantity));
    //how do I find the customer OBJECT to adjust based off of the customerID passed into order???
    //Customer.setTotalPurchaseValue(5); //find the customer Object that this order is tied to and increase the value by the orderItemTotalPrice
  }

  @Override
  public String toString() {
    return "Customer ID: " + getCustomerID() + '\n' +
            "Order Number: " + getOrderNumber() + '\n' +
            "Ship To Country: " + getShipToCountry() + '\n' +
            "Date Of Purchase: " + getDateOfPurchase() + '\n' +
            "Order: " /* + orderTotalValue*/ + getOrder();
  }

  public void print() {
    System.out.println(toString());
  }

  /*GETTERS*/

  public String getCustomerID() {
    return customerID;
  }

  public String getOrderNumber() {
    return orderNumber;
  }

  public String getShipToCountry() {
    return shipToCountry;
  }

  public LocalDate getDateOfPurchase() {
    return dateOfPurchase;
  }

  public List<OrderItem> getOrder() {
    return order;
  }

}
