package com.psybergate.grad21.core.oopart2.hw5a_part1;

public class OrderItem {

  private Product product;

  private int quantity;

  public OrderItem(Product product, int quantity) {
    this.product = product;
    this.quantity = quantity;
  }

  /* ORDER ITEM SHOULD DO THE TOTALS AND TELL THE ORDER. TELL DON'T ASK!!! */

/**
 *  Discounted prices can go here or later on - on the order itself? I like the latter in case of a discount
 *  model such as "if customers order for more than R1000, give them 10% discount" etc as this is a common discount model also KEEP this method
 *  and have another one that includes discount. Then in the order.print(), we can give base amount then discount (or import fees) and then
 *  final payment due
 *  */

  public int getOrderItemTotal() {
    return quantity * product.randPricePerUnit;
  }

  @Override
  public String toString() {
    return "\n" + "{" + "Product = " + product + ", Quantity = " + quantity + ", Total Cost = " + getOrderItemTotal() + "}" ;
  }

  public void print() {
    System.out.println(toString());
  }

  /*GETTERS*/

  public Product getProduct() {
    return product;
  }

  public int getQuantity() {
    return quantity;
  }

  public void setUniqueItemCount(int uniqueItemCount) {
    uniqueItemCount = uniqueItemCount;
  }
}
