package com.psybergate.grad21.core.oopart2.hw5a_part2final;

import static com.psybergate.grad21.core.oopart2.hw5a_part2final.ProductSize.*;

public enum ProductCatalogue {
  SMALLBICPEN("Bic Pen (Small)", SMALL, 5),
  BICPEN("Bic Pen", STANDARD, 10),
  SMALLSTEADTLERPEN("Steadtler Pen (Small)", SMALL, 20),
  STEADTLERPEN("Steadtler Pen", STANDARD, 30),
  SMALLBICERASER("Bic Eraser (Small)", SMALL, 3),
  BICERASER("Bic Eraser", STANDARD, 8),
  BICHIGHLIGHTERPACK("Bic Highlighter Pack", STANDARD, 70),
  STEADTLERHIGHLIGHTERPACK("Steadtler Highlighter Pack", STANDARD, 100);

  private String productName;

  private ProductSize size;

  public int randPricePerUnit;

  private ProductCatalogue(String productName, ProductSize size, int randPricePerUnit) {
    this.productName = productName;
    this.size = size;
    this.randPricePerUnit = randPricePerUnit;
  }

  @Override
  public String toString() {
    return productName;
  }

}
