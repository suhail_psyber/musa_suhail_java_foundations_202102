package com.psybergate.grad2021.jeefnds.servlets.domain;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
public class Customer {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE)
  private Long customerNumber;

  @Column(name = "name")
  private String name;

  @Column(name = "surname")
  private String surname;

  @Column(name = "date_of_birth")
  private LocalDate dOB;

  public Customer(String name, String surname, LocalDate dOB) {
    this.name = name;
    this.surname = surname;
    this.dOB = dOB;
  }

  public Long getCustomerNumber() {
    return customerNumber;
  }

  public void setCustomerNumber(Long customerNumber) {
    this.customerNumber = customerNumber;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public LocalDate getdOB() {
    return dOB;
  }

  public void setdOB(LocalDate dOB) {
    this.dOB = dOB;
  }

  //Create Table jee_ent_hw1_Customer (Customer_Number TEXT, name TEXT, surname TEXT, date_of_birth DATE);
}
