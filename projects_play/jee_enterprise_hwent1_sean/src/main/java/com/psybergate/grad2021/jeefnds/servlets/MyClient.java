package com.psybergate.grad2021.jeefnds.servlets;

import com.psybergate.grad2021.jeefnds.servlets.domain.*;

import java.time.LocalDate;

/**
 * Client layer utilising customer services
 */
public class MyClient {

  public static void main(String[] args) {
    Customer customer = new Customer("Suhail", "Musa", LocalDate.of(1994,05,31));
    Audit audit = new Audit("This is my audit description message", LocalDate.now());

    CustomerService customerService = new CustomerService();
    customerService.insertCustomerAndAudit(customer, audit);

    System.out.println("Done");
  }

}
