package com.psybergate.grad2021.jeefnds.servlets;

import com.psybergate.grad2021.jeefnds.servlets.domain.Audit;
import com.psybergate.grad2021.jeefnds.servlets.domain.Customer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class CustomerService {

  public static final EntityManagerFactory EMF = Persistence.createEntityManagerFactory("CustomerAudit_JPA");

  public void insertCustomerAndAudit(Customer customer, Audit audit) {
    EntityManager entityManager = null;
    EntityTransaction eTransaction = null;

    try {
      entityManager = EMF.createEntityManager();

      entityManager.getTransaction().begin();

      entityManager.persist(customer);
      entityManager.persist(audit);

      entityManager.getTransaction().commit();
    } catch (Exception e) {
      if (entityManager != null) {
        entityManager.getTransaction().rollback();
      }
      e.printStackTrace();
    } finally {
      if (entityManager != null) {
        entityManager.close();
      }
    }
  }

}
